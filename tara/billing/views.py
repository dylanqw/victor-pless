from datetime import datetime
import stripe
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from billing.models import (BillingAccount, BillingInvoice,
                            BillingPaymentMethod, BillingSubscription,
                            SchoolSubscription)
from billing.permissions import BillingPermission
from billing.serializers import (GetBillingAccountSerializer,
                                 SchoolSubscriptionSerializer)
from tara.settings.config_tara import main_conf as config

stripe.api_key = config.get('STRIPE_API_KEY')
stripe_webhook_signature = config.get('STRIPE_WEBHOOK_SIGNATURE')


# Helpers
def get_stripe_plan(plan):
    if plan == "yearly":
        return config.get('STRIPE_YEARLY_SUBSCRIPTION_ID')

    return config.get('STRIPE_MONTHLY_SUBSCRIPTION_ID')


def create_billing_customer(user, token, plan, code=None):
    customer = stripe.Customer.create(
        email=user.email,
        source=token,
    )
    try:
        subscription = stripe.Subscription.create(
            customer=customer.id,
            items=[
                {
                    'plan': get_stripe_plan(plan),
                },
            ],
            coupon=code,
            expand=['latest_invoice.payment_intent'],
        )
        return customer, subscription, None
    except stripe.error.CardError as e:
        return customer, None, e


def cancel_subscription(billing_account):
    billing_subscription = BillingSubscription.objects.filter(
        billing_account=billing_account).first()
    subscription = stripe.Subscription.modify(
        billing_subscription.subscription_id,
        cancel_at_period_end=True
    )
    billing_subscription.status = subscription.status
    if subscription.cancel_at is not None:
        billing_subscription.cancelled_at =\
            datetime.fromtimestamp(subscription.cancel_at)
    billing_subscription.billing_cycle_anchor =\
        datetime.fromtimestamp(subscription.billing_cycle_anchor)
    billing_subscription.save()
    return billing_subscription


def reactivate_subscription(billing_account):
    billing_subscription = BillingSubscription.objects.filter(
        billing_account=billing_account).first()
    subscription = stripe.Subscription.retrieve(
        billing_subscription.subscription_id
    )
    if subscription.ended_at is None and\
            subscription.cancel_at_period_end is True:
        subscription = stripe.Subscription.modify(
            billing_subscription.subscription_id,
            cancel_at_period_end=False,
            items=[{
                'id': subscription['items']['data'][0].id,
                'plan': get_stripe_plan(billing_subscription.plan),
            }]
        )
    else:
        subscription = stripe.Subscription.create(
            customer=billing_account.external_customer_id,
            items=[
                {
                    "plan": get_stripe_plan(billing_subscription.plan),
                },
            ]
        )
    billing_subscription.cancelled_at = None
    billing_subscription.billing_cycle_anchor =\
        datetime.fromtimestamp(subscription.billing_cycle_anchor)
    billing_subscription.subscription_id = subscription.id
    billing_subscription.save()
    return billing_subscription


def change_subscription(billing_account, plan):
    billing_subscription = BillingSubscription.objects.filter(
        billing_account=billing_account).first()
    subscription = stripe.Subscription.retrieve(
        billing_subscription.subscription_id
    )
    subscription = stripe.Subscription.modify(
        billing_subscription.subscription_id,
        cancel_at_period_end=False,
        items=[{
            'id': subscription['items']['data'][0].id,
            'plan': get_stripe_plan(plan),
        }]
    )
    billing_subscription.cancelled_at = None
    billing_subscription.billing_cycle_anchor =\
        datetime.fromtimestamp(subscription.billing_cycle_anchor)
    billing_subscription.plan = plan
    billing_subscription.save()


def save_card_payment_method(billing_account, card):
    payment_method = BillingPaymentMethod()
    payment_method.billing_account = billing_account
    payment_method.brand = card.brand
    payment_method.expiration_month = card.exp_month
    payment_method.expiration_year = card.exp_year
    payment_method.card_last_four = card.last4
    payment_method.type = 'card'
    payment_method.external_payment_id = card.id
    payment_method.save()
    return payment_method


def update_payment_method(billing_account, token):
    customer = stripe.Customer.modify(billing_account.external_customer_id, source=token)
    stripe_card = stripe.Customer.retrieve_source(customer.id, customer.default_source)
    payment_method = save_card_payment_method(billing_account, stripe_card)
    billing_subscription = BillingSubscription.objects.filter(
        billing_account=billing_account).first()
    billing_subscription.billing_payment_method = payment_method
    billing_subscription.save()
    return payment_method


def reattempt_failed_payment(billing_account, token):
    payment_method = update_payment_method(billing_account, token=token)
    billing_subscription = BillingSubscription.objects.filter(
        billing_account=billing_account).first()
    billing_subscription.billing_payment_method = payment_method
    billing_subscription.save()
    unpaid_invoice = BillingInvoice.objects.filter(
        billing_account=billing_account, status='unpaid').first()
    if unpaid_invoice is not None:
        paid_invoice = stripe.Invoice.pay(
            invoice=unpaid_invoice.external_invoice_id,
            expand=['payment_intent']
        )
        if paid_invoice.status == 'succeeded':
            billing_subscription.status = 'active'
        else:
            billing_subscription.status = paid_invoice.status
        billing_subscription.save()
    return billing_subscription


def handle_subscription_updated(subscription):
    billing_subscription = BillingSubscription.objects.filter(
        subscription_id=subscription.id).first()
    if billing_subscription is not None:
        billing_subscription.status = subscription.status
        if subscription.cancel_at is not None:
            billing_subscription.cancelled_at =\
                datetime.fromtimestamp(subscription.cancel_at)
        billing_subscription.save()


def handle_invoice_updated(invoice):
    billing_account = BillingAccount.objects.filter(
        external_customer_id=invoice.customer).first()
    if billing_account is not None:
        billing_invoice = BillingInvoice.objects.filter(
            external_invoice_id=invoice.id).first()
        if billing_invoice is None:
            billing_invoice = BillingInvoice()
            billing_invoice.billing_account = billing_account
            billing_invoice.external_invoice_id = invoice.id
            billing_invoice.amount = invoice.total
            billing_invoice.status = invoice.status
            billing_invoice.invoice_pdf = invoice.invoice_pdf
            billing_invoice.created_at =\
                datetime.fromtimestamp(invoice.created)
            billing_invoice.save()
        else:
            billing_invoice.status = invoice.status
            billing_invoice.save()


@method_decorator(csrf_exempt, name='dispatch')
class StripeWebhook(View):
    @method_decorator(csrf_exempt, name='dispatch')
    def post(self, request, *args, **kwargs):
        payload = request.body
        sig_header = request.META['HTTP_STRIPE_SIGNATURE']
        event = None
        try:
            event = stripe.Webhook.construct_event(
                payload, sig_header, stripe_webhook_signature
            )
        except ValueError as e:
            # Invalid payload
            return HttpResponse(status=400)
        except stripe.error.SignatureVerificationError as e:
            # Invalid signature
            return HttpResponse(status=400)
        # Handle the event
        if event.type in ['customer.subscription.updated',
                          'customer.subscription.deleted',
                          'customer.subscription.trial_will_end',
                          'customer.subscription.created']:
            subscription = event.data.object  # contains a stripe.Subscription
            handle_subscription_updated(subscription)
        elif event.type in ['invoice.deleted', 'invoice.payment_succeeded',
                            'invoice.finalized',
                            'invoice.marked_uncollectible',
                            'invoice.payment_action_required',
                            'invoice.payment_failed',
                            'invoice.sent',
                            'invoice.voided',
                            'invoice.upcoming',
                            'invoice.created']:
            invoice = event.data.object  # contains a stripe.Invoice
            handle_invoice_updated(invoice)
        else:
            # Unexpected event type
            return HttpResponse(status=400)

        return HttpResponse(status=200)


class BillingAccountViewSet(viewsets.ModelViewSet):
    """
        list:
            Return a list of all the existing billing account.
        create:
            Create a new billing account using token given.
        retrieve:
            Return the given billing account.
        update:
            Update the given billing account using token give.
        destroy:
            Cancels the given billing account.
        """
    permission_classes = (IsAuthenticated, BillingPermission)
    queryset = BillingAccount.objects.all()
    serializer_class = GetBillingAccountSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('created_by__id',)

    def update(self, request, *args, **kwargs):
        billing_account = BillingAccount.objects.filter(
            created_by=self.request.user).first()
        if request.data.get('subscription_request') == 'card_update':
            update_payment_method(
                billing_account, request.data.get('card_token')
            )
        elif request.data.get('subscription_request') == 'subscription_cancel':
            cancel_subscription(billing_account)
        elif request.data.get('subscription_request') ==\
                'subscription_reactivate':
            reactivate_subscription(billing_account)
        elif request.data.get('subscription_request') == 'subscription_change':
            change_subscription(billing_account, request.data.get('plan'))
        return Response(
            GetBillingAccountSerializer(billing_account).data,
            status.HTTP_200_OK
        )

    def create(self, request, **kwargs):
        # Create Account
        billing_account = BillingAccount()
        billing_account.created_by = self.request.user
        plan = request.data.get('plan')
        code = request.data.get('code')
        customer, billing, error = create_billing_customer(
            self.request.user,request.data.get('card_token'), plan, code)
        billing_account.external_customer_id = customer.id
        if billing is None and error is not None:
            return Response(
                error.json_body,
                status.HTTP_400_BAD_REQUEST
            )
        # Create Invoice
        billing_account.save()
        latest_invoice = billing.latest_invoice
        intent = latest_invoice.payment_intent.charges.data[0]
        invoice = BillingInvoice()
        invoice.billing_account = billing_account
        invoice.external_invoice_id = latest_invoice.id
        invoice.amount = latest_invoice.total
        invoice.created_at = datetime.fromtimestamp(latest_invoice.created)
        invoice.invoice_pdf = latest_invoice.invoice_pdf
        if intent.status == "succeeded":
            invoice.status = "paid"
        invoice.save()
        # Create Payment Method from initial intent
        payment_method = BillingPaymentMethod()
        payment_method.billing_account = billing_account
        payment_method.brand = intent.payment_method_details.card.brand
        payment_method.expiration_month =\
            intent.payment_method_details.card.exp_month
        payment_method.expiration_year =\
            intent.payment_method_details.card.exp_year
        payment_method.card_last_four =\
            intent.payment_method_details.card.last4
        payment_method.type = intent.payment_method_details.type
        payment_method.external_payment_id = intent.payment_method
        payment_method.save()
        # Create Subscription
        subscription = BillingSubscription()
        subscription.billing_account = billing_account
        subscription.billing_payment_method = payment_method
        subscription.plan = plan
        subscription.subscription_id = billing.id
        subscription.billing_cycle_anchor =\
            datetime.fromtimestamp(billing.billing_cycle_anchor)
        subscription.status = billing.status
        subscription.save()
        # Handle Status
        return Response(
            GetBillingAccountSerializer(billing_account).data,
            status.HTTP_201_CREATED
        )


class SchoolSubscriptionViewset(viewsets.ModelViewSet):
    """
        list:
            Return a list of all the existing school subscription.
        create:
            Create a new school subscription.
        retrieve:
            Return the given school subscription.
        update:
            Update the given school subscription.
        destroy:
            Cancels the given school subscription.
    """
    permission_classes = (IsAuthenticated,)
    queryset = SchoolSubscription.objects.all()
    serializer_class = SchoolSubscriptionSerializer


class ValidateCouponViewSet(viewsets.ViewSet):
    """
        View to validate coupon code provided by user.
        * Only logged In users are able to access this view.
    """

    def list(self, request):
        """
        Return coupon object.
        """
        if request.user.is_authenticated:
            get_data = request.query_params
            if not get_data['coupon'] == '':
                try:
                    coupon = stripe.Coupon.retrieve(get_data['coupon'])
                    return Response(coupon)
                except stripe.error.InvalidRequestError as e:
                    return Response({"error": "No such coupon", })
        else:
            return Response(
                {"detail": "Authentication credentials were not provided."}
            )
