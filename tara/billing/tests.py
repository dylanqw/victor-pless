from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from billing.models import SchoolSubscription
from billing.serializers import SchoolSubscriptionSerializer
from lesson.models import School

User = get_user_model()

client = APIClient()


class CreateSchoolSubscriptionTest(APITestCase):
    """
    Create SchoolSubscription API test cases
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))
        self.school = School.objects.create(name='test school')

        self.data = {
            'school': self.school.id,
            'start_date': '2019-11-28',
            'end_date': '2019-12-28',
            'active': True
        }

    def test_admin_can_create_school_subcrition(self):
        """
        Assert object creation success
        """
        user = User.objects.get(username=self.user_1)
        self.client.force_authenticate(user=user)
        self.client.login(username=self.user_1.username,
                          password=self.user_1.password)
        response = self.client.post(reverse('schoolsubscription-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.client.logout()
        self.client.force_authenticate(user=None)

    def test_user_can_create_school_subcrition(self):
        """
        Teacher can create school_subcrition
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2)
        self.client.force_authenticate(user=user)

        response = self.client.post(reverse('schoolsubscription-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class ReadSchoolSubscriptionTest(APITestCase):
    """
    Read School Subscription API test cases
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))
        self.school = School.objects.create(name='New Bosten')

        self.school_subcrition = SchoolSubscription.objects.create(
            school=self.school,
            start_date='2019-11-28',
            end_date='2019-12-28',
            active=True
        )

    def test_admin_can_read_school_subcrition_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('schoolsubscription-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_school_subcrition_detail(self):
        """
        Assert object detail read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('schoolsubscription-detail',
                                           args=[self.school_subcrition.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_can_read_school_subcrition_list(self):
        """
        Teacher can create subcrition_list
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('schoolsubscription-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_school_subcrition_detail(self):
        """
        Assert object detail read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('schoolsubscription-detail',
                                           args=[self.school_subcrition.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UpdateSchoolSubscriptionTest(APITestCase):
    """
    Update School Subscription API test cases
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.school = School.objects.create(name='New Bosten')

        self.school_subcrition = SchoolSubscription.objects.create(
            school=self.school,
            start_date='2019-11-28',
            end_date='2019-12-28',
            active=True
        )

        self.data = SchoolSubscriptionSerializer(self.school_subcrition).data
        self.data.update({
            'active': False,
        })

    def test_admin_can_update_school_subcrition(self):
        """
        Admin can update school subscription
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)

        response = self.client.put(reverse('schoolsubscription-detail',
                                           args=[self.school_subcrition.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_update_school_subcrition(self):
        """
        teacher can update school subscription
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)

        response = self.client.put(reverse('schoolsubscription-detail',
                                           args=[self.school_subcrition.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class DeleteSchoolSubcriptionTest(APITestCase):
    """
    Delete School API test cases
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.school = School.objects.create(name='New Bosten')

        self.school_subcrition = SchoolSubscription.objects.create(
            school=self.school,
            start_date='2019-11-28',
            end_date='2019-12-28',
            active=True
        )

    def test_admin_can_delete_school_subcrition(self):
        """
        Assert delete success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)

        response = self.client.delete(
            reverse('schoolsubscription-detail', args=[self.school_subcrition.id]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_can_delete_school_subcrition(self):
        """
        Teacher can delete school subscription
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)

        response = self.client.delete(
            reverse('schoolsubscription-detail', args=[self.school_subcrition.id]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)
