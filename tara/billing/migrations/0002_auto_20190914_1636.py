# Generated by Django 2.2 on 2019-09-14 16:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='billingpaymentmethod',
            old_name='billing_account_id',
            new_name='billing_account',
        ),
        migrations.RenameField(
            model_name='billingsubscription',
            old_name='billing_account_id',
            new_name='billing_account',
        ),
        migrations.AddField(
            model_name='billingpaymentmethod',
            name='external_payment_id',
            field=models.CharField(max_length=150, null=True, unique=True),
        ),
        migrations.CreateModel(
            name='BillingInvoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('external_invoice_id', models.CharField(max_length=150, null=True, unique=True)),
                ('billing_account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='billing.BillingAccount')),
            ],
        ),
    ]
