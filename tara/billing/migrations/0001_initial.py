# Generated by Django 2.2 on 2019-09-09 14:46

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BillingAccount',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('external_customer_id', models.CharField(max_length=150, unique=True)),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='BillingPaymentMethod',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('brand', models.CharField(max_length=50)),
                ('card_last_four', models.CharField(max_length=6)),
                ('type', models.CharField(max_length=12)),
                ('deleted_at', models.DateTimeField(null=True)),
                ('billing_account_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='billing.BillingAccount')),
            ],
        ),
        migrations.CreateModel(
            name='BillingSubscription',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subscription_id', models.CharField(max_length=150)),
                ('status', models.CharField(max_length=50)),
                ('cancelled_at', models.DateTimeField(null=True)),
                ('billing_account_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='billing.BillingAccount')),
                ('billing_payment_method', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='billing.BillingPaymentMethod')),
            ],
        ),
    ]
