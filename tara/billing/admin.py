from django.contrib import admin

from billing.models import SchoolSubscription

# Register your models here.
admin.site.register(SchoolSubscription)
