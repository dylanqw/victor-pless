from django.contrib.auth import get_user_model
from rest_framework import permissions

User = get_user_model()


class BillingPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return True
        else:
            return False

    def has_object_permission(self, request, view, obj):
        user_group = User.objects.filter(
            username=request.user
        ).values_list('groups__name', flat=True)
        if (request.user.is_staff or 'administrator' in user_group):
            return True
        else:
            if request.user == obj.created_by and request.method in \
                    ['GET', 'PUT', 'PATCH']:
                return True
