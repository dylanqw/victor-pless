from django.db import models
from django.conf import settings
from lesson.models import School


class BillingAccount(models.Model):
    external_customer_id = models.CharField(max_length=150, unique=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL
    )


class BillingPaymentMethod(models.Model):
    billing_account = models.ForeignKey(BillingAccount,
                                        related_name='payment_methods',
                                        on_delete=models.CASCADE)
    external_payment_id = models.CharField(max_length=150, null=True, unique=True)
    brand = models.CharField(max_length=50)
    expiration_year = models.IntegerField(),
    expiration_month = models.IntegerField(),
    card_last_four = models.CharField(max_length=6)
    type = models.CharField(max_length=12)
    deleted_at = models.DateTimeField(null=True)


class BillingSubscription(models.Model):
    billing_account = models.ForeignKey(BillingAccount,
                                        related_name='subscriptions',
                                        on_delete=models.CASCADE)
    billing_payment_method = models.ForeignKey(BillingPaymentMethod,
                                               null=True,
                                               on_delete=models.SET_NULL)
    subscription_id = models.CharField(max_length=150)
    plan = models.CharField(max_length=50, default='monthly')
    status = models.CharField(max_length=50)
    billing_cycle_anchor = models.DateTimeField(null=True, blank=True)
    cancelled_at = models.DateTimeField(null=True, blank=True)


class BillingInvoice(models.Model):
    billing_account = models.ForeignKey(BillingAccount,
                                        related_name='invoices',
                                        on_delete=models.CASCADE)
    status = models.CharField(max_length=50, default='unpaid')
    amount = models.IntegerField(default=0)
    invoice_pdf = models.CharField(max_length=256, null=True)
    created_at = models.DateTimeField(null=True, blank=True)
    external_invoice_id = models.CharField(max_length=150, null=True, unique=True)


class SchoolSubscription(models.Model):
    school = models.ForeignKey(School, on_delete=models.CASCADE)
    start_date = models.DateField()
    end_date = models.DateField()
    active = models.BooleanField(default=False)

    def __str__(self):
        return self.school.name
