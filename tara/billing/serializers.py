from rest_framework import serializers

from billing.models import (BillingAccount, BillingInvoice,
                            BillingPaymentMethod, BillingSubscription,
                            SchoolSubscription)


class GetBillingPaymentMethodSerializer(serializers.ModelSerializer):
    """
    Billing Payment serializers
    """

    class Meta:
        """
        Serializing all fields
        """
        model = BillingPaymentMethod
        fields = ['id', 'billing_account_id', 'external_payment_id',
                  'brand',
                  'card_last_four', 'type']


class GetBillingSubscriptionSerializer(serializers.ModelSerializer):
    """
    Billing Subscription serializers
    """

    class Meta:
        """
        Serializing all fields
        """
        model = BillingSubscription
        fields = ['id', 'billing_account_id',
                  'subscription_id', 'status',
                  'billing_payment_method','billing_cycle_anchor', 'cancelled_at', 'plan']


class GetBillingInvoiceSerializer(serializers.ModelSerializer):
    """
    Billing Subscription serializers
    """

    class Meta:
        """
        Serializing all fields
        """
        model = BillingInvoice
        fields = ['id', 'amount', 'created_at', 'invoice_pdf']


class GetBillingAccountSerializer(serializers.ModelSerializer):
    """
       Billing Subscription serializers
       """
    subscriptions = GetBillingSubscriptionSerializer(
        many=True,
        read_only=True
    )
    payment_methods = GetBillingPaymentMethodSerializer(
        many=True,
        read_only=True
    )

    invoices = GetBillingInvoiceSerializer(
        many=True,
        read_only=True
    )

    class Meta:
        """
        Serializing all fields
        """
        model = BillingAccount
        fields = ['id', 'external_customer_id', 'subscriptions', 'payment_methods', 'invoices']


class SchoolSubscriptionSerializer(serializers.ModelSerializer):
    """
    School Subscription serializers
    """

    class Meta:
        """
        Serializing all fields
        """
        model = SchoolSubscription
        fields = '__all__'
