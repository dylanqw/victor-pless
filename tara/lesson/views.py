import calendar
import os
import re
from datetime import date, datetime, timedelta
from math import ceil
from os import environ

import docx
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models import Case, Count, F, Q, When
from django.http import HttpResponse, HttpResponseForbidden, JsonResponse
from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from docx import Document
from docx.enum.dml import MSO_THEME_COLOR_INDEX
from docx.enum.section import WD_ORIENT
from docx.enum.text import WD_ALIGN_PARAGRAPH, WD_COLOR_INDEX, WD_LINE_SPACING
from docx.oxml import parse_xml
from docx.oxml.ns import nsdecls, qn
from docx.oxml.shared import OxmlElement
from docx.shared import Cm, Inches, Mm, Pt, RGBColor
from rest_framework import filters, generics, status, viewsets
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from lesson.models import (Activity, ActivityComment, ActivityContainer,
                           ActivityTemplate, Attachment, Calender, Class,
                           Container, EvaluateLessonObservation,
                           EvaluationChoices, EvaluationCriteriaGroup,
                           EvaluationType, LearningIntelligence,
                           LessonActivity, LessonPlan, Observation,
                           ObservationType, RateLesson, School, SchoolDocument,
                           SchoolType, SmartGuide, Standards, State,
                           StrategyGroup, Subject, TagElement, Template,
                           getDomain, lesson_observation_email,
                           sendEmailToCoTeacher)
from lesson.permissions import (ActivityCommentPermission,
                                ActivityTemplatePermission, ClassPermission,
                                EvaluateLessonPermission,
                                FixActivityPermission,
                                LessonActivityPermission, LessonPermission,
                                ObservationPermission,
                                OnlyAdministrativePermission,
                                OnlyReadPermission, RateLessonPermission,
                                SchoolPermission, StandardPermission,
                                TagElementPermission, TemplatePermission)
from lesson.serializers import (ActivityTemplateSerializer,
                                AttachmentSerializer, CalenderSerializers,
                                ContainerSerializer,
                                EvaluateLessonObservationSerializers,
                                EvaluationChoicesSerializers,
                                EvaluationCriteriaGroupSerializers,
                                EvaluationTypeSerializers,
                                GetActivityCommentSerializer,
                                GetActivityContainerSerializer,
                                GetActivitySerializer, GetClassSerializer,
                                GetLessonActivitySerializer,
                                GetLessonPlanSerializer, GetTemplateSerializer,
                                LearningIntelligenceSerializer,
                                ObservationSerializers,
                                ObservationTypeSerializers,
                                PostActivityCommentSerializer,
                                PostActivityContainerSerializer,
                                PostActivitySerializer, PostClassSerializer,
                                PostLessonActivitySerializer,
                                PostLessonPlanSerializer,
                                PostTemplateSerializer, RateLessonSerializer,
                                RetriveObservationSerializers,
                                SchoolDocumentSerializer, SchoolSerializer,
                                SchoolTypeSerailizer, SmartGuideSerializers,
                                StandardSerializer, StateSerializer,
                                StrategyGroupSerializers, SubjectSerializer,
                                TagElementSerializer)
from tara_user.models import ClusterAdminSchool

User = get_user_model()


class SchoolViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing school.
    create:
        Create a new school instance.
    retrieve:
        Return the given school.
    update:
        Update the given school.
    partial_update:
        Update the given school given field only.
    destroy:
        Delete the given school.
    """
    permission_classes = (OnlyReadPermission,)
    queryset = School.objects.all().order_by('name')
    serializer_class = SchoolSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('^name', )


class SubjectViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing subject.
    create:
        Create a new subject instance.
    retrieve:
        Return the given subject.
    update:
        Update the given subject.
    partial_update:
        Update the given subject given field only.
    destroy:
        Delete the given subject.
    """
    permission_classes = (IsAuthenticated, OnlyAdministrativePermission)
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer


class LessonClassPagination(LimitOffsetPagination):
    default_limit = 15


class ClassViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing class.
    create:
        Create a new class instance.
    retrieve:
        Return the given class.
    update:
        Update the given class.
    partial_update:
        Update the given class given field only.
    destroy:
        Delete the given class.
    """
    permission_classes = (IsAuthenticated, ClassPermission)
    queryset = Class.objects.all()
    pagination_class = LessonClassPagination
    serializer_class = GetClassSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter,)
    filterset_fields = ('created_by__id', 'co_teacher__id', 'share_with_school')
    ordering_fields = ('id',)

    def get_serializer_class(self):

        serializer_class = self.serializer_class
        if self.request.method not in ['GET']:
            serializer_class = PostClassSerializer
        return serializer_class

    def get_queryset(self):
        """
        Custom queryset to get classes based on ids
        """
        queryset = Class.objects.all()
        class_id = self.request.query_params.get('class_id')
        user_id = self.request.query_params.get('user_id')

        if class_id:
            queryset = Class.objects.filter(id__in=class_id.split(','))

        if user_id:
            if not self.request.user.school:
                queryset = queryset.filter(Q(created_by__id=user_id) | Q(
                    co_teacher__id=user_id))
            else:
                queryset = queryset.filter(Q(created_by__id=user_id) | Q(
                    co_teacher__id=user_id) | Q(
                        created_by__school=self.request.user.school,
                        share_with_school=True))

        return queryset.order_by('id').distinct()

    def create(self, request):
        user = request.user
        user.last_updated_on = datetime.now()
        user.save()

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        class_obj = Class.objects.get(id=serializer.data['id'])
        if request.data.get('co_teacher'):
            for pk in set(dict(request.data)['co_teacher']):
                co_teacher = User.objects.get(id=pk)
                if environ["TARA_APIS_ENVIRON"] not in ["JENKINS", ]:
                    if class_obj.created_by != co_teacher:
                        sendEmailToCoTeacher(class_obj, getDomain(), co_teacher)
        return Response(
            serializer.data, status.HTTP_201_CREATED
        )

    def partial_update(self, request, pk=None):
        user = request.user
        user.last_updated_on = datetime.now()
        user.save()

        classes_obj = Class.objects.all()
        class_obj = get_object_or_404(classes_obj, pk=pk)

        if request.data.get('name'):
            class_obj.name = request.data.get('name')
            class_obj.save()

        if request.data.get('co_teacher'):
            co_tech = class_obj.co_teacher.all()
            for i in request.data.get('co_teacher'):
                co_teacher = User.objects.get(id=i)
                if co_tech:
                    if co_teacher not in co_tech:
                        if environ["TARA_APIS_ENVIRON"] not in ["JENKINS", ]:
                            if class_obj.created_by != co_teacher:
                                sendEmailToCoTeacher(
                                    class_obj, getDomain(), co_teacher)
                else:
                    if environ["TARA_APIS_ENVIRON"] not in ["JENKINS", ]:
                        if class_obj.created_by != co_teacher:
                            sendEmailToCoTeacher(
                                class_obj, getDomain(), co_teacher)
        serializer = self.get_serializer(class_obj,
                                         data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        start_date = request.query_params.get('start_date')
        end_date = request.query_params.get('end_date')
        all_result = request.query_params.get('all_result')

        if (start_date and end_date) or all_result:
            serializer = self.get_serializer(queryset, many=True)
            return Response(serializer.data)
        else:
            page = self.paginate_queryset(queryset)
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)


class StandardViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing standard.
    create:
        Create a new standard instance.
    retrieve:
        Return the given standard.
    update:
        Update the given standard.
    partial_update:
        Update the given standard given field only.
    destroy:
        Delete the given standard.
    """
    permission_classes = (IsAuthenticated, StandardPermission)
    queryset = Standards.objects.all()
    serializer_class = StandardSerializer
    filter_backends = (filters.SearchFilter,)
    # search_fields = ('^name',)

    def get_queryset(self):
        name = self.request.query_params.get('search')
        if name:
            if self.request.user.state:
                return self.queryset.filter(Q(name__icontains=name,
                                              state=self.request.user.state) |
                                            Q(name__icontains=name,
                                              state__isnull=True))
            else:
                return self.queryset.filter(name__icontains=name)
        else:
            return self.queryset


class ContainerViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing container.
    create:
        Create a new container.
    retrieve:
        Return the given container.
    update:
        Update the given container.
    partial_update:
        Update the given container given field only.
    destroy:
        Delete the given container.
    """
    permission_classes = (IsAuthenticated, OnlyReadPermission)
    queryset = Container.objects.all()
    serializer_class = ContainerSerializer


class LearningIntelligenceViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing learning intelligence.
    create:
        Create a new learning intelligence.
    retrieve:
        Return the given learning intelligence.
    update:
        Update the given learning intelligence.
    partial_update:
        Update the given learning intelligence given field only.
    destroy:
        Delete the given learning intelligence.
    """
    permission_classes = (IsAuthenticated, OnlyAdministrativePermission)
    queryset = LearningIntelligence.objects.all()
    serializer_class = LearningIntelligenceSerializer


class ActivityViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing activity.
    create:
        Create a new activity.
    retrieve:
        Return the given activity.
    update:
        Update the given activity.
    partial_update:
        Update the given activity given field only.
    destroy:
        Delete the given activity.
    """
    permission_classes = (IsAuthenticated, FixActivityPermission)
    queryset = Activity.objects.all()
    serializer_class = GetActivitySerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    ordering_fields = ('tittle', )
    filterset_fields = ('created_by', 'subject__name', 'default_activity',
                        'archive')

    def get_serializer_class(self):

        serializer_class = self.serializer_class
        if self.request.method not in ['GET']:
            serializer_class = PostActivitySerializer
        return serializer_class

    def get_queryset(self):
        """
        Custom queryset for Activity
            - Admin users can create, view, update, and destroy Activity
              of their school only.
            - Teacher can create, update and read their own activity.
            - School Leader can only read the Activity of their school.
            - Department Coach can only read the Activity of their school.
            - staff user has access (CRUD) to every records
        """
        search_title = self.request.query_params.get('search')
        param = self.request.query_params.get('ordering')
        if not self.request.user.is_authenticated:
            queryset = Activity.objects.none()
        else:
            queryset = Activity.objects.filter(
                Q(created_by=self.request.user, archive=False) | Q(
                    default_activity=True, archive=False)).order_by('tittle')

            if param == 'default_activity':
                queryset = queryset.order_by('default_activity', 'tittle')

            if search_title:
                queryset = queryset.filter(tittle__icontains=search_title)

        return queryset

    def list(self, request):
        """
        Overriding list method for custom ordering of activity based on
            - most used activity
            - least used activity
        """
        queryset = self.filter_queryset(self.get_queryset())
        param = self.request.query_params.get('ordering')
        la = LessonActivity.objects.filter(lesson_plan__created_by=request.user)
        if la:
            if param == 'most_used':
                return Response(customSorting(queryset, la, 'tittle'))
            elif param == 'least_used':
                return Response(customSorting(queryset, la, '-tittle')[::-1])
            else:
                return super().list(request)
        else:
            return super().list(request)


def customSorting(queryset, la, title):
    """
    customSorting function will return most used activity list.
    """
    act_order = la.values('activity').annotate(
        c=Count('activity'), tittle=F('activity__tittle')
    ).order_by('-c', title)
    ids = []
    if act_order:
        for act in act_order:
            ids.append(act['activity'])
        items = queryset.in_bulk(ids)
        most_used = [GetActivitySerializer(items[x]).data for x in ids
                     if x in items.keys()]
        least_used = queryset.exclude(id__in=ids).order_by(title)
        for least in least_used:
            most_used.append(GetActivitySerializer(least).data)
        return most_used
    else:
        queryset = queryset.order_by(title)
        return [GetActivitySerializer(qs).data for qs in queryset]


class TemplateViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing template.
    create:
        Create a new template.
    retrieve:
        Return the given template.
    update:
        Update the given template.
    partial_update:
        Update the given template given field only.
    destroy:
        Delete the given template.
    """
    permission_classes = (IsAuthenticated, OnlyReadPermission)
    queryset = Template.objects.all()
    serializer_class = GetTemplateSerializer

    def get_serializer_class(self):

        serializer_class = self.serializer_class
        if self.request.method not in ['GET']:
            serializer_class = PostTemplateSerializer
        return serializer_class


class LessonPlanViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing lesson plan.
    create:
        Create a new lesson plan.
    retrieve:
        Return the given lesson plan.
    update:
        Update the given lesson plan.
    partial_update:
        Update the given lesson plan given field only.
    destroy:
        Delete the given lesson plan.
    """
    permission_classes = (IsAuthenticated, LessonPermission)
    queryset = (LessonPlan.objects.select_related(
        'created_by', 'lesson_class', 'last_updated_by'))
    serializer_class = GetLessonPlanSerializer
    pagination_class = LessonClassPagination
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('created_by__id', 'lesson_class__id',
                        'lesson_class__name')

    def get_serializer_class(self):

        serializer_class = self.serializer_class
        if self.request.method not in ['GET']:
            serializer_class = PostLessonPlanSerializer
        return serializer_class

    def get_queryset(self):
        """
        Custom queryset for Lesson Plan
            - Admin users can create, view, update, and destroy Lesson Plan of
              their school only.
            - Teacher can create, update and read and delete their own
              lesson plan.
            - School Leader can only read the Lesson of their school.
            - Department Coach can create, update read and delete the Lesson
              Plan of their school.
            - staff user has access (CRUD) to every records
        """

        if not self.request.user.is_authenticated:
            queryset = LessonPlan.objects.none()
        else:
            get_user_grp = User.objects.filter(
                username=self.request.user
            ).values_list('groups__name', flat=True).order_by('groups__name')

            queryset = LessonPlan.objects.select_related(
                'template', 'created_by', 'lesson_class', 'last_updated_by').\
                prefetch_related(
                'standard', 'elements__standards', 'template__container')

            if self.request.user.is_staff or 'administrator' in get_user_grp:
                queryset = queryset

            elif 'cluster_administrator' in get_user_grp:
                schools = ClusterAdminSchool.objects.filter(
                    user=self.request.user)
                if schools:
                    queryset = queryset.filter(Q(
                        created_by__school__in=schools[0].school.all()) | Q(
                            lesson_class__created_by=self.request.user) | Q(
                                lesson_class__co_teacher=self.request.user))
                else:
                    queryset = queryset.filter(Q(
                        created_by=self.request.user) | Q(
                            lesson_class__created_by=self.request.user) | Q(
                        lesson_class__co_teacher=self.request.user))

            elif 'school_leader' in get_user_grp:
                queryset = queryset.filter(Q(
                    created_by__school=self.request.user.school,
                    created_by__groups__name__in=['teacher', 'school_leader']
                ) | Q(lesson_class__co_teacher=self.request.user) | Q(
                    lesson_class__created_by=self.request.user))

            elif 'department_coach' in get_user_grp:
                queryset = queryset.filter(Q(
                    created_by__school=self.request.user.school,
                    created_by__groups__name__in=['teacher', 'department_coach']
                ) | Q(lesson_class__co_teacher=self.request.user) | Q(
                    lesson_class__created_by=self.request.user))

            elif 'teacher' in get_user_grp:
                queryset = queryset.filter(Q(
                    created_by=self.request.user,) | Q(
                        lesson_class__co_teacher=self.request.user) | Q(
                    lesson_class__created_by=self.request.user) | Q(
                        created_by__school=self.request.user.school,
                        created_by__groups__name__in=['teacher', ]))

        return queryset.order_by('-id').distinct()

    def perform_create(self, serializer):
        serializer.save(last_updated_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(last_updated_by=self.request.user)


class GetLessonPlanViewSet(generics.ListAPIView):
    """
        Return the given lesson plan on slug.
    """
    permission_classes = (IsAuthenticated, )
    serializer_class = GetLessonPlanSerializer

    def get_queryset(self):
        slug = self.kwargs['slug']
        return LessonPlan.objects.filter(slug=slug)


class RateLessonViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing rate lesson.
    create:
        Create a new rate lesson.
    retrieve:
        Return the given rate lesson.
    update:
        Update the given rate lesson.
    partial_update:
        Update the given rate lesson given field only.
    destroy:
        Delete the given rate lesson.
    """
    permission_classes = (IsAuthenticated, RateLessonPermission,)
    queryset = RateLesson.objects.all()
    serializer_class = RateLessonSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('lesson_plan__id', 'lesson_plan__tittle',
                        'rated_by__id')

    def get_queryset(self):
        """
        Custom queryset for Rate Lesson
            - Admin users can create, view, update, and destroy rating of
              their school only.
            - Teacher can create, update and read their own rating.
            - School Leader can only read the rating of their school.
            - Department Coach can only read the rating of their school.
            - staff user has access (CRUD) to every records
        """

        if not self.request.user.is_authenticated:
            queryset = RateLesson.objects.none()
        else:
            get_user_grp = User.objects.filter(
                username=self.request.user
            ).values_list('groups__name', flat=True).order_by('groups__name')

            if self.request.user.is_staff or\
                    'administrator' in get_user_grp[0]:
                queryset = RateLesson.objects.all()

            elif 'cluster_administrator' in get_user_grp:
                queryset = RateLesson.objects.filter(
                    rated_by=self.request.user
                )

            elif get_user_grp[0] == 'teacher':
                queryset = RateLesson.objects.filter(
                    rated_by=self.request.user
                )
            else:
                queryset = RateLesson.objects.filter(
                    rated_by__school=self.request.user.school
                )

        return queryset


class ActivityContainerViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing activity container.
    create:
        Create a new activity container.
    retrieve:
        Return the given activity container.
    update:
        Update the given activity container.
    partial_update:
        Update the given activity container given field only.
    destroy:
        Delete the given activity container.
    """
    permission_classes = (IsAuthenticated,)
    queryset = ActivityContainer.objects.all()
    serializer_class = GetActivityContainerSerializer

    def get_serializer_class(self):

        serializer_class = self.serializer_class
        if self.request.method not in ['GET']:
            serializer_class = PostActivityContainerSerializer
        return serializer_class


class TagElementViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing tag element.
    create:
        Create a new tag element.
    retrieve:
        Return the given tag element.
    update:
        Update the given tag element.
    partial_update:
        Update the given tag element given field only.
    destroy:
        Delete the given tag element.
    """
    permission_classes = (IsAuthenticated, TagElementPermission)
    queryset = TagElement.objects.all()
    serializer_class = TagElementSerializer


class AttachmentViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing attachments.
    create:
        Create a new attachment.
    retrieve:
        Return the given attachment.
    update:
        Update the given attachment.
    partial_update:
        Update the given attachment given field only.
    destroy:
        Delete the given attachment.
    """
    permission_classes = (IsAuthenticated,)
    queryset = Attachment.objects.all()
    serializer_class = AttachmentSerializer


class LessonActivityViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing activity viewset.
    create:
        Create a new lesson activity.
    retrieve:
        Return the given lesson activity.
    update:
        Update the given lesson activity.
    partial_update:
        Update the given lesson activity given field only.
    destroy:
        Delete the given lesson activity.
    """
    permission_classes = (IsAuthenticated, LessonActivityPermission)
    queryset = LessonActivity.objects.all()
    serializer_class = GetLessonActivitySerializer

    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('lesson_plan__id', 'lesson_plan__tittle')

    def get_serializer_class(self):

        serializer_class = self.serializer_class
        if self.request.method not in ['GET']:
            serializer_class = PostLessonActivitySerializer
        return serializer_class


class ActivityCommentViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing activity comment.
    create:
        Create a new activity comment.
    retrieve:
        Return the given activity comment.
    update:
        Update the given activity comment.
    partial_update:
        Update the given activity comment given field only.
    destroy:
        Delete the given activity comment
    """
    permission_classes = (IsAuthenticated, ActivityCommentPermission)
    queryset = ActivityComment.objects.all()
    serializer_class = GetActivityCommentSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('lesson_activity__lesson_plan__id',
                        'lesson_activity__lesson_plan__tittle')

    def get_serializer_class(self):

        serializer_class = self.serializer_class
        if self.request.method not in ['GET']:
            serializer_class = PostActivityCommentSerializer
        return serializer_class

    def create(self, request):
        activity_comment = ActivityComment()
        obj = LessonActivity.objects.get(id=request.data.get('lesson_activity'))
        activity_comment.comment = request.data.get('comment')
        activity_comment.commented_by = self.request.user
        if request.data.get('reviewed'):
            activity_comment.reviewed = True
        else:
            activity_comment.reviewed = False
        activity_comment.lesson_activity = obj
        activity_comment.created_on = datetime.now()
        activity_comment.save()
        return Response(
            GetActivityCommentSerializer(activity_comment).data,
            status.HTTP_201_CREATED
        )


class ActivityTemplateViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing activity template.
    """
    permission_classes = (IsAuthenticated, OnlyReadPermission)
    queryset = ActivityTemplate.objects.all()
    serializer_class = ActivityTemplateSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter,)
    filterset_fields = ('activity__id', 'activity__tittle')
    ordering_fields = ('name',)


class SchoolDocumentViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing school documents.
    create:
        Create a new school document.
    retrieve:
        Return the given school document.
    update:
        Update the given school document.
    partial_update:
        Update the given school document given field only.
    destroy:
        Delete the given school document.
    """
    permission_classes = (IsAuthenticated, OnlyAdministrativePermission)
    queryset = SchoolDocument.objects.all()
    serializer_class = SchoolDocumentSerializer


class StateViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing State.
    create:
        Create a new State.
    retrieve:
        Return the given State.
    update:
        Update the given State.
    partial_update:
        Update the given State given field only.
    destroy:
        Delete the given State.
    """
    permission_classes = (IsAuthenticated, )
    queryset = State.objects.all().order_by('name')
    serializer_class = StateSerializer


class SchoolTypeViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing School Type.
    create:
        Create a new School Type.
    retrieve:
        Return the given School Type.
    update:
        Update the given School Type.
    partial_update:
        Update the given School Type given field only.
    destroy:
        Delete the given School Type.
    """
    permission_classes = (IsAuthenticated,)
    queryset = SchoolType.objects.all()
    serializer_class = SchoolTypeSerailizer


class CalenderViewset(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing Calender.
    create:
        Create a new Calender.
    retrieve:
        Return the given Calender.
    update:
        Update the given Calender.
    partial_update:
        Update the given Calender Type given field only.
    destroy:
        Delete the given Calender.
    """
    permission_classes = (IsAuthenticated,)
    queryset = Calender.objects.all()
    serializer_class = CalenderSerializers

    def create(self, request, *args, **kwargs):
        Calender.objects.filter(
            lesson__id=request.data['lesson'], date__gte=date.today()
        ).delete()
        date_list = request.data['date']
        calendar_list = []
        for dates in date_list:
            if dates >= datetime.strftime(date.today(), '%Y-%m-%d'):
                calendar = Calender.objects.create(
                    date=dates, lesson=LessonPlan.objects.get(
                        id=request.data['lesson']))
                calendar_list.append(calendar.id)

        serializer = CalenderSerializers(
            Calender.objects.filter(id__in=calendar_list), many=True)

        return Response(serializer.data, status.HTTP_201_CREATED)


class StrategyGroupViewset(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing StrategyGroup.
    create:
        Create a new StrategyGroup.
    retrieve:
        Return the given StrategyGroup.
    update:
        Update the given StrategyGroup.
    partial_update:
        Update the given StrategyGroup given field only.
    destroy:
        Delete the given StrategyGroup.
    """
    permission_classes = (IsAuthenticated, OnlyReadPermission)
    queryset = StrategyGroup.objects.all()
    serializer_class = StrategyGroupSerializers
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('activity__subject__name',)

    def get_queryset(self):
        queryset = StrategyGroup.objects.all()

        if self.request.user.school:
            if self.request.user.school.strategy_group.all():
                queryset = queryset.filter(
                    id__in=self.request.user.school.strategy_group.all())

        return queryset

    def list(self, request, *args, **kwargs):
        querylist = []
        activity_list = []
        queryset = self.filter_queryset(self.get_queryset())
        act_sub = request.query_params.get('activity__subject__name')
        param = self.request.query_params.get('ordering')
        search_title = self.request.query_params.get('search')

        if request.user.school:
            if request.user.school.strategy_group.all():
                self_queryset = Activity.objects.filter(created_by=request.user)

                for activity in queryset:
                    for act in activity.activity.all():
                        activity_list.append(act.id)
                for self_act in self_queryset:
                    activity_list.append(self_act.id)

                activity_list = Activity.objects.filter(
                    id__in=activity_list).order_by('tittle')

                if param == 'tittle':
                    activity_list = activity_list

                if param == 'default_activity':
                    activity_list = activity_list.order_by(Case(When(
                        created_by=request.user, then=0), default=1), 'tittle')

                la = LessonActivity.objects.filter(
                    lesson_plan__created_by=request.user)
                if act_sub:
                    activity_list = activity_list.filter(subject__name=act_sub)
                if search_title:
                    activity_list = activity_list.filter(
                        tittle__icontains=search_title)

                if param == 'most_used':
                    return Response(customSorting(activity_list, la, 'tittle'))
                if param == 'least_used':
                    return Response(customSorting(
                        activity_list, la, '-tittle')[::-1])

                for activity in activity_list:
                    querylist.append(GetActivitySerializer(activity).data)

                return Response(querylist)
            else:
                return Response(None)
        else:
            return super().list(request, *args, **kwargs)


class ObservationTypeViewset(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing Type.
    create:
        Create a new Type.
    retrieve:
        Return the given Type.
    update:
        Update the given Type.
    partial_update:
        Update the given Type given field only.
    destroy:
        Delete the given Type.
    """
    permission_classes = (IsAuthenticated, OnlyReadPermission)
    queryset = ObservationType.objects.all()
    serializer_class = ObservationTypeSerializers


class ObservationViewset(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing Observation.
    create:
        Create a new Observation.
    retrieve:
        Return the given Observation.
    update:
        Update the given Observation.
    partial_update:
        Update the given Observation given field only.
    destroy:
        Delete the given Observation.
    """
    permission_classes = (IsAuthenticated, ObservationPermission)
    queryset = Observation.objects.all()
    serializer_class = ObservationSerializers

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return RetriveObservationSerializers
        else:
            return super().get_serializer_class()

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.user == instance.lesson_plan.created_by:
            if not instance.read:
                instance.read = True
                instance.save()
        return super().retrieve(request, *args, **kwargs)

    def get_queryset(self):
        """
        Override get_queryset for
            - Administrator can return all data
            - cluster_administrator can read observation of schools they belongs
            - school_leader can read observation of depatment_coach, teacher of
              his own school and his own observation and his lessons observation
            - depatment_coach can read observation of teacher of his school and
              his own observation and his lessons observation
            - Teacher can read his lessons observation
        """
        user_group = User.objects.filter(
            username=self.request.user
        ).values_list('groups__name', flat=True).order_by('groups__name')
        queryset = Observation.objects.all()

        if (self.request.user.is_staff or 'administrator' in user_group):
            queryset = queryset

        elif 'cluster_administrator' in user_group:
            schools = ClusterAdminSchool.objects.filter(user=self.request.user)
            if schools:
                queryset = queryset.filter(Q(
                    created_by__school__in=schools[0].school.all()) | Q(
                        created_by=self.request.user) | Q(
                            lesson_plan__created_by=self.request.user))
            else:
                queryset = queryset.filter(Q(created_by=self.request.user) | Q(
                    lesson_plan__created_by=self.request.user))

        elif 'school_leader' in user_group:
            queryset = queryset.filter(
                Q(created_by__school=self.request.user.school,
                  created_by__groups__name__in=['school_leader',
                                                'department_coach',
                                                'teacher']) | Q(
                    created_by=self.request.user) | Q(
                    lesson_plan__lesson_class__created_by=self.request.user) | Q(
                    lesson_plan__lesson_class__co_teacher=self.request.user
                ))

        elif 'department_coach' in user_group:
            queryset = queryset.filter(
                Q(created_by__school=self.request.user.school,
                  created_by__groups__name__in=['department_coach',
                                                'teacher']) | Q(
                    created_by=self.request.user) | Q(
                    lesson_plan__lesson_class__created_by=self.request.user) | Q(
                    lesson_plan__lesson_class__co_teacher=self.request.user))

        elif user_group[0] == 'teacher':
            queryset = queryset.filter(Q(created_by=self.request.user) | Q(
                lesson_plan__lesson_class__created_by=self.request.user) |
                Q(lesson_plan__lesson_class__co_teacher=self.request.user) | Q(
                    lesson_plan__created_by=self.request.user))

        return queryset.distinct()

    def perform_create(self, serializer):
        serializer.save()
        lesson_plan = LessonPlan.objects.get(
            id=self.request.data['lesson_plan'])
        if not (int(self.request.data['created_by']) == int(
                lesson_plan.created_by.id)):
            if not self.request.data['save_for_later']:
                lesson_observation_email(lesson_plan, getDomain())

    def perform_update(self, serializer):
        serializer.save()
        lesson_plan = LessonPlan.objects.get(
            id=self.request.data['lesson_plan'])
        if not (int(self.request.data['created_by']) == int(
                lesson_plan.created_by.id)):
            if not self.request.data['save_for_later']:
                lesson_observation_email(lesson_plan, getDomain())


class SmartGuideViewset(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing SmartGuide.
    create:
        Create a new SmartGuide.
    retrieve:
        Return the given SmartGuide.
    update:
        Update the given SmartGuide.
    partial_update:
        Update the given SmartGuide given field only.
    destroy:
        Delete the given SmartGuide.
    """
    permission_classes = (IsAuthenticated, OnlyReadPermission)
    queryset = SmartGuide.objects.all()
    serializer_class = SmartGuideSerializers

    def get_queryset(self):
        if self.request.user.groups.all():
            if 'cluster_administrator' in self.request.user.groups.all().\
                    order_by('name').values_list('name', flat=True)[0]:
                if self.request.user.school:
                    queryset = SmartGuide.objects.filter(
                        school=self.request.user.school)
                    return queryset
                else:
                    schools = ClusterAdminSchool.objects.filter(
                        user=self.request.user)
                    if schools:
                        queryset = SmartGuide.objects.filter(
                            school=schools[0].school.all()[0])
                        return queryset
                    else:
                        return None
            else:
                if self.request.user.school:
                    queryset = SmartGuide.objects.filter(
                        school=self.request.user.school)
                    return queryset
                else:
                    return None
        else:
            if self.request.user.school:
                queryset = SmartGuide.objects.filter(
                    school=self.request.user.school)
                return queryset
            else:
                return None


class EvaluationTypeViewset(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing EvaluationType.
    create:
        Create a new EvaluationType with login user school.
    retrieve:
        Return the given EvaluationType.
    update:
        Update the given EvaluationType.
    partial_update:
        Update the given EvaluationType given field only.
    destroy:
        Delete the given EvaluationType.
    """
    permission_classes = (IsAuthenticated, OnlyReadPermission)
    queryset = EvaluationType.objects.all().order_by('id')
    serializer_class = EvaluationTypeSerializers
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('school__name', 'school__id')

    def create(self, request, *args, **kwargs):
        if request.user.school:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.validated_data['school'] = request.user.school
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED,
                            headers=headers)
        else:
            return Response({
                'message': 'your school not set, please add your school'})

    def get_queryset(self):

        return EvaluationType.objects.filter(Q(default_type=True) | Q(
            school=self.request.user.school)).distinct().order_by('id')


class EvaluationChoicesViewset(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing EvaluationChoices.
    create:
        Create a new EvaluationChoices.
    retrieve:
        Return the given EvaluationChoices.
    update:
        Update the given EvaluationChoices.
    partial_update:
        Update the given EvaluationChoices given field only.
    destroy:
        Delete the given EvaluationChoices.
    """
    permission_classes = (IsAuthenticated, OnlyReadPermission)
    queryset = EvaluationChoices.objects.all()
    serializer_class = EvaluationChoicesSerializers


class EvaluateLessonObservationViewset(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing EvaluateLessonObservation.
    create:
        Create a new EvaluateLessonObservation.
    retrieve:
        Return the given EvaluateLessonObservation.
    update:
        Update the given EvaluateLessonObservation.
    partial_update:
        Update the given EvaluateLessonObservation given field only.
    destroy:
        Delete the given EvaluateLessonObservation.
    """
    permission_classes = (IsAuthenticated, EvaluateLessonPermission)
    queryset = EvaluateLessonObservation.objects.all()
    serializer_class = EvaluateLessonObservationSerializers


class EvaluationCriteriaGroupViewset(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing EvaluationCriteriaGroup.
    create:
        Create a new EvaluationCriteriaGroup.
    retrieve:
        Return the given EvaluationCriteriaGroup.
    update:
        Update the given EvaluationCriteriaGroup.
    partial_update:
        Update the given EvaluationCriteriaGroup given field only.
    destroy:
        Delete the given EvaluationCriteriaGroup.
    """
    permission_classes = (IsAuthenticated, )
    queryset = EvaluationCriteriaGroup.objects.filter(archive=False)
    serializer_class = EvaluationCriteriaGroupSerializers

    def get_queryset(self):
        queryset = EvaluationCriteriaGroup.objects.all()

        if self.request.user.school:
            if self.request.user.school.evaluation_criteria_group.all():
                queryset = self.request.user.school.evaluation_criteria_group.\
                    all()

                return queryset

    def list(self, request, *args, **kwargs):
        querylist = []
        eval_type_list = []
        queryset = self.filter_queryset(self.get_queryset())

        if queryset:
            if request.user.school:
                for eval_type in queryset.filter(archive=False):
                    for qslist in eval_type.evaluation_type.all():
                        eval_type_list.append(EvaluationTypeSerializers(qslist).data)

                return Response(eval_type_list)
            else:
                return Response(None)
        else:
            return Response(None)


class GetSchoolLessonTemplateAPIView(APIView):
    """
    View to list of templates assign to users school.
    """
    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):

        if request.user.school:
            if request.user.school.template.all():
                queryset = request.user.school.template.all()
                return Response(
                    [GetTemplateSerializer(qs).data for qs in queryset])
            else:
                return Response(None)
        else:
            return Response(None)


def create_element(name):
    return OxmlElement(name)


def create_attribute(element, name, value):
    element.set(qn(name), value)


def add_page_number(run):
    fldChar1 = create_element('w:fldChar')
    create_attribute(fldChar1, 'w:fldCharType', 'begin')

    instrText = create_element('w:instrText')
    create_attribute(instrText, 'xml:space', 'preserve')
    instrText.text = "PAGE"

    fldChar2 = create_element('w:fldChar')
    create_attribute(fldChar2, 'w:fldCharType', 'end')

    run._r.append(fldChar1)
    run._r.append(instrText)
    run._r.append(fldChar2)


def add_hyperlink(paragraph, text, url, color, underline):
    """
    A function that places a hyperlink within a paragraph object.

    :param paragraph: The paragraph we are adding the hyperlink to.
    :param url: A string containing the required url
    :param text: The text displayed for the url
    :return: The hyperlink object
    """

    if 'http' not in url:
        url = 'http://' + url
    # This gets access to the document.xml.rels file and gets a new relation id value
    part = paragraph.part
    r_id = part.relate_to(url, docx.opc.constants.RELATIONSHIP_TYPE.HYPERLINK,
                          is_external=True)

    # Create the w:hyperlink tag and add needed values
    hyperlink = docx.oxml.shared.OxmlElement('w:hyperlink')
    hyperlink.set(docx.oxml.shared.qn('r:id'), r_id, )

    # Create a w:r element
    new_run = docx.oxml.shared.OxmlElement('w:r')

    # Create a new w:rPr element
    rPr = docx.oxml.shared.OxmlElement('w:rPr')

    # Add color if it is given
    if not color is None:
        c = docx.oxml.shared.OxmlElement('w:color')
        c.set(docx.oxml.shared.qn('w:val'), color)
        rPr.append(c)

    # Remove underlining if it is requested
    if not underline:
        u = docx.oxml.shared.OxmlElement('w:u')
        u.set(docx.oxml.shared.qn('w:val'), 'none')
        rPr.append(u)

    # Join all the xml elements together add add the required text to the w:r element
    new_run.append(rPr)
    new_run.text = text.replace('&nbsp;', ' ').replace('\\r\\n', '').replace(
        '&amp;', '&')
    hyperlink.append(new_run)

    paragraph._p.append(hyperlink)

    return hyperlink


def highlight_color(rgb):
    """
    Function return the text highlighting color 
    """

    if rgb == 'white':
        return WD_COLOR_INDEX.WHITE

    if rgb == 'black':
        return WD_COLOR_INDEX.BLACK

    if rgb == 'red':
        return WD_COLOR_INDEX.RED

    if rgb == 'darkred':
        return WD_COLOR_INDEX.DARK_RED

    if rgb == 'green':
        return WD_COLOR_INDEX.GREEN

    if rgb == 'blue':
        return WD_COLOR_INDEX.BLUE

    if rgb == 'darkblue':
        return WD_COLOR_INDEX.DARK_BLUE

    if rgb == 'yellow':
        return WD_COLOR_INDEX.YELLOW

    if rgb == 'gray':
        return WD_COLOR_INDEX.GRAY_50

    if rgb == 'teal':
        return WD_COLOR_INDEX.TEAL

    if rgb == 'turquoise':
        return WD_COLOR_INDEX.TURQUOISE

    if [255, 0, 255] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.PINK

    if [128, 0, 128] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.VIOLET

    if [102, 255, 0] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.BRIGHT_GREEN

    if [155, 135, 12] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.DARK_YELLOW
    # custom colors
    if [61, 20, 102] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.DARK_BLUE
    if [0, 41, 102] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.BLUE
    if [0, 55, 0] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.GREEN
    if [102, 102, 0] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.DARK_YELLOW
    if [102, 61, 0] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.DARK_YELLOW
    if [92, 0, 0] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.DARK_RED
    if [68, 68, 68] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.GRAY_50
    if [107, 36, 178] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.BLUE
    if [0, 71, 178] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.BLUE
    if [0, 97, 0] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.GREEN
    if [178, 178, 0] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.DARK_YELLOW
    if [178, 107, 0] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.DARK_YELLOW
    if [161, 0, 0] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.RED
    if [136, 136, 136] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.GRAY_50
    if [194, 133, 255] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.BLUE
    if [102, 163, 224] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.BLUE
    if [102, 185, 102] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.BRIGHT_GREEN
    if [255, 255, 102] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.YELLOW
    if [255, 194, 102] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.YELLOW
    if [240, 102, 102] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.RED
    if [187, 187, 187] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.GRAY_50
    if [235, 214, 255] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.VIOLET
    if [204, 224, 245] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.BLUE
    if [204, 232, 204] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.BRIGHT_GREEN
    if [255, 255, 204] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.YELLOW
    if [255, 235, 204] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.DARK_YELLOW
    if [250, 204, 204] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.VIOLET
    if [153, 51, 255] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.BLUE
    if [0, 102, 204] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.BLUE
    if [0, 138, 0] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.GREEN
    if [255, 255, 0] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.YELLOW
    if [255, 153, 0] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.DARK_YELLOW
    if [230, 0, 0] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.RED
    if [0, 0, 0] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.BLACK
    if [68, 68, 68] == [int(rgb[0]), int(rgb[1]), int(rgb[2])]:
        return WD_COLOR_INDEX.GRAY_50
    # end custom colors
    else:
        return WD_COLOR_INDEX.AUTO


def html_code(p, html, li_cnt=''):
    """
    Function takes raw html code and formates the word document accordingly
    """

    html = html.split('<')
    html = [html[0]] + ['<' + l for l in html[1:]]
    tags = []
    color = {}
    run_cnt = 0
    for run in html:
        try:
            if run:
                tag_change = re.match('(?:<)(.*?)(?:>)', run)
                if tag_change != None:
                    tag_strip = tag_change.group(0)
                    tag_change = tag_change.group(1)
                    styles = tag_change.split('style=')  # get `css` style
                    allingment = tag_change.split('class=')  # get allingment class

                    if len(styles) >= 2:
                        styles = styles[1].split(';')
                    for style in styles:
                        if 'background-color' in style:
                            if len(style.split(': ')) >= 2:
                                color['background-color'] = style.split(': ')[1]
                        elif 'color' in style:
                            if len(style.split(': ')) >= 2:
                                color['color'] = style.split(': ')[1]

                    # Setting paragraph allingment
                    if len(allingment) >= 2:
                        allingment = allingment[1].replace('"', '')
                    if allingment == 'ql-align-center':
                        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
                    elif allingment == 'ql-align-right':
                        p.alignment = WD_ALIGN_PARAGRAPH.RIGHT
                    elif allingment == 'ql-align-justify':
                        p.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY

                    if not(tag_change.startswith('/a')
                           or tag_change.startswith('a ')):
                        tag_change = tag_change.split(' ')[0]
                    if tag_change.startswith('/'):
                        if tag_change.startswith('/a'):
                            tag_change = next(
                                tag for tag in tags if tag.startswith('a '))
                        tag_change = tag_change.strip('/')
                        if tag_change in tags:
                            tags.remove(tag_change)
                    else:
                        tags.append(tag_change)
                else:
                    tag_strip = ''
                hyperlink = [tag for tag in tags if tag.startswith('a ')]
                if run.startswith('<'):
                    run = run.replace(tag_strip, '')
                    if hyperlink:
                        hyperlink = hyperlink[0].replace('\\', '')
                        hyperlink = re.match('.*?(?:href=")(.*?)(?:").*?',
                                             hyperlink)
                        if hyperlink:
                            hyperlink = hyperlink.group(1)
                        if run:
                            run_cnt += 1
                            if run_cnt == 1:
                                run = li_cnt + run
                        hyperlink = add_hyperlink(p, run, hyperlink, '0000FF',
                                                  True)
                    else:
                        if run:
                            run_cnt += 1
                            if run_cnt == 1:
                                run = li_cnt + run
                        runner = p.add_run(run.replace('&nbsp;', ' ').replace(
                            '\\r\\n', '').replace('&amp;', '&').replace(
                                '&lt;', '<').replace('&gt;', '>'))
                        if 'strong' in tags:
                            runner.bold = True
                        if 'u' in tags:
                            runner.underline = True
                        if 'em' in tags:
                            runner.italic = True
                        if 'h1' in tags:
                            runner.font.size = Pt(24)
                        if 'h2' in tags:
                            runner.font.size = Pt(22)
                        if 's' in tags:
                            runner.font.strike = True
                        try:
                            if color.keys():
                                try:
                                    if 'background-color' in color.keys():
                                        if run:
                                            if color['background-color'] ==\
                                                    'transparent':
                                                pass
                                            elif 'rgb' in color[
                                                    'background-color']:
                                                rgb = color[
                                                    'background-color'].replace(
                                                    'rgb(', '').replace(
                                                        ')', '').split(',')
                                                if rgb:
                                                    runner.font.\
                                                        highlight_color = \
                                                        highlight_color(rgb)
                                            else:
                                                if not color[
                                                    'background-color'].\
                                                        startswith('#'):
                                                    runner.font.\
                                                        highlight_color =\
                                                        highlight_color(
                                                            color['background-color'])
                                except Exception:
                                    pass
                                try:
                                    if 'color' in color.keys():
                                        if run:
                                            if 'rgba' in color['color']:
                                                rgba = color['color'].replace(
                                                    'rgba(', '').replace(
                                                        ')', '').split(',')
                                                if rgba:
                                                    runner.font.color.rgb =\
                                                        RGBColor(int(rgba[0]),
                                                                 int(rgba[1]),
                                                                 int(rgba[2]))

                                            elif 'rgb' in color['color']:
                                                rgb = color['color'].replace(
                                                    'rgb(', '').replace(
                                                        ')', '').split(',')
                                                if rgb:
                                                    runner.font.color.rgb =\
                                                        RGBColor(int(rgb[0]),
                                                                 int(rgb[1]),
                                                                 int(rgb[2]))
                                except Exception:
                                    pass
                        except Exception:
                            pass
                else:
                    if run:
                        run_cnt += 1
                        if run_cnt == 1:
                            run = li_cnt + run
                    p.add_run(run.replace('&nbsp;', ' ').replace(
                        '\\r\\n', '').replace('&amp;', '&').replace('&lt;', '<').
                        replace('&gt;', '>'))
        except Exception:
            pass


def download_docx(request):
    """
    This function will download docx file
    """
    document = Document()
    font = document.styles['Normal'].font
    font.name = 'Arial'
    font.size = Pt(10)
    add_page_number(document.sections[0].header.paragraphs[0].add_run(
        '                                                                                                                                                                               '))
    footer = document.sections[0].footer
    foot = footer.add_paragraph()
    foot.add_run('www.taraedtech.com')
    foot.add_run('                                                                                                          ')
    path = os.path.dirname(os.path.dirname(settings.BASE_DIR))
    foot.add_run().add_picture(path + '/static/logo.png', width=Inches(1.03),
                               height=Inches(0.31))
    lesson_id = request.GET.get('lesson_id')
    with_comment = request.GET.get('with_comment')
    queryset = LessonActivity.objects.none()
    if lesson_id == None:
        return HttpResponseForbidden()
    try:
        lesson_plan = LessonPlan.objects.get(id=lesson_id)
        qs = LessonActivity.objects.filter(lesson_plan=lesson_plan).\
            order_by('container__id')
        if lesson_plan is not None:
            docx_title = str(lesson_plan.tittle.replace(" ", "_"))\
                + '.docx'
        para = document.add_paragraph()
        if lesson_plan.created_by.first_name:
            fname = para.add_run(lesson_plan.created_by.first_name)
            para.add_run(" ")
        if lesson_plan.created_by.last_name:
            lname = para.add_run(lesson_plan.created_by.last_name)
        if lesson_plan.lesson_class:
            document.add_paragraph(lesson_plan.lesson_class.name)
            document.add_paragraph()
        p = document.add_paragraph()
        p.add_run(lesson_plan.tittle).bold = True
        pp = document.add_paragraph()
        pp.add_run('Subject : ').bold = True

        if qs:
            if qs[0].activity.subject:
                pp.add_run(qs[0].activity.subject.name)

        if lesson_plan.standard.all():
            for std in lesson_plan.standard.all():
                q = document.add_paragraph()
                q.add_run(std.name).bold = True
                q.add_run(':').bold = True
                if std.description:
                    document.add_paragraph(std.description.replace('\n', ' '))
                elements = lesson_plan.elements.all()
                if elements:
                    for element in elements:
                        if std == element.standards:
                            ele = document.add_paragraph(element.element)
                            ele.paragraph_format.left_indent = Pt(24)

        document.add_paragraph(
            "_____________________________________________________________________________")
        document.add_paragraph().add_run('Objective: ').bold = True
        if lesson_plan.objective:
            document.add_paragraph(lesson_plan.objective)
        p1 = document.add_paragraph()
        p1.add_run('Template Type: ').bold = True
        document.add_paragraph("_____________________________________________________________________________")
        document.add_paragraph()
        if lesson_plan.template:
            p1.add_run(lesson_plan.template.name).bold = False
        if qs:
            count = 0
            if lesson_plan.template:
                for container in lesson_plan.template.container.all():
                    count += 1
                    for activity in qs:
                        if activity.container == container:

                            p2 = document.add_paragraph()
                            if lesson_plan.template:
                                if lesson_plan.template.name != 'Blank Template':
                                    if activity.container:
                                        run = p2.add_run(activity.container.title)
                                        run.bold = True
                                        run.underline = True
                                else:
                                    act = 'Strategy ' + str(count)
                                    run = p2.add_run(act)
                                    run.bold = True
                                    run.underline = True

                            p3 = document.add_paragraph()
                            if activity.activity:
                                p3.add_run(activity.activity.tittle).italic = True
                                p3.add_run('               ')
                                if activity.set_duration:
                                    set_dur = str(activity.set_duration) + ' mins'
                                else:
                                    set_dur = '0 mins'
                                p3.add_run(set_dur).italic = True
                                p3.add_run('               ')
                                if activity.set_grouping:
                                    p3.add_run(activity.set_grouping.title().
                                               replace("_", " ")).italic = True
                                else:
                                    p3.add_run('No Grouping').italic = True

                            if activity.description:
                                for descr in re.split(
                                        '</p><p>|<p><br>|</p>|</h1>|</h2>|</ol>|</ul>',
                                        activity.description):
                                    if '<ol>' in descr:
                                        li_cnt = 0
                                        for desc in descr.split('</li>'):
                                            if desc:
                                                li_cnt += 1
                                                spacing = document.\
                                                    add_paragraph(
                                                        style='List Paragraph')
                                                html_code(spacing, desc,
                                                          str(li_cnt) + '. ')
                                    elif '<ul>' in descr:
                                        for desc in descr.split('</li>'):
                                            if desc:
                                                spacing = document.\
                                                    add_paragraph(
                                                        style='List Bullet')
                                                html_code(spacing, desc)
                                    else:
                                        spacing = document.add_paragraph()
                                        html_code(spacing, descr)
                                    spacing.paragraph_format.line_spacing =\
                                        WD_LINE_SPACING.AT_LEAST = 0.9

                            counting = 0
                            if activity.attachment.all():
                                # retriving attachment
                                p4 = document.add_paragraph()
                                p4.add_run('Attachments:').bold = True
                                for attachment in activity.attachment.all():
                                    counting += 1
                                    attachment = str(attachment.attachment).\
                                        split('/')[1]
                                    document.add_paragraph(
                                        str(counting) + '.' + attachment.split('.')[0],)
                            if with_comment:
                                # Retriving comment
                                comments = ActivityComment.objects.filter(
                                    lesson_activity=activity)
                                cmt = document.add_paragraph()
                                cmt.add_run('Comments').bold = True
                                for comment in comments:
                                    cmts = document.add_paragraph()
                                    if comment.commented_by:
                                        cmts.add_run(
                                            comment.commented_by.first_name
                                        ).italic = True
                                        cmts.add_run(' ').italic = True
                                        cmts.add_run(
                                            comment.commented_by.last_name
                                        ).italic = True
                                        cmts.add_run(': ').italic = True
                                    cmts.add_run(comment.comment)
                            document.add_paragraph(
                                "_____________________________________________________________________________")
        if lesson_plan.differentiation:
            p5 = document.add_paragraph()
            run = p5.add_run('Differentiation')
            run.bold = True
            run.underline = True
            for diff in re.split('</p><p>|<p><br>|</p>|</h1>|</h2>|</ol>|</ul>',
                                 lesson_plan.differentiation):
                if '<ol>' in diff:
                    li_cnt = 0
                    for desc in diff.split('</li>'):
                        if desc:
                            li_cnt += 1
                            spacing = document.add_paragraph(
                                style='List Paragraph')
                            html_code(spacing, desc, str(li_cnt) + '. ')
                elif '<ul>' in diff:
                    for desc in diff.split('</li>'):
                        if desc:
                            spacing = document.add_paragraph(
                                style='List Bullet')
                            html_code(spacing, desc)
                else:
                    spacing = document.add_paragraph()
                    html_code(spacing, diff)
        if lesson_plan.accommodations:
            document.add_paragraph("_____________________________________________________________________________")
            p6 = document.add_paragraph()
            run = p6.add_run('Accommodations')
            run.bold = True
            run.underline = True
            for diff in re.split('</p><p>|<p><br>|</p>|</h1>|</h2>|</ol>|</ul>',
                                 lesson_plan.accommodations):
                if '<ol>' in diff:
                    li_cnt = 0
                    for desc in diff.split('</li>'):
                        if desc:
                            li_cnt += 1
                            spacing = document.add_paragraph(
                                style='List Paragraph')
                            html_code(spacing, desc, str(li_cnt) + '. ')
                elif '<ul>' in diff:
                    for desc in diff.split('</li>'):
                        if desc:
                            spacing = document.add_paragraph(
                                style='List Bullet')
                            html_code(spacing, desc)
                else:
                    spacing = document.add_paragraph()
                    html_code(spacing, diff)

        if lesson_plan.essential_questions:
            document.add_paragraph("_____________________________________________________________________________")
            p7 = document.add_paragraph()
            run = p7.add_run('Essential Questions')
            run.bold = True
            run.underline = True
            for diff in re.split('</p><p>|<p><br>|</p>|</h1>|</h2>|</ol>|</ul>',
                                 lesson_plan.essential_questions):
                if '<ol>' in diff:
                    li_cnt = 0
                    for desc in diff.split('</li>'):
                        if desc:
                            li_cnt += 1
                            spacing = document.add_paragraph(
                                style='List Paragraph')
                            html_code(spacing, desc, str(li_cnt) + '. ')
                elif '<ul>' in diff:
                    for desc in diff.split('</li>'):
                        if desc:
                            spacing = document.add_paragraph(
                                style='List Bullet')
                            html_code(spacing, desc)
                else:
                    spacing = document.add_paragraph()
                    html_code(spacing, diff)

        if lesson_plan.remediation:
            document.add_paragraph("_____________________________________________________________________________")
            p8 = document.add_paragraph()
            run = p8.add_run('Remediation')
            run.bold = True
            run.underline = True
            for diff in re.split('</p><p>|<p><br>|</p>|</h1>|</h2>|</ol>|</ul>',
                                 lesson_plan.remediation):
                if '<ol>' in diff:
                    li_cnt = 0
                    for desc in diff.split('</li>'):
                        if desc:
                            li_cnt += 1
                            spacing = document.add_paragraph(
                                style='List Paragraph')
                            html_code(spacing, desc, str(li_cnt) + '. ')
                elif '<ul>' in diff:
                    for desc in diff.split('</li>'):
                        if desc:
                            spacing = document.add_paragraph(
                                style='List Bullet')
                            html_code(spacing, desc)
                else:
                    spacing = document.add_paragraph()
                    html_code(spacing, diff)

        if lesson_plan.use_of_technology:
            document.add_paragraph("_____________________________________________________________________________")
            p9 = document.add_paragraph()
            run = p9.add_run('Use of Technology')
            run.bold = True
            run.underline = True
            for diff in re.split('</p><p>|<p><br>|</p>|</h1>|</h2>|</ol>|</ul>',
                                 lesson_plan.use_of_technology):
                if '<ol>' in diff:
                    li_cnt = 0
                    for desc in diff.split('</li>'):
                        if desc:
                            li_cnt += 1
                            spacing = document.add_paragraph(
                                style='List Paragraph')
                            html_code(spacing, desc, str(li_cnt) + '. ')
                elif '<ul>' in diff:
                    for desc in diff.split('</li>'):
                        if desc:
                            spacing = document.add_paragraph(
                                style='List Bullet')
                            html_code(spacing, desc)
                else:
                    spacing = document.add_paragraph()
                    html_code(spacing, diff)

        if lesson_plan.vocabulary:
            document.add_paragraph("_____________________________________________________________________________")
            p10 = document.add_paragraph()
            run = p10.add_run('Vocabulary')
            run.bold = True
            run.underline = True
            for diff in re.split('</p><p>|<p><br>|</p>|</h1>|</h2>|</ol>|</ul>',
                                 lesson_plan.vocabulary):
                if '<ol>' in diff:
                    li_cnt = 0
                    for desc in diff.split('</li>'):
                        if desc:
                            li_cnt += 1
                            spacing = document.add_paragraph(
                                style='List Paragraph')
                            html_code(spacing, desc, str(li_cnt) + '. ')
                elif '<ul>' in diff:
                    for desc in diff.split('</li>'):
                        if desc:
                            spacing = document.add_paragraph(
                                style='List Bullet')
                            html_code(spacing, desc)
                else:
                    spacing = document.add_paragraph()
                    html_code(spacing, diff)

        if lesson_plan.supplementary_materials:
            document.add_paragraph("_____________________________________________________________________________")
            p11 = document.add_paragraph()
            run = p11.add_run('Supplementary Materials')
            run.bold = True
            run.underline = True
            for diff in re.split('</p><p>|<p><br>|</p>|</h1>|</h2>|</ol>|</ul>',
                                 lesson_plan.supplementary_materials):
                if '<ol>' in diff:
                    li_cnt = 0
                    for desc in diff.split('</li>'):
                        if desc:
                            li_cnt += 1
                            spacing = document.add_paragraph(
                                style='List Paragraph')
                            html_code(spacing, desc, str(li_cnt) + '. ')
                elif '<ul>' in diff:
                    for desc in diff.split('</li>'):
                        if desc:
                            spacing = document.add_paragraph(
                                style='List Bullet')
                            html_code(spacing, desc)
                else:
                    spacing = document.add_paragraph()
                    html_code(spacing, diff)

        if lesson_plan.procedures:
            document.add_paragraph("_____________________________________________________________________________")
            p12 = document.add_paragraph()
            run = p12.add_run('Procedures')
            run.bold = True
            run.underline = True
            for diff in re.split('</p><p>|<p><br>|</p>|</h1>|</h2>|</ol>|</ul>',
                                 lesson_plan.procedures):
                if '<ol>' in diff:
                    li_cnt = 0
                    for desc in diff.split('</li>'):
                        if desc:
                            li_cnt += 1
                            spacing = document.add_paragraph(
                                style='List Paragraph')
                            html_code(spacing, desc, str(li_cnt) + '. ')
                elif '<ul>' in diff:
                    for desc in diff.split('</li>'):
                        if desc:
                            spacing = document.add_paragraph(
                                style='List Bullet')
                            html_code(spacing, desc)
                else:
                    spacing = document.add_paragraph()
                    html_code(spacing, diff)

        if lesson_plan.connections_to_prior_knowledge_and_learning:
            document.add_paragraph("_____________________________________________________________________________")
            p13 = document.add_paragraph()
            run = p13.add_run('Connections to Prior Knowledge & Learning')
            run.bold = True
            run.underline = True
            for diff in re.split(
                '</p><p>|<p><br>|</p>|</h1>|</h2>|</ol>|</ul>', lesson_plan.
                    connections_to_prior_knowledge_and_learning):
                if '<ol>' in diff:
                    li_cnt = 0
                    for desc in diff.split('</li>'):
                        if desc:
                            li_cnt += 1
                            spacing = document.add_paragraph(
                                style='List Paragraph')
                            html_code(spacing, desc, str(li_cnt) + '. ')
                elif '<ul>' in diff:
                    for desc in diff.split('</li>'):
                        if desc:
                            spacing = document.add_paragraph(
                                style='List Bullet')
                            html_code(spacing, desc)
                else:
                    spacing = document.add_paragraph()
                    html_code(spacing, diff)

        if lesson_plan.conceptual_understanding_goal:
            document.add_paragraph("_____________________________________________________________________________")
            p14 = document.add_paragraph()
            run = p14.add_run('Conceptual Understanding Goal')
            run.bold = True
            run.underline = True
            for diff in re.split(
                '</p><p>|<p><br>|</p>|</h1>|</h2>|</ol>|</ul>', lesson_plan.
                    conceptual_understanding_goal):
                if '<ol>' in diff:
                    li_cnt = 0
                    for desc in diff.split('</li>'):
                        if desc:
                            li_cnt += 1
                            spacing = document.add_paragraph(
                                style='List Paragraph')
                            html_code(spacing, desc, str(li_cnt) + '. ')
                elif '<ul>' in diff:
                    for desc in diff.split('</li>'):
                        if desc:
                            spacing = document.add_paragraph(
                                style='List Bullet')
                            html_code(spacing, desc)
                else:
                    spacing = document.add_paragraph()
                    html_code(spacing, diff)

        if lesson_plan.key_points:
            document.add_paragraph("_____________________________________________________________________________")
            p15 = document.add_paragraph()
            run = p15.add_run('Key Points')
            run.bold = True
            run.underline = True
            for diff in re.split(
                    '</p><p>|<p><br>|</p>|</h1>|</h2>|</ol>|</ul>', lesson_plan.key_points):
                if '<ol>' in diff:
                    li_cnt = 0
                    for desc in diff.split('</li>'):
                        if desc:
                            li_cnt += 1
                            spacing = document.add_paragraph(
                                style='List Paragraph')
                            html_code(spacing, desc, str(li_cnt) + '. ')
                elif '<ul>' in diff:
                    for desc in diff.split('</li>'):
                        if desc:
                            spacing = document.add_paragraph(
                                style='List Bullet')
                            html_code(spacing, desc)
                else:
                    spacing = document.add_paragraph()
                    html_code(spacing, diff)

        if lesson_plan.assessment:
            document.add_paragraph("_____________________________________________________________________________")
            p16 = document.add_paragraph()
            run = p16.add_run('Assessment')
            run.bold = True
            run.underline = True
            for diff in re.split(
                    '</p><p>|<p><br>|</p>|</h1>|</h2>|</ol>|</ul>', lesson_plan.assessment):
                if '<ol>' in diff:
                    li_cnt = 0
                    for desc in diff.split('</li>'):
                        if desc:
                            li_cnt += 1
                            spacing = document.add_paragraph(
                                style='List Paragraph')
                            html_code(spacing, desc, str(li_cnt) + '. ')
                elif '<ul>' in diff:
                    for desc in diff.split('</li>'):
                        if desc:
                            spacing = document.add_paragraph(
                                style='List Bullet')
                            html_code(spacing, desc)
                else:
                    spacing = document.add_paragraph()
                    html_code(spacing, diff)

        if lesson_plan.student_exemplar_response:
            document.add_paragraph("_____________________________________________________________________________")
            p17 = document.add_paragraph()
            run = p17.add_run('Student Exemplar Response')
            run.bold = True
            run.underline = True
            for diff in re.split('</p><p>|<p><br>|</p>|</h1>|</h2>|</ol>|</ul>',
                                 lesson_plan.student_exemplar_response):
                if '<ol>' in diff:
                    li_cnt = 0
                    for desc in diff.split('</li>'):
                        if desc:
                            li_cnt += 1
                            spacing = document.add_paragraph(
                                style='List Paragraph')
                            html_code(spacing, desc, str(li_cnt) + '. ')
                elif '<ul>' in diff:
                    for desc in diff.split('</li>'):
                        if desc:
                            spacing = document.add_paragraph(
                                style='List Bullet')
                            html_code(spacing, desc)
                else:
                    spacing = document.add_paragraph()
                    html_code(spacing, diff)

        if lesson_plan.back_pocket_questions:
            document.add_paragraph("_____________________________________________________________________________")
            p18 = document.add_paragraph()
            run = p18.add_run('Back Pocket Questions')
            run.bold = True
            run.underline = True
            for diff in re.split('</p><p>|<p><br>|</p>|</h1>|</h2>|</ol>|</ul>',
                                 lesson_plan.back_pocket_questions):
                if '<ol>' in diff:
                    li_cnt = 0
                    for desc in diff.split('</li>'):
                        if desc:
                            li_cnt += 1
                            spacing = document.add_paragraph(
                                style='List Paragraph')
                            html_code(spacing, desc, str(li_cnt) + '. ')
                elif '<ul>' in diff:
                    for desc in diff.split('</li>'):
                        if desc:
                            spacing = document.add_paragraph(
                                style='List Bullet')
                            html_code(spacing, desc)
                else:
                    spacing = document.add_paragraph()
                    html_code(spacing, diff)

    except Exception as e:
        return JsonResponse({
            "message": str(e)}, status=status.HTTP_400_BAD_REQUEST
        )

    response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.\
                      wordprocessingml.document'
    )
    response['Content-Disposition'] = 'attachment; filename=' + docx_title.\
        replace(',', '')
    document.save(response)

    return response


def daterange(start_date, end_date, view):
    """Return list of dates for given range
    """
    date_list = []

    start_date = datetime.strptime(start_date, '%Y-%m-%d').date()
    end_date = datetime.strptime(end_date, '%Y-%m-%d').date()

    if view == 'monthly':
        start_date = start_date.replace(day=1)
        num_days = calendar.monthrange(start_date.year, start_date.month)
        end_date = date(start_date.year, start_date.month, num_days[1])

    for n in range(int((end_date - start_date).days)+1):
        date_list.append(start_date + timedelta(n))
    return date_list


def week_of_month(dt, value=None):
    """ Returns the week of the month for the specified date.
    """

    first_day = dt.replace(day=1)
    dom = dt.day
    adjusted_dom = dom + first_day.weekday()
    if value:
        return int(ceil((adjusted_dom)/7.0))-1

    return int(ceil((adjusted_dom)/7.0))


def exportLessonCalendar(request):
    """
    This function will download monthly and weekly lesson calender
    """
    start_date = request.GET.get('start_date')
    end_date = request.GET.get('end_date')
    user_id = request.GET.get('user_id')
    view = request.GET.get('view')
    class_id = request.GET.get('class_id')

    document = Document()

    footer = document.sections[0].footer
    foot = footer.add_paragraph()
    path = os.path.dirname(os.path.dirname(settings.BASE_DIR))
    foot.add_run('\t\t\t\t\t\t\t\t\t').add_picture(
        path + '/static/logo.png', width=Inches(0.6), height=Inches(0.2))
    foot_run = foot.add_run('\n\t\t\t\t\t\t\t\t       www.taraedtech.com')
    foot_run.font.size = Pt(9)
    foot_run.font.name = 'Calibria'

    user = User.objects.filter(id=user_id)

    one_class = False
    if class_id:
        classes_created = Class.objects.filter(id__in=class_id.split(','))
        if classes_created and len(class_id.split(',')) == 1:
            class_name = classes_created[0].name
            one_class = True
    else:
        classes_created = []
    calender_dates = daterange(start_date, end_date, view)

    lesson_lists = []
    for classes in classes_created:
        lessons = LessonPlan.objects.filter(lesson_class=classes)
        for lesson in lessons:
            lesson_lists.append(lesson)

    if view == 'monthly':
        section = document.sections[0]
        section.left_margin = Mm(12.7)
        section.right_margin = Mm(12.7)
        section.top_margin = Mm(12.7)
        section.bottom_margin = Mm(12.7)

        section = document.sections[-1]
        new_width, new_height = section.page_height, section.page_width
        section.orientation = WD_ORIENT.LANDSCAPE
        section.page_width = new_width
        section.page_height = new_height

        para = document.add_paragraph()
        run = para.add_run(calender_dates[0].strftime('%B'))
        run.font.size = Pt(28)
        run.font.name = 'Avenir'
        para.add_run(' ')
        run = para.add_run(str(calender_dates[0].year))
        run.font.size = Pt(28)
        run.font.name = 'Avenir'
        run = para.add_run('-')
        run.font.size = Pt(28)
        run.font.name = 'Avenir'
        run = para.add_run(user[0].last_name)
        run.font.size = Pt(28)
        run.font.name = 'Avenir'

        para1 = document.add_paragraph()
        if one_class == False:
            class_name = 'All Classes'
        run = para1.add_run(class_name)
        run.font.size = Pt(28)
        run.font.name = 'Avenir'

        rows = week_of_month(calender_dates[-1])
        table = document.add_table(rows, 7, 'Table Grid')

        table.cell(0, 0).paragraphs[0].add_run('    Sunday').font.name = 'Avenir'
        table.cell(0, 1).paragraphs[0].add_run('    Monday').font.name = 'Avenir'
        table.cell(0, 2).paragraphs[0].add_run('    Tuesday').font.name = 'Avenir'
        table.cell(0, 3).paragraphs[0].add_run('  Wednesday').font.name = 'Avenir'
        table.cell(0, 4).paragraphs[0].add_run('   Thursday').font.name = 'Avenir'
        table.cell(0, 5).paragraphs[0].add_run('    Friday').font.name = 'Avenir'
        table.cell(0, 6).paragraphs[0].add_run('    Saturday').font.name = 'Avenir'

        table.rows[0].cells[0]._tc.get_or_add_tcPr().append(
            parse_xml(r'<w:shd {} w:fill="b1b1b1"/>'.format(nsdecls('w'))))
        table.rows[0].cells[1]._tc.get_or_add_tcPr().append(
            parse_xml(r'<w:shd {} w:fill="b1b1b1"/>'.format(nsdecls('w'))))
        table.rows[0].cells[2]._tc.get_or_add_tcPr().append(
            parse_xml(r'<w:shd {} w:fill="b1b1b1"/>'.format(nsdecls('w'))))
        table.rows[0].cells[3]._tc.get_or_add_tcPr().append(
            parse_xml(r'<w:shd {} w:fill="b1b1b1"/>'.format(nsdecls('w'))))
        table.rows[0].cells[4]._tc.get_or_add_tcPr().append(
            parse_xml(r'<w:shd {} w:fill="b1b1b1"/>'.format(nsdecls('w'))))
        table.rows[0].cells[5]._tc.get_or_add_tcPr().append(
            parse_xml(r'<w:shd {} w:fill="b1b1b1"/>'.format(nsdecls('w'))))
        table.rows[0].cells[6]._tc.get_or_add_tcPr().append(
            parse_xml(r'<w:shd {} w:fill="b1b1b1"/>'.format(nsdecls('w'))))

        count = 0
        for row in table.rows:
            if count > 0:
                row.height = Pt(40)
            count += 1

        for dates in calender_dates:
            row_num = week_of_month(dates)
            col_num = dates.isocalendar()[2]
            try:
                if dates.day < 10:
                    if dates.replace(day=1).isocalendar()[2] == 7:
                        row_num = week_of_month(dates, 1)
                    table.cell(row_num, col_num).text = '\t\t 0' +\
                        str(dates.day) + '\n'
                else:
                    if dates.replace(day=1).isocalendar()[2] == 7:
                        row_num = week_of_month(dates, 1)
                    table.cell(row_num, col_num).text = '\t\t ' +\
                        str(dates.day) + '\n'
                if classes_created:
                    for lesson in lesson_lists:
                        for lesson_calender in Calender.objects.filter(
                                lesson=lesson, date__gte=start_date,
                                date__lte=end_date):
                            if lesson_calender.date.day == dates.day:
                                title = lesson.tittle
                                run = table.cell(row_num, col_num).paragraphs[0].\
                                    add_run(title)
                                run.bold = True
                                run.font.size = Pt(9)
                                run.font.name = 'Calibria'
                                for standard in lesson.standard.all():
                                    name = '\n' + standard.name
                                    run1 = table.cell(row_num, col_num).paragraphs[0].\
                                        add_run(name)
                                    run1.italic = True
                                    run1.font.size = Pt(9)
                                    run1.font.name = 'Calibria'
                                table.cell(row_num, col_num).paragraphs[0].\
                                    add_run('\n\n')
            except IndexError:
                row = table.add_row()
                row.height = Pt(50)
                table.cell(row_num, col_num).text = '\t           ' +\
                    str(dates.day)
    else:
        section = document.sections[0]
        section.left_margin = Mm(25.4)
        section.right_margin = Mm(25.4)
        section.top_margin = Mm(25.4)
        section.bottom_margin = Mm(25.4)

        section = document.sections[-1]
        new_width, new_height = section.page_height, section.page_width
        section.orientation = WD_ORIENT.LANDSCAPE
        section.page_width = new_width
        section.page_height = new_height

        name = document.add_paragraph()
        run = name.add_run(user[0].first_name + ' ' + user[0].last_name)
        run.font.size = Pt(16)
        run.font.name = 'Calibri'
        run.bold = True

        all_classes = document.add_paragraph()
        if one_class == False:
            class_name = 'All Classes'
        run = all_classes.add_run(class_name)
        run.font.size = Pt(11)
        run.font.name = 'Calibri'
        run.bold = True

        date_range = document.add_paragraph()
        run = date_range.add_run(calender_dates[0].strftime('%B') + ' ')
        run.font.size = Pt(11)
        run.font.name = 'Calibri'
        run.bold = True
        run = date_range.add_run(
            str(calender_dates[0].day) + '-' + str(calender_dates[-1].day) + ',')
        run.font.size = Pt(11)
        run.font.name = 'Calibri'
        run.bold = True
        run = date_range.add_run(' ' + str(calender_dates[0].year))
        run.font.size = Pt(11)
        run.font.name = 'Calibri'
        run.bold = True

        table = document.add_table(2, 7, 'Table Grid')
        table.cell(0, 0).paragraphs[0].add_run('    Sunday').font.name = 'Calibri'
        table.cell(0, 1).paragraphs[0].add_run('    Monday').font.name = 'Calibri'
        table.cell(0, 2).paragraphs[0].add_run('    Tuesday').font.name = 'Calibri'
        table.cell(0, 3).paragraphs[0].add_run('   Wednesday').font.name = 'Calibri'
        table.cell(0, 4).paragraphs[0].add_run('    Thursday').font.name = 'Calibri'
        table.cell(0, 5).paragraphs[0].add_run('    Friday').font.name = 'Calibri'
        table.cell(0, 6).paragraphs[0].add_run('    Saturday').font.name = 'Calibri'

        if classes_created:
            if lesson_lists:
                for lesson in lesson_lists:
                    row_num = 1
                    col_num = 0
                    for lesson_calender in Calender.objects.filter(
                            lesson=lesson, date__gte=start_date, date__lte=end_date):

                        if lesson_calender.date.isocalendar()[2] == 7:
                            row_num = 1
                            col_num = 0
                        else:
                            row_num = 1
                            col_num = lesson_calender.date.isocalendar()[2]

                        title = lesson.tittle
                        table.cell(row_num, col_num).paragraphs[0].add_run('\n')
                        run = table.cell(row_num, col_num).paragraphs[0].add_run(title)
                        run.bold = True
                        run.font.size = Pt(11)
                        run.font.name = 'Calibria'
                        table.cell(row_num, col_num).paragraphs[0].add_run('\n')
                        for standard in lesson.standard.all():
                            name = '\n' + standard.name
                            run1 = table.cell(row_num, col_num).paragraphs[0].\
                                add_run(name)
                            run1.italic = True
                            run1.font.size = Pt(11)
                            run1.font.name = 'Calibria'
                            table.cell(row_num, col_num).paragraphs[0].add_run('\n')
                table.cell(row_num, col_num).paragraphs[0].add_run('\n\n\n\n\n\n')

    response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.\
                      wordprocessingml.document'
    )
    response['Content-Disposition'] = 'attachment; filename=' +\
        user[0].first_name + '_' + user[0].last_name + '_' + view + '_classes.docx'
    document.save(response)

    return response


def download_observation_docx(request):
    """
    This function will download observation docx file
    """
    document = Document()

    font = document.styles['Normal'].font
    font.name = 'Calibri'

    footer = document.sections[0].footer
    foot = footer.add_paragraph()
    path = os.path.dirname(os.path.dirname(settings.BASE_DIR))
    foot.add_run('\t\t\t\t\t').add_picture(path + '/static/logo.png',
                                           width=Inches(0.7283465),
                                           height=Inches(0.1653543))

    observation_id = request.GET.get('observation_id')
    observation = Observation.objects.get(id=observation_id)
    school = document.add_paragraph()
    school.add_run('School: ').bold = True
    if observation.lesson_plan.created_by.school:
        school.add_run(observation.lesson_plan.created_by.school.name)
    else:
        school.add_run(observation.lesson_plan.created_by.my_school)

    teacher = document.add_paragraph()
    teacher.add_run('Teacher: ').bold = True
    teacher.add_run(observation.lesson_plan.created_by.first_name)
    teacher.add_run('  ')
    teacher.add_run(observation.lesson_plan.created_by.last_name)

    document.add_paragraph().add_run('Lesson Observation').bold = True
    document.add_paragraph()

    observed_by = document.add_paragraph()
    observed_by.add_run('Observation by: ').bold = True
    observed_by.add_run(observation.created_by.first_name)
    observed_by.add_run('  ')
    observed_by.add_run(observation.created_by.last_name)

    date = document.add_paragraph()
    date.add_run('Date: ').bold = True
    date.add_run(observation.date.strftime("%B"))
    date.add_run('  ')
    date.add_run(str(observation.date.day))
    date.add_run('th').font.superscript = True
    date.add_run(',  ')
    date.add_run(str(observation.date.year))
    date.add_run('  ')
    if (observation.date.time().hour >= 12):
        date.add_run(str(observation.date.time().hour - 12))
        date.add_run(':')
        date.add_run(str(observation.date.time().minute))
        date.add_run('pm')
    else:
        date.add_run(str(observation.date.time().hour))
        date.add_run(':')
        date.add_run(str(observation.date.time().minute))
        date.add_run('am')
    document.add_paragraph()

    length = document.add_paragraph()
    length.add_run('Length of Observation: ').bold = True
    if observation.duration:
        length.add_run(str(observation.duration))
        length.add_run(' minutes')

    type = document.add_paragraph()
    type.add_run('Type of Observation: ').bold = True
    if observation.type:
        type.add_run(observation.type.name)

    eval_lesson_obs = EvaluateLessonObservation.objects.filter(
        observation=observation)
    count = 0
    for elobs in eval_lesson_obs:
        table = document.add_table(1, 2)
        count += 1
        if count == 1:
            table.cell(0, 0).paragraphs[0].add_run('Observation Focus: ').bold = True
            if elobs.evaluation_type:
                table.cell(0, 0).paragraphs[0].add_run(
                    elobs.evaluation_type.name)
            table.cell(0, 1).paragraphs[0].add_run('Rating: ').bold = True
            if elobs.evaluation_choice:
                table.cell(0, 1).paragraphs[0].add_run(
                    elobs.evaluation_choice.name)
            document.add_paragraph("_____________________________________________________________________________")
        else:
            focus = table.cell(0, 0).paragraphs[0].add_run('Observation Focus: ')
            focus.bold = True
            focus.italic = True
            if elobs.evaluation_type:
                table.cell(0, 0).paragraphs[0].add_run(
                    elobs.evaluation_type.name).italic = True
            rating = table.cell(0, 1).paragraphs[0].add_run('Rating: ')
            rating.bold = True
            rating.italic = True
            if elobs.evaluation_choice:
                table.cell(0, 1).paragraphs[0].add_run(
                    elobs.evaluation_choice.name).italic = True
            document.add_paragraph("_____________________________________________________________________________")

    document.add_paragraph()
    if observation.description:
        clean = re.compile('<.*?>')
        li_cnt = 0
        for descr in re.split('</p><p>|<p><br>|</li>|</p>',
                              observation.description):
            if '<li>' in descr:
                li_cnt += 1
                li = '\t' + str(li_cnt) + '. '
                descr = descr.replace('<li>', li)
            desc = re.sub(clean, '', descr)
            spacing = document.add_paragraph(desc.replace(
                '&nbsp;', ' ').replace('\\r\\n', '').replace('&amp;', '&'))
            spacing.paragraph_format.line_spacing = WD_LINE_SPACING.AT_LEAST =\
                0.9

    response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.\
                      wordprocessingml.document'
    )
    response['Content-Disposition'] = 'attachment; filename=observatio.docx'
    document.save(response)

    return response
