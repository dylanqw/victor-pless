"""
Management utility to add templates.
"""
from django.core.management.base import BaseCommand

from lesson.models import Container, Template


class Command(BaseCommand):
    """
    Management utility to add default template.
    """
    help = 'Used to add default template.'

    def handle(self, *args, **options):

        Template.objects.all().delete()
        Container.objects.all().delete()
        default_templates = [
            {
                'name': 'Clear Template',
                'container': ['container1', 'container2', 'container3',
                              'container4', 'container5', 'container6',
                              'container7', 'container8']
            },
        ]
        for template in default_templates:
            temp, created = Template.objects.get_or_create(
                name=template['name']
            )
            for container in template['container']:
                contain, created = Container.objects.get_or_create(
                    title=container
                )
                temp.container.add(contain)
                self.stdout.write(
                    self.style.SUCCESS(
                        "{} template container added successfully.".format(temp.name))
                )
