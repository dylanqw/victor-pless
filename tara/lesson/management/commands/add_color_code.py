"""
Management utility to add color codes.
"""
from django.core.management.base import BaseCommand

from lesson.models import Class
from tara_user.models import User


class Command(BaseCommand):
    """
    Management utility to add color codes.
    """
    help = 'Used to add color codes.'

    def handle(self, *args, **options):
        color_list = ['#DE8F6E', '#C3D2D5', '#BDA0BC', '#7FF9F9', '#70A9C1',
                      '#8CADA7', '#DBCFB0', '#D888B2']

        all_user = User.objects.all()
        for user in all_user:
            classes = Class.objects.filter(created_by=user)
            color_index = 0
            for user_class in classes:
                if color_index <= 7:
                    user_class.background = color_list[color_index]
                    user_class.save()
                else:
                    user_class.background = color_list[color_index % 8]
                    user_class.save()

                color_index += 1

                self.stdout.write(
                    self.style.SUCCESS(
                        "background color code added successfully."))
