"""
Management utility to default activity.
"""
import csv

from django.core.management.base import BaseCommand

from lesson.models import Activity, LearningIntelligence, Subject


class Command(BaseCommand):
    """
    Management utility to add default activity.
    """
    help = 'Used to add default activity'

    def add_arguments(self, parser):
        parser.add_argument('path', type=str)

    def handle(self, *args, **options):

        with open(options['path']) as f:
            count = 0
            for data in csv.reader(f):
                count += 1
                if count == 1:
                    pass
                else:
                    for subject in data[1].split('\n'):
                        sub, created = Subject.objects.get_or_create(
                            name=subject.strip()
                        )

                        for learning_intelligence in data[2].split('\n'):
                            learn, created = LearningIntelligence.objects.\
                                get_or_create(
                                    name=learning_intelligence.strip())

                        activity, created = Activity.objects.get_or_create(
                            tittle=data[0].replace('"', "'"),
                            description=data[3].replace('"', "'").
                            replace('\n', ''),
                            subject=sub,
                            default_activity=True
                        )
                        for learning_intelligence in data[2].split('\n'):
                            activity.learning_intelligence.add(
                                LearningIntelligence.objects.get(
                                    name=learning_intelligence.strip())
                            )

                        if created:
                            self.stdout.write(
                                self.style.SUCCESS(
                                    "{} activity is added successfully.".
                                    format(activity.tittle))
                            )
                        else:
                            self.stdout.write(
                                self.style.ERROR(
                                    "{} user already exists."
                                    .format(activity.tittle))
                            )
