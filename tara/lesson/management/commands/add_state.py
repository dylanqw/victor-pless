"""
Management utility to add templates.
"""
from django.core.management.base import BaseCommand

from lesson.models import State


class Command(BaseCommand):
    """
    Management utility to add state.
    """
    help = 'Used to add US State.'

    def handle(self, *args, **options):

        state_list = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
                      'Colorado', 'Connecticut', 'Delaware', 'Florida',
                      'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana',
                      'Iowa', 'Kansas', 'Louisiana', 'Maine', 'Maryland',
                      'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi',
                      'Missouri', 'Montana', 'Nebraska', 'Nevada',
                      'New Hampshire', 'New Jersey', 'New Mexico', 'New York',
                      'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma',
                      'Oregon', 'Pennsylvania', 'Rhode Island',
                      'South Carolina', 'South Dakota', 'Tennessee', 'Texas',
                      'Utah', 'Vermont', 'Virginia', 'Washington',
                      'West Virginia', 'Wisconsin', 'Wyoming']

        for state_name in state_list:
            state, created = State.objects.get_or_create(
                name=state_name
            )
            if created:
                self.stdout.write(
                    self.style.SUCCESS(
                        "{} State added successfully.".format(state_name))
                )
            else:
                self.stdout.write(
                    self.style.ERROR(
                        "{} State already exists.".format(state.name))
                )
