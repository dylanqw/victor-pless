from django.core.management.base import BaseCommand

from lesson.models import Standards


class Command(BaseCommand):
    """
    Management utility to remove leading and trailing white spaces from standard.
    """
    def handle(self, *args, **options):
        
        for std in Standards.objects.all():
            std.name = std.name.strip()
            std.save()
            self.stdout.write(
                self.style.SUCCESS(
                    "{} removing leading and trailing spaces successfully."
                    .format(std.name))
            )
