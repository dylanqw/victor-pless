import csv

from django.core.management.base import BaseCommand

from lesson.models import Standards, TagElement, State


class Command(BaseCommand):
    """
    Management utility to add standards and elements.
    """
    help = 'Used to add standards and elements'

    def add_arguments(self, parser):
        parser.add_argument('path', type=str)

    def handle(self, *args, **options):

        with open(options['path'], encoding='ISO-8859-1') as f:
            count = 0
            for data in csv.reader(f):
                count += 1
                if count == 1:
                    pass
                else:
                    if data[0] != '':
                        state, created = State.objects.get_or_create(name=data[0])

                    if data[1] != '':
                        std = Standards.objects.filter(name=data[1].strip())
                        if not std:
                            std, created = Standards.objects.get_or_create(
                                name=data[1].strip(),
                                description=data[2],
                                state=state if data[0] else None
                            )
                            if created:
                                self.stdout.write(
                                    self.style.SUCCESS(
                                        "{} standard is added successfully.".
                                        format(std.name))
                                )
                            else:
                                self.stdout.write(
                                    self.style.ERROR(
                                        "{} already exists."
                                        .format(std.name))
                                )

                            if data[3] != '':
                                tag, created = TagElement.objects.get_or_create(
                                    element=data[3],
                                    standards=std
                                )
                                if created:
                                    self.stdout.write(
                                        self.style.SUCCESS(
                                            "{} element added successfully."
                                            .format(std.name))
                                    )
                                else:
                                    self.stdout.write(
                                        self.style.ERROR(
                                            "{} already exists."
                                            .format(std.name))
                                    )
                        else:
                            std[0].state = state
                            std[0].save()
                            self.stdout.write(
                                self.style.SUCCESS(
                                    "{} standard updated successfully."
                                    .format(std[0].name))
                            )
