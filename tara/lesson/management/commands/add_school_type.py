"""
Management utility to add templates.
"""
from django.core.management.base import BaseCommand

from lesson.models import SchoolType


class Command(BaseCommand):
    """
    Management utility to add school type.
    """
    help = 'Used to add school type.'

    def handle(self, *args, **options):

        school_type_list = ['Elementary School', 'Middle School',
                            'High School', 'Other']

        for school_type in school_type_list:
            temp, created = SchoolType.objects.get_or_create(
                name=school_type
            )
            if created:
                self.stdout.write(
                    self.style.SUCCESS(
                        "{} School Type added successfully.".format(school_type))
                )
            else:
                self.stdout.write(
                    self.style.ERROR(
                        "{} School Type already exists.".format(temp.name))
                )
