from django.contrib.auth import get_user_model
from rest_framework import permissions

User = get_user_model()


class FixActivityPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return True
        else:
            return False

    def has_object_permission(self, request, view, obj):
        user_group = User.objects.filter(
            username=request.user
        ).values_list('groups__name', flat=True)

        if (request.user.is_staff or 'administrator' in user_group):
            return True

        elif ('cluster_administrator' in user_group or 'school_leader' in
              user_group or 'department_coach' in user_group or 'teacher' in
              user_group):
            if request.method == 'GET':
                return True
            elif request.method in ['DELETE', 'PUT', 'PATCH']:
                if not obj.default_activity and request.user == obj.created_by:
                    return True
                else:
                    return False
            else:
                return False
        else:
            return False


class RateLessonPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return True
        else:
            return False

    def has_object_permission(self, request, view, obj):
        user_group = User.objects.filter(
            username=request.user
        ).values_list('groups__name', flat=True)

        if (request.user.is_staff or 'administrator' in user_group):
            return True

        elif request.user == obj.rated_by and request.method in\
                ['GET', 'PUT', 'PATCH']:
            return True

        elif ('teacher' in user_group or 'school_leader' in user_group or
                'department_coach' in user_group or 'cluster_administrator' in
                user_group) and request.method in ['GET', ]:
            return True


class TagElementPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return True
        else:
            return False

    def has_object_permission(self, request, view, obj):
        user_group = User.objects.filter(
            username=request.user
        ).values_list('groups__name', flat=True)

        if (request.user.is_staff or 'administrator' in user_group):
            return True

        elif ('teacher' in user_group or 'school_leader' in user_group or
                'department_coach' in user_group) and request.method in\
                ['GET', ]:
            return True


class SchoolPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        return True

    def has_object_permission(self, request, view, obj):

        user_grp = request.user.groups.values_list('name', flat=True)

        if (request.user.is_staff or 'administrator' in user_grp):
            return True

        if request.user.is_anonymous and request.method == 'GET':
            return True

        if user_grp:
            if ('school_leader' in user_grp[0] or 'department_coach' in user_grp[0] or
                    'teacher' in user_grp[0]) and request.method == 'GET':
                return True


class OnlyReadPermission(permissions.BasePermission):

    def has_permission(self, request, view):

        user_grp = User.objects.filter(
            username=request.user
        ).values_list('groups__name', flat=True).order_by('groups__name')

        if request.user.is_anonymous and request.method == 'GET':
            return True

        if user_grp:
            if ('cluster_administrator' in user_grp or 'school_leader' in
                user_grp or 'department_coach' in user_grp or 'teacher' in
                    user_grp) and request.method == 'POST':
                return False
            else:
                return True

    def has_object_permission(self, request, view, obj):

        user_grp = request.user.groups.values_list('name', flat=True)

        if (request.user.is_staff or 'administrator' in user_grp):
            return True

        if ('cluster_administrator' in user_grp or 'school_leader' in
            user_grp or 'department_coach' in user_grp or 'teacher' in
                user_grp) and request.method == 'GET':
            return True

        if request.user.is_anonymous and request.method == 'GET':
            return True


class OnlyAdministrativePermission(permissions.BasePermission):

    def has_permission(self, request, view):
        user_grp = request.user.groups.values_list('name', flat=True)
        if (request.user.is_staff or 'administrator' in user_grp):
            return True
        else:
            False


class ClassPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        user_group = User.objects.filter(
            username=request.user
        ).values_list('groups__name', flat=True).order_by('groups__name')

        if (request.user.is_staff or 'administrator' in user_group):
            return True

        elif request.user.is_authenticated:
            if 'teacher' in user_group and request.method == 'POST':
                if request.data:
                    if 'share_with_school' in request.data.keys():
                        return False

            elif 'school_leader' in user_group and request.method == 'POST':
                if request.data:
                    if 'share_with_school' in request.data.keys():
                        return False

            elif 'department_coach' in user_group and request.method == 'POST':
                if request.data:
                    if 'share_with_school' in request.data.keys():
                        return False
            return True

        else:
            return False

    def has_object_permission(self, request, view, obj):
        user_group = User.objects.filter(
            username=request.user
        ).values_list('groups__name', flat=True).order_by('groups__name')

        if (request.user.is_staff or 'administrator' in user_group):
            return True

        elif ('cluster_administrator' in user_group or 'school_leader' in
              user_group or 'department_coach' in user_group or 'teacher' in
              user_group):
            if request.user == obj.created_by and request.method in\
                    ['GET', 'PUT', 'PATCH', 'DELETE']:
                return True
            if request.user in obj.co_teacher.all() and request.method in\
                    ['GET', 'PUT', 'PATCH']:
                return True
            if request.method in ['GET', ]:
                return True


class LessonPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return True
        else:
            return False

    def has_object_permission(self, request, view, obj):
        user_group = User.objects.filter(
            username=request.user
        ).values_list('groups__name', flat=True).order_by('groups__name')

        if (request.user.is_staff or 'administrator' in user_group):
            return True

        elif ('cluster_administrator' in user_group or 'school_leader' in
              user_group or 'department_coach' in user_group or 'teacher' in
              user_group):
            if request.method == 'DELETE':
                return request.user == obj.created_by
            elif (request.user in obj.lesson_class.co_teacher.all() or
                  request.user == obj.created_by or
                  request.user == obj.lesson_class.created_by) and\
                    request.method in['PUT', 'PATCH']:
                return True
            elif request.method in ['GET', ]:
                return True


class StandardPermission(permissions.BasePermission):

    def has_permission(self, request, view):

        if request.user.is_authenticated:
            return True
        else:
            return False

    def has_object_permission(self, request, view, obj):

        user_grp = request.user.groups.values_list('name', flat=True)

        if (request.user.is_staff or 'administrator' in user_grp):
            return True

        elif ('cluster_administrator' in user_grp or 'teacher' in user_grp or
              'school_leader' in user_grp or 'department_coach' in user_grp
              ) and request.method == 'GET':
            return True
        else:
            return False


class TemplatePermission(permissions.BasePermission):

    def has_permission(self, request, view):

        user_grp = User.objects.filter(
            username=request.user
        ).values_list('groups__name', flat=True).order_by('groups__name')

        if ('school_leader' in user_grp[0] or
            'department_coach' in user_grp[0] or 'teacher' in user_grp[0])\
                and request.method == 'POST':
            return False
        else:
            return True

    def has_object_permission(self, request, view, obj):

        user_grp = User.objects.filter(
            username=request.user
        ).values_list('groups__name', flat=True).order_by('groups__name')

        if request.user.is_staff or 'administrator' in user_grp:
            return True
        elif 'teacher' in user_grp[0] and\
                request.method in ['GET', 'PUT', 'PATCH']:
            return True

        elif ('school_leader' in user_grp[0] or 'department_coach' in user_grp[0])\
                and request.method == 'GET':
            return True
        else:
            return False


class LessonActivityPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return True
        else:
            return False

    def has_object_permission(self, request, view, obj):
        user_group = User.objects.filter(
            username=request.user
        ).values_list('groups__name', flat=True).order_by('groups__name')

        if (request.user.is_staff or 'administrator' in user_group):
            return True

        elif 'cluster_administrator' in user_group:
            if request.method in ['GET', 'PUT', 'PATCH', 'DELETE']:
                return True

        elif ('school_leader' in user_group):
            if request.user == obj.lesson_plan.created_by and request.method\
                    in ['GET', 'PUT', 'PATCH', 'DELETE']:
                return True
            if (request.user in obj.lesson_plan.lesson_class.co_teacher.all() or
                request.user == obj.lesson_plan.lesson_class.created_by) and\
                    request.method in ['GET', 'PUT', 'PATCH', 'DELETE']:
                return True
            if request.method in ['GET', ]:
                return request.user.school == obj.lesson_plan.created_by.school

        elif ('department_coach' in user_group):
            if request.user == obj.lesson_plan.created_by and request.method\
                    in ['GET', 'PUT', 'PATCH', 'DELETE']:
                return True
            if (request.user in obj.lesson_plan.lesson_class.co_teacher.all() or
                request.user == obj.lesson_plan.lesson_class.created_by) and\
                    request.method in ['GET', 'PUT', 'PATCH', 'DELETE']:
                return True
            if request.method in ['GET', ]:
                return request.user.school == obj.lesson_plan.created_by.school

        elif ('teacher' in user_group):
            if request.user == obj.lesson_plan.created_by and request.method in\
                    ['GET', 'PUT', 'PATCH', 'DELETE']:
                return True
            if (request.user in obj.lesson_plan.lesson_class.co_teacher.all() or
                request.user == obj.lesson_plan.lesson_class.created_by) and\
                    request.method in ['GET', 'PUT', 'PATCH', 'DELETE']:
                return True


class ActivityTemplatePermission(permissions.BasePermission):
    """Get Activity Template Permission """

    def has_permission(self, request, view):
        if request.user.is_authenticated and request.method == 'GET':
            return True
        else:
            return False


class ObservationPermission(permissions.BasePermission):
    """
        - Teacher can create and read observation on his lesson
        - School leader has create, edit and read the permission
        - Department coach has create, edit and read the permission
        - Administrator has create, edit, read and delete permission
    """

    def has_permission(self, request, view):
        from lesson.models import LessonPlan
        user_group = User.objects.filter(
            username=request.user
        ).values_list('groups__name', flat=True).order_by('groups__name')

        if request.user.is_authenticated:
            if (request.user.is_staff or 'administrator' in user_group):
                return True
            elif 'cluster_administrator' in user_group and \
                    request.method == 'DELETE':
                return False
            elif 'school_leader' in user_group and request.method == 'DELETE':
                return False
            elif 'department_coach' in user_group and request.method == 'DELETE':
                return False
            elif 'teacher' in user_group and request.method == 'DELETE':
                return False
            else:
                if 'teacher' in user_group[0] and request.method == 'POST':
                    if 'lesson_plan' in request.data.keys():
                        return LessonPlan.objects.get(
                            id=request.data['lesson_plan']
                        ).created_by == request.user
                return True
        else:
            return False

    def has_object_permission(self, request, view, obj):
        user_group = User.objects.filter(
            username=request.user
        ).values_list('groups__name', flat=True).order_by('groups__name')

        if (request.user.is_staff or 'administrator' in user_group):
            return True
        elif 'cluster_administrator' in user_group and request.method == 'GET':
            return True
        elif 'school_leader' in user_group and request.method == 'GET':
            return True
        elif 'department_coach' in user_group and request.method == 'GET':
            return True
        elif 'teacher' in user_group[0] and request.method == 'GET':
            return True
        else:
            return request.user == obj.created_by


class EvaluateLessonPermission(permissions.BasePermission):
    """
        - Administrator has create, edit, read and delete the permission
        - School leader has create, edit and read permission
        - Department coach has create, edit and read permission
        - Teacher has create, edit and read permission.
    """

    def has_permission(self, request, view):
        user_group = User.objects.filter(
            username=request.user
        ).values_list('groups__name', flat=True).order_by('groups__name')

        if request.user.is_authenticated:
            if (request.user.is_staff or 'administrator' in user_group):
                return True
            elif 'cluster_administrator' in user_group and \
                    request.method == 'DELETE':
                return False
            elif 'school_leader' in user_group and request.method == 'DELETE':
                return False
            elif 'department_coach' in user_group and request.method == 'DELETE':
                return False
            elif 'teacher' in user_group and request.method == 'DELETE':
                return False
            else:
                return True


class ActivityCommentPermission(permissions.BasePermission):
    """
    - Administrator has create, edit, read and delete the permission
    - cluster_administrator can create, edit and read and delete his own record
    - School leader can create, edit and read and delete his own record
    - Department coach can create, edit and read and delete his own record
    - Teacher can create, edit and read and delete his own record.
    """

    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return True
        else:
            return False

    def has_object_permission(self, request, view, obj):
        user_group = User.objects.filter(
            username=request.user
        ).values_list('groups__name', flat=True).order_by('groups__name')

        if (request.user.is_staff or 'administrator' in user_group):
            return True
        if ('cluster_administrator' in user_group or 'school_leader' in
            user_group or 'department_coach' in user_group or 'teacher' in
                user_group) and request.method == 'GET':
            return True
        else:
            return request.user == obj.commented_by
