from django.db.models import Q
from rest_framework import serializers

from lesson.models import (Activity, ActivityComment, ActivityContainer,
                           ActivityTemplate, Attachment, Calender, Class,
                           Container, EvaluateLessonObservation,
                           EvaluationChoices, EvaluationCriteriaGroup,
                           EvaluationType, LearningIntelligence,
                           LessonActivity, LessonPlan, Observation,
                           ObservationType, RateLesson, School, SchoolDocument,
                           SchoolType, SmartGuide, Standards, State,
                           StrategyGroup, Subject, TagElement, Template)
from tara_user.models import ClusterAdminSchool
from tara_user.serializers import (GetUserSerializer, PostUserSerializer,
                                   Userserializer)


class SchoolSerializer(serializers.ModelSerializer):
    """
    School model serializers
    """
    school_document = serializers.SerializerMethodField('getSchoolDocument')

    def getSchoolDocument(self, school):
        school_documents = SchoolDocument.objects.filter(school=school)
        school_doc_list = []
        for data in school_documents:
            school_doc = {
                'id': data.id,
                'instruction': data.instruction,
                'content': data.content,
                'learning_environment': data.learning_environment,
                'differentiation': data.differentiation,
                'accommodations': data.accommodations,
                'essential_questions': data.essential_questions,
                'remediation': data.remediation,
                'use_of_technology': data.use_of_technology,
                'vocabulary': data.vocabulary,
                'supplementary_materials': data.supplementary_materials,
                'procedures': data.procedures,
                'connections_to_prior_knowledge_and_learning':
                    data.connections_to_prior_knowledge_and_learning,
                'conceptual_understanding_goal':
                    data.conceptual_understanding_goal,
                'key_points': data.key_points,
                'assessment': data.assessment,
                'student_exemplar_response': data.student_exemplar_response,
                'back_pocket_questions': data.back_pocket_questions
            }
            school_doc_list.append(school_doc)
        return school_doc_list

    class Meta:
        """
        Serializing all fields
        """
        model = School
        fields = '__all__'


class SubjectSerializer(serializers.ModelSerializer):
    """
    Subject model serializers
    """
    class Meta:
        """
        Serializing all fields
        """
        model = Subject
        fields = '__all__'


class GetClassSerializer(serializers.ModelSerializer):
    """
    Class model serializers
    """
    completed_lesson_count = serializers.SerializerMethodField(
        'getLessonCount'
    )

    lesson_created_count = serializers.SerializerMethodField()

    lesson_calendar = serializers.SerializerMethodField()

    def getLessonCount(self, lesson_class):
        return LessonPlan.objects.filter(lesson_class=lesson_class,
                                         mark_as_complete=True).count()

    def get_lesson_created_count(self, lesson_class):
        return LessonPlan.objects.filter(lesson_class=lesson_class).count()

    def get_lesson_calendar(self, lesson_class):

        start_date = self.context['request'].query_params.get('start_date')
        end_date = self.context['request'].query_params.get('end_date')
        class_id = self.context['request'].query_params.get('class_id')
        is_admin = self.context['request'].query_params.get('is_admin')

        if start_date and end_date and class_id:
            lesson_plans = LessonPlan.objects.filter(lesson_class=lesson_class)

            lesson_plan_list = []
            for lesson in lesson_plans:
                user = self.context['request'].user

                # get lesson rating
                lesson_rating = RateLesson.objects.filter(rated_by=user,
                                                          lesson_plan=lesson)
                for current_lesson_rating in lesson_rating:
                    if current_lesson_rating.lesson_plan == lesson:
                        rate_lesson = {
                            'id': current_lesson_rating.id,
                            'engagement': current_lesson_rating.engagement,
                            'effectiveness': current_lesson_rating.effectiveness,
                            'notes': current_lesson_rating.notes,
                            'created_on': current_lesson_rating.created_on,
                        }

                # get activity attachment
                lesson_activites = LessonActivity.objects.filter(
                    lesson_plan=lesson)
                attachment_list = []
                for lesson_activite in lesson_activites:
                    for attachment in lesson_activite.attachment.all():
                        attachment_list.append(attachment)

                # get lesson observation
                unread_obs_cnt = Observation.objects.filter(
                    lesson_plan=lesson, read=False, save_for_later=False).count()
                if is_admin:
                    lesson_observation = Observation.objects.filter(
                        lesson_plan=lesson,
                        save_for_later=False).select_related('created_by')
                else:
                    lesson_observation = Observation.objects.filter(
                        lesson_plan=lesson, created_by=user).select_related(
                            'created_by')
                obs_list = []
                for obs in lesson_observation:
                    lesson_obs = {
                        'observation_id': obs.id,
                        'save_for_later': obs.save_for_later,
                        'observation_on': obs.date,
                        'observation_by_id': obs.created_by.id,
                        'observation_by': obs.created_by.last_name
                        if obs.created_by.last_name
                        else obs.created_by.username,
                        'user_type': obs.created_by.groups.all().order_by(
                            'name')[0].name if obs.created_by.groups.all()
                        else 'principal',
                        'read': obs.read,
                    }
                    obs_list.append(lesson_obs)

                # Return all observations are submited or not
                observation = Observation.objects.filter(lesson_plan=lesson,
                                                         save_for_later=True,
                                                         created_by=user)
                if observation:
                    is_submited_observation = False
                else:
                    is_submited_observation = True

                # Return non submitted my observation on my lesson only
                my_obs = Observation.objects.filter(
                    lesson_plan__created_by=user, save_for_later=True,
                    created_by=user)
                for myobs in my_obs:
                    my_lesson_obs = None
                    if myobs.lesson_plan == lesson:
                        my_lesson_obs = {
                            'observation_id': myobs.id,
                            'save_for_later': myobs.save_for_later,
                            'observation_on': myobs.date,
                            'observation_by': myobs.created_by.last_name
                            if myobs.created_by.last_name
                            else myobs.created_by.username,
                            'user_type': myobs.created_by.groups.all().
                            order_by('name')[0].name if myobs.
                            created_by.groups.all() else 'principal',
                            'read': myobs.read,
                        }

                lesson_obj = {
                    'id': lesson.id,
                    'title': lesson.tittle,
                    'last_updated': lesson.last_updated,
                    'created_by': lesson.created_by.id,
                    'last_updated_by':
                        lesson.last_updated_by.first_name + ' ' +
                        lesson.last_updated_by.last_name
                        if lesson.last_updated_by else '',
                    'standard':
                        [standard.name for standard in lesson.standard.all()],
                    'objective': lesson.objective,
                    'mark_as_complete': lesson.mark_as_complete,
                    'slug': lesson.slug,
                    'lesson_activity_attachment': [
                        AttachmentSerializer(attachments).data
                        for attachments in attachment_list],
                    'calender_dates': [CalenderSerializers(dates).data
                                       for dates in Calender.objects.filter(
                                           date__gte=start_date,
                                           date__lte=end_date,
                                           lesson=lesson)],
                    'all_dates': [CalenderSerializers(dates).data
                                  for dates in Calender.objects.filter(
                        lesson=lesson)],
                    'rate_lesson': rate_lesson if lesson_rating else None,
                    'un_read_observation': unread_obs_cnt,
                    'observation': obs_list,
                    'is_submited_observation': is_submited_observation,
                    'self_observation': my_lesson_obs if my_obs else None,
                }
                lesson_plan_list.append(lesson_obj)
            return lesson_plan_list

    class Meta:
        """
        Serializing all fields
        """
        model = Class
        fields = '__all__'
        depth = 1


class PostClassSerializer(serializers.ModelSerializer):
    """
    Class model serializers
    """

    def to_representation(self, instance):
        from django.contrib.auth import get_user_model
        data = super().to_representation(instance)
        data['co_teacher'] = Userserializer(
            get_user_model().objects.filter(pk__in=data['co_teacher']),
            many=True).data
        data['school'] = SchoolSerializer(
            School.objects.filter(pk__in=data['school']), many=True).data

        return data

    class Meta:
        """
        Serializing all fields
        """
        model = Class
        fields = '__all__'


class OptimizeClassSerializer(serializers.ModelSerializer):
    """
    Optimised Class model serializers
    """
    class Meta:
        """
        Serializing required fields
        """
        model = Class
        fields = ('id', 'name', 'shared_to_school', 'co_teacher')
        depth = 1


class StandardSerializer(serializers.ModelSerializer):
    """
    Standards model serializers
    """
    elements = serializers.SerializerMethodField('getStandardElement')

    def getStandardElement(self, standard):
        element_list = []
        elements = TagElement.objects.filter(standards=standard).order_by('id')

        for element in elements:
            standard_element = {
                'id': element.id,
                'element': element.element
            }

            element_list.append(standard_element)
        return element_list

    class Meta:
        """
        Serializing all fields
        """
        model = Standards
        fields = '__all__'


class ContainerSerializer(serializers.ModelSerializer):
    """
    Container model serializers
    """
    class Meta:
        """
        Serializing all fields
        """
        model = Container
        fields = '__all__'


class LearningIntelligenceSerializer(serializers.ModelSerializer):
    """
    LearningIntelligence model serializers
    """
    class Meta:
        """
        Serializing all fields
        """
        model = LearningIntelligence
        fields = '__all__'


class GetActivitySerializer(serializers.ModelSerializer):
    """
    Activity model serializers
    """
    created_by = Userserializer()
    activity_guide = serializers.SerializerMethodField('getActivityTemplate')

    def getActivityTemplate(self, activity):
        activity_templates = ActivityTemplate.objects.filter(activity=activity)
        activity_template_list = []
        for data in activity_templates:
            activity_template = {
                'id': data.id,
                'name': data.name,
                'description': data.description
            }
            activity_template_list.append(activity_template)
        return activity_template_list

    class Meta:
        """
        Serializing all fields
        """
        model = Activity
        fields = '__all__'
        depth = 2


class PostActivitySerializer(serializers.ModelSerializer):
    """
    Activity model serializers
    """
    class Meta:
        """
        Serializing all fields
        """
        model = Activity
        fields = '__all__'


class GetTemplateSerializer(serializers.ModelSerializer):
    """
    Template model serializers
    """
    class Meta:
        """
        Serializing all fields
        """
        model = Template
        fields = '__all__'
        depth = 1


class PostTemplateSerializer(serializers.ModelSerializer):
    """
    Template model serializers
    """
    class Meta:
        """
        Serializing all fields
        """
        model = Template
        fields = '__all__'


class OptimiseTagElementSerializer(serializers.ModelSerializer):
    """
    TagElement model serializers
    """
    class Meta:
        """
        Serializing all fields
        """
        model = TagElement
        exclude = ('standards', )


class GetLessonPlanSerializer(serializers.ModelSerializer):
    """
    LessonPlan model serializers
    """
    created_by = Userserializer()
    last_updated_by = Userserializer()
    # elements = OptimiseTagElementSerializer(many=True)
    lesson_class = OptimizeClassSerializer()
    lesson_activities = serializers.SerializerMethodField(
        'get_activities'
    )

    standard = serializers.SerializerMethodField(
        'get_standards'
    )
    rate_lesson = serializers.SerializerMethodField('getRateLesson')

    calendar = serializers.SerializerMethodField()
    observation = serializers.SerializerMethodField()
    un_read_observation = serializers.SerializerMethodField()
    is_attachment = serializers.SerializerMethodField()
    is_submited_observation = serializers.SerializerMethodField()
    self_observation = serializers.SerializerMethodField()

    def get_is_attachment(self, lesson_plan):
        attachment = LessonActivity.objects.filter(
            lesson_plan=lesson_plan, attachment__isnull=False
        ).values_list('attachment')
        if len(attachment) == 0:
            return False
        else:
            return True

    def get_standards(self, lesson_plan):
        tag_element_list = []
        for data in lesson_plan.standard.all():
            standard = {
                'id': data.id,
                'name': data.name,
                'description': data.description,
            }
            tag_element_list.append(standard)
        return tag_element_list

    def get_activities(self, lesson_plan):
        activities = LessonActivity.objects.filter(lesson_plan=lesson_plan).\
            select_related(
                'container', 'activity__created_by', 'activity__subject').\
            prefetch_related('attachment')
        activities_list = []
        if lesson_plan.template:
            for container in lesson_plan.template.container.all():
                for activity in activities:
                    if container == activity.container:
                        activities_list.append(
                            GetLessonActivitySerializer(activity).data)
        return activities_list

    def getRateLesson(self, lesson_plan):
        user = self.context['request'].user
        obj = RateLesson.objects.filter(
            rated_by=user, lesson_plan=lesson_plan
        ).select_related('lesson_plan')
        for current_lesson in obj:
            if current_lesson.lesson_plan == lesson_plan:
                rate_lesson = {
                    'id': current_lesson.id,
                    'engagement': current_lesson.engagement,
                    'effectiveness': current_lesson.effectiveness,
                    'notes': current_lesson.notes,
                    'created_on': current_lesson.created_on,
                }
                return rate_lesson
            else:
                pass

    def get_calendar(self, lesson_plan):
        calendars = Calender.objects.filter(
            lesson=lesson_plan
        ).order_by('date')
        lesson_calendar_list = []
        for calendar in calendars:
            lesson_calendar = {
                'id': calendar.id,
                'date': calendar.date
            }
            lesson_calendar_list.append(lesson_calendar)
        return lesson_calendar_list

    def get_observation(self, lesson_plan):
        is_admin = self.context['request'].query_params.get('is_admin')
        user = self.context['request'].user
        user_grp = user.groups.all().values_list('name', flat=True)
        if is_admin:
            observation = Observation.objects.filter(lesson_plan=lesson_plan,
                                                     save_for_later=False)
        else:
            # for all teacher view
            if user.is_staff or 'administrator' in user_grp:
                observation = Observation.objects.filter(Q(
                    lesson_plan=lesson_plan, save_for_later=False) | Q(
                        lesson_plan=lesson_plan, created_by=user))

            elif 'cluster_administrator' in user_grp:
                schools = ClusterAdminSchool.objects.filter(user=user)

                if schools:
                    observation = Observation.objects.filter(Q(
                        lesson_plan=lesson_plan, save_for_later=False,
                        created_by__school__in=schools[0].school.all()) | Q(
                            lesson_plan=lesson_plan, created_by=user)).exclude(
                                created_by__groups__name__in=['administrator', ])
                else:
                    observation = Observation.objects.filter(
                        lesson_plan=lesson_plan, created_by=user)

            elif 'school_leader' in user_grp:
                observation = Observation.objects.filter(Q(
                    lesson_plan=lesson_plan, save_for_later=False,
                    created_by__school=user.school) | Q(
                    lesson_plan=lesson_plan, created_by=user)).exclude(
                    created_by__groups__name__in=['administrator',
                                                  'cluster_administrator'])
            elif 'department_coach' in user_grp:
                observation = Observation.objects.filter(Q(
                    lesson_plan=lesson_plan, save_for_later=False,
                    created_by__school=user.school) | Q(
                    lesson_plan=lesson_plan, created_by=user)).exclude(
                    created_by__groups__name__in=['administrator',
                                                  'cluster_administrator',
                                                  'school_leader'])
            else:
                observation = Observation.objects.filter(
                    lesson_plan=lesson_plan, created_by=user)

        obs_list = []
        for obs in observation.order_by('-id').distinct():
            if obs.created_by.groups.all():
                user_grp = obs.created_by.groups.all().values_list(
                    'name', flat=True)
                if 'administrator' in user_grp:
                    user_type = 'administrator'
                elif 'cluster_administrator' in user_grp:
                    user_type = 'cluster_administrator'
                elif 'school_leader' in user_grp:
                    user_type = 'school_leader'
                elif 'department_coach' in user_grp:
                    user_type = 'department_coach'
                elif 'teacher' in user_grp:
                    user_type = 'teacher'
                else:
                    user_type = 'principal'
            else:
                user_type = 'principal'
            lesson_obs = {
                'observation_id': obs.id,
                'save_for_later': obs.save_for_later,
                'observation_on': obs.date,
                'observation_by_id': obs.created_by.id,
                'observation_by': obs.created_by.last_name
                if obs.created_by.last_name else obs.created_by.username,
                'user_type': user_type,
                'read': obs.read,
            }
            obs_list.append(lesson_obs)
        return obs_list

    def get_un_read_observation(self, lesson_plan):
        """
        Return un_read observation count.
        """
        return Observation.objects.filter(lesson_plan=lesson_plan, read=False,
                                          save_for_later=False).count()

    def get_is_submited_observation(self, lesson_plan):
        """
        Return all observations are submited or not
        """
        user = self.context['request'].user
        observation = Observation.objects.filter(lesson_plan=lesson_plan,
                                                 save_for_later=True,
                                                 created_by=user)
        if observation:
            return False
        else:
            return True

    def get_self_observation(self, lesson_plan):
        """
        Return non submitted my observation on my lesson only
        """
        user = self.context['request'].user
        self_obs = Observation.objects.filter(
            lesson_plan__created_by=user, save_for_later=True,
            created_by=user)

        for obs in self_obs:
            if lesson_plan == obs.lesson_plan:
                lesson_obs = {
                    'observation_id': obs.id,
                    'save_for_later': obs.save_for_later,
                    'observation_on': obs.date,
                    'observation_by': obs.created_by.last_name
                    if obs.created_by.last_name else obs.created_by.
                    username,
                    'user_type': obs.created_by.groups.all().order_by(
                        'name')[0].name if obs.created_by.groups.all()
                    else 'principal',
                    'read': obs.read,
                }
                return lesson_obs

    class Meta:
        """
        Serializing all fields
        """
        model = LessonPlan
        fields = ('id', 'tittle', 'slug', 'date', 'objective',
                  'mark_as_complete', 'template', 'created_by', 'calendar',
                  'lesson_class', 'standard', 'elements', 'lesson_activities',
                  'rate_lesson', 'last_updated', 'last_updated_by', 'remediation',
                  'differentiation', 'accommodations', 'essential_questions',
                  'assessment', 'use_of_technology', 'vocabulary',
                  'is_attachment', 'supplementary_materials',
                  'procedures', 'connections_to_prior_knowledge_and_learning',
                  'conceptual_understanding_goal', 'key_points',
                  'student_exemplar_response', 'back_pocket_questions',
                  'observation', 'un_read_observation',
                  'is_submited_observation', 'self_observation')
        depth = 2


class PostLessonPlanSerializer(serializers.ModelSerializer):
    """
    LessonPlan model serializers
    """
    last_updated_by = serializers.SerializerMethodField()

    def get_last_updated_by(self, lesson):
        return PostUserSerializer(lesson.last_updated_by).data

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['standard'] = StandardSerializer(
            Standards.objects.filter(pk__in=data['standard']),
            many=True).data
        data['elements'] = OptimiseTagElementSerializer(
            TagElement.objects.filter(pk__in=data['elements']), many=True).data

        return data

    class Meta:
        """
        Serializing all fields
        """
        model = LessonPlan
        fields = '__all__'


class RateLessonSerializer(serializers.ModelSerializer):
    """
    RateLesson model serializers
    """
    class Meta:
        """
        Serializing all fields
        """
        model = RateLesson
        fields = '__all__'


class GetActivityContainerSerializer(serializers.ModelSerializer):
    """
    ActivityContainer model serializers
    """
    class Meta:
        """
        Serializing all fields
        """
        model = ActivityContainer
        fields = '__all__'
        depth = 2


class PostActivityContainerSerializer(serializers.ModelSerializer):
    """
    ActivityContainer model serializers
    """
    class Meta:
        """
        Serializing all fields
        """
        model = ActivityContainer
        fields = '__all__'


class TagElementSerializer(serializers.ModelSerializer):
    """
    TagElement model serializers
    """
    class Meta:
        """
        Serializing all fields
        """
        model = TagElement
        fields = '__all__'


class AttachmentSerializer(serializers.ModelSerializer):
    """
    Attachment model serializers
    """
    class Meta:
        """
        Serializing all fields
        """
        model = Attachment
        fields = '__all__'


class GetLessonActivitySerializer(serializers.ModelSerializer):
    """
    LessonActivity model serializers
    """

    activity_comments = serializers.SerializerMethodField('getActivityComment')
    activity = serializers.SerializerMethodField('getActivity')

    def getActivityComment(self, lesson_activity):
        comments = ActivityComment.objects.filter(
            lesson_activity=lesson_activity
        )
        comment_list = []
        for data in comments:
            comment_list.append({
                'id': data.id,
                'comment': data.comment,
                'created_on': data.created_on,
                'reviewed': data.reviewed,
                'commented_by': Userserializer(data.commented_by).data
            })
        return comment_list

    def getActivity(self, lesson_activity):
        return GetActivitySerializer(lesson_activity.activity).data

    class Meta:
        """
        Serializing all fields
        """
        model = LessonActivity
        fields = ('id', 'set_duration', 'set_grouping', 'sequence', 'archive',
                  'description', 'activity', 'container', 'attachment',
                  'activity_comments', 'custom_set_duration', 'activity_guide')
        depth = 2


class PostLessonActivitySerializer(serializers.ModelSerializer):
    """
    LessonActivity model serializers
    """
    set_duration = serializers.IntegerField(required=False, allow_null=True)

    class Meta:
        """
        Serializing all fields
        """
        model = LessonActivity
        fields = '__all__'


class GetActivityCommentSerializer(serializers.ModelSerializer):
    """
    ActivityComment model Serializer
    """
    class Meta:
        """
        Serializeing all fields
        """
        model = ActivityComment
        fields = '__all__'
        depth = 1


class PostActivityCommentSerializer(serializers.ModelSerializer):
    """
    ActivityComment model Serializer
    """
    class Meta:
        """
        Serializing all fields
        """
        model = ActivityComment
        fields = '__all__'


class ActivityTemplateSerializer(serializers.ModelSerializer):
    """
    ActivityTemplate model Serializer
    """
    class Meta:
        """
        Serializing all fields
        """
        model = ActivityTemplate
        fields = '__all__'


class SchoolDocumentSerializer(serializers.ModelSerializer):
    """
    SchoolDocuments model Serializer
    """
    class Meta:
        """
        Serializeing all fields
        """
        model = SchoolDocument
        fields = '__all__'


class StateSerializer(serializers.ModelSerializer):
    """
    State model Serializer
    """
    class Meta:
        """
        Serializing all fields
        """
        model = State
        fields = '__all__'


class SchoolTypeSerailizer(serializers.ModelSerializer):
    """
    SchoolType model Serializer
    """
    class Meta:
        """
        Serializing all fields
        """
        model = SchoolType
        fields = '__all__'


class CalenderSerializers(serializers.ModelSerializer):
    """
    Calender model Serializer
    """
    class Meta:
        """
        Serializing all fields.
        """
        model = Calender
        fields = '__all__'


class StrategyGroupSerializers(serializers.ModelSerializer):
    """
    StrategyGroup model Serializer
    """
    class Meta:
        """
        Serializing all fields.
        """
        model = StrategyGroup
        fields = '__all__'


class ObservationTypeSerializers(serializers.ModelSerializer):
    """
    Type model serializer
    """
    class Meta:
        """
        Serializing all fields.
        """
        model = ObservationType
        fields = '__all__'


class ObservationSerializers(serializers.ModelSerializer):
    """
    Observation model serializer
    """
    class Meta:
        """
        Serializing all fields.
        """
        model = Observation
        fields = '__all__'
        extra_kwargs = {
            'lesson_plan': {'required': True, 'allow_null': False}
        }


class RetriveObservationSerializers(serializers.ModelSerializer):
    """
    Observation model retrive serializer
    """
    evaluate_lesson_observation = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    last_evaluated = serializers.SerializerMethodField()

    def get_last_name(self, obs):
        if obs.created_by.last_name:
            return obs.created_by.last_name
        else:
            return obs.created_by.username

    def get_evaluate_lesson_observation(self, obs):
        eval_lesson_obs = EvaluateLessonObservation.objects.filter(
            observation=obs)
        evaluation_lesson_obs_list = []
        for eval_obs in eval_lesson_obs:
            evaluation_lesson_obs = {
                'id': eval_obs.id,
                'evaluation_type': EvaluationTypeSerializers(
                    eval_obs.evaluation_type).data,
                'evaluation_choice': EvaluationChoicesSerializers(
                    eval_obs.evaluation_choice).data,
            }
            evaluation_lesson_obs_list.append(evaluation_lesson_obs)

        return evaluation_lesson_obs_list

    def get_last_evaluated(self, obs):
        eval_last_evaluated = EvaluateLessonObservation.objects.filter(
            observation=obs)

        if eval_last_evaluated:
            return eval_last_evaluated.latest('last_evaluated').last_evaluated

    class Meta:
        """
        Serializing all fields.
        """
        model = Observation
        fields = '__all__'


class SmartGuideSerializers(serializers.ModelSerializer):
    """
    SmartGuide model serializer
    """
    class Meta:
        """
        Serializing all fields.
        """
        model = SmartGuide
        fields = '__all__'


class EvaluationTypeSerializers(serializers.ModelSerializer):
    """
    EvaluationType model serializer
    """
    class Meta:
        """
        Serializing all fields.
        """
        model = EvaluationType
        fields = '__all__'


class EvaluationChoicesSerializers(serializers.ModelSerializer):
    """
    EvaluationChoices model serializer
    """
    class Meta:
        """
        Serializing all fields.
        """
        model = EvaluationChoices
        fields = '__all__'


class EvaluateLessonObservationSerializers(serializers.ModelSerializer):
    """
    EvaluateLessonObservation model serializer
    """
    class Meta:
        """
        Serializing all fields.
        """
        model = EvaluateLessonObservation
        fields = '__all__'


class EvaluationCriteriaGroupSerializers(serializers.ModelSerializer):
    """
    EvaluationCriteriaGroup model serializer
    """
    class Meta:
        """
        Serializing all fields.
        """
        model = EvaluationCriteriaGroup
        fields = '__all__'
