import codecs
import csv
from rangefilter.filter import DateRangeFilter
from django import forms
from django.contrib import admin, messages
from django.contrib.auth import get_user_model
from django.db.models import Count, Q
from django.shortcuts import redirect, render
from django.urls import path
from django.utils.safestring import mark_safe
from django_admin_listfilter_dropdown.filters import RelatedDropdownFilter

from billing.models import SchoolSubscription
from lesson.models import (Activity, ActivityTemplate, Container,
                           EvaluationChoices, EvaluationCriteriaGroup,
                           EvaluationType, LearningIntelligence, Observation,
                           ObservationType, School, SchoolDocument, SchoolType,
                           SmartGuide, Standards, State, StrategyGroup,
                           Subject, TagElement, Template, LessonPlan)

User = get_user_model()


class ClassAdmin(admin.ModelAdmin):
    list_display = ['name', 'created_by']
    list_filter = ('created_by',)
    fieldsets = (
        ('Your class', {
            'fields': ('name', 'created_by', 'co_teacher')
        }),
    )


class TemplateAdmin(admin.ModelAdmin):
    list_display = ['name']
    fieldsets = (
        ('', {
            'fields': ('name', 'container')
        }),
    )


class ActivityTemplateInline(admin.StackedInline):
    model = ActivityTemplate
    can_delete = False
    verbose_name_plural = 'Activity Guide'
    fk_name = 'activity'
    extra = 0


class StrategyGroupInline(admin.StackedInline):
    autocomplete_fields = ['strategygroup']
    model = StrategyGroup.activity.through
    can_delete = False
    verbose_name_plural = 'Strategy Group'
    verbose_name = 'Strategy_Group'
    fk_name = 'activity'
    extra = 0
    min_num = 0


class ActivityAdmin(admin.ModelAdmin):

    search_fields = ['tittle']

    inlines = (ActivityTemplateInline, StrategyGroupInline)

    list_display = ['tittle', 'description', 'subject', 'default_activity',
                    'archive', 'created_by']
    list_filter = (('subject', RelatedDropdownFilter),
                   ('learning_intelligence', RelatedDropdownFilter),
                   'archive', )
    # list_editable = ('archive',)
    actions = ['make_default_activity', 'remove_default_activity']

    fieldsets = (
        ('Section 1', {
            'fields': ('tittle', 'description',)
        }),
        ('Section 2', {
            'fields': ('subject', 'created_by')
        }),
        ('Section 3', {
            'fields': ('learning_intelligence', 'default_activity', 'archive')
        }),
    )

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        super().save_model(request, obj, form, change)

    def get_paginator(self, request, queryset, per_page, orphans=0,
                      allow_empty_first_page=True):
        if request.path == '/admin/lesson/activity/autocomplete/':
            queryset = queryset.filter(created_by__is_staff=True)

        return self.paginator(queryset, per_page, orphans,
                              allow_empty_first_page)

    def make_default_activity(self, request, queryset):
        for activity in queryset:
            activity.default_activity = True
            activity.save()
        messages.add_message(
            request, messages.INFO, 'Selected activities converted into'
                                    ' default activity.'
        )

    make_default_activity.short_description = 'Make Default Activity'

    def remove_default_activity(self, request, queryset):
        for activity in queryset:
            activity.default_activity = False
            activity.save()
        messages.add_message(
            request, messages.INFO, 'Selected activities removed from'
                                    ' default activity bank.'
        )

    remove_default_activity.short_description = 'Remove Default Activity'


class LessonPlanAdmin(admin.ModelAdmin):
    list_display = ['tittle', 'lesson_class', 'mark_as_complete']
    list_filter = ('lesson_class',)
    readonly_fields = ["slug"]
    fieldsets = (
        ('Section 1', {
            'fields': ('tittle', 'slug', 'objective', 'lesson_class')
        }),
        ('Section 2', {
            'fields': ('standard', 'template', 'elements',)
        }),
        ('Section 3', {
            'fields': ('date', 'mark_as_complete', 'created_by')
        }),
    )


class ActivityTemplateAdmin(admin.ModelAdmin):
    list_display = ['name', 'activity', 'get_subject']

    def get_subject(self, obj):
        return obj.activity.subject

    get_subject.admin_order_field = 'activity__subject'
    get_subject.short_description = 'Subject'

    class ActivityModelChoiceField(forms.ModelChoiceField):
        def label_from_instance(self, obj):
            return "%s-%s" % (obj.tittle, obj.subject)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'activity':
            return self.ActivityModelChoiceField(
                queryset=Activity.objects.filter(archive=False,
                                                 default_activity=True)
            )

        return super(ActivityTemplateAdmin, self).\
            formfield_for_foreignkey(db_field, request, **kwargs)


class SchoolDocumentInline(admin.StackedInline):
    model = SchoolDocument
    can_delete = False
    verbose_name_plural = 'School Permissions'
    fk_name = 'school'
    fieldsets = (
        (None, {
            'fields': ('instruction', 'content', 'learning_environment')
        }),
        ('Teacher Permissions', {
            'fields': (
                ('differentiation', 'accommodations'),
                ('essential_questions', 'remediation'),
                ('use_of_technology', 'vocabulary'),
                ('supplementary_materials', 'procedures'),
                ('connections_to_prior_knowledge_and_learning',
                 'back_pocket_questions'),
                ('conceptual_understanding_goal', 'key_points'),
                ('assessment', 'student_exemplar_response'),
            )
        }),
    )


class SchoolSubscriptionInline(admin.StackedInline):
    model = SchoolSubscription
    can_delete = False
    verbose_name_plural = 'School Subscription'
    fk_name = 'school'
    extra = 0
    min_num = 0


class SchoolAdmin(admin.ModelAdmin):

    inlines = (SchoolDocumentInline, SchoolSubscriptionInline)

    list_display = ['school_name', 'date_created', 'state', 'district',
                    'teachers', 'coaches', 'school_leaders', 'lesson_created',
                    'lesson_completed', 'action_link']

    fieldsets = (
        ('School Information', {
            'fields': ('name', 'school_type', 'state', 'district',
                       'created_by')
        }),
        ('School Lesson Templates', {
            'fields': ('template',)
        }),
        ("Strategy Group", {
            'fields': ('strategy_group',)
        }),
        ("Evaluation Criteria Group", {
            'fields': ('evaluation_criteria_group',)
        })
    )

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(SchoolAdmin, self).get_inline_instances(request, obj)

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        qs = qs.annotate(
            _teacher_count=Count(
                'user', filter=Q(user__groups__name='teacher')
            ),
            _coache_count=Count(
                'user', filter=Q(user__groups__name='department_coach')
            ),
            _leader_count=Count(
                'user', filter=Q(user__groups__name='school_leader')
            ),
            _lesson_count=Count('user__lessonplan', distinct=True),
            _lesson_complete_count=Count(
                'user__lessonplan', filter=Q(
                    user__lessonplan__mark_as_complete=True
                ),
                distinct=True
            )
        )
        return qs

    def school_name(self, school):
        return school

    school_name.admin_order_field = 'name'

    def date_created(self, school):
        return school.created_at

    date_created.admin_order_field = 'created_at'

    def teachers(self, school):
        return school._teacher_count

    teachers.admin_order_field = '_teacher_count'

    def coaches(self, school):
        return school._coache_count

    coaches.admin_order_field = '_coache_count'

    def school_leaders(self, school):
        return school._leader_count

    school_leaders.admin_order_field = '_leader_count'

    def lesson_created(self, school):
        return school._lesson_count

    lesson_created.admin_order_field = '_lesson_count'

    def lesson_completed(self, school):
        return school._lesson_complete_count

    lesson_completed.admin_order_field = '_lesson_complete_count'

    def action_link(self, obj):
        app_name = obj._meta.app_label
        url_name = self.model.__name__.lower()
        school_id = obj.id
        drop_down = """<html>
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
               <style>
                .w3-text-blue, .w3-hover-text-blue:hover{{
                   color: #79AEC8!important;
                }}strong {{font-weight: bold;}}body {{line-height: 1.2;}}
                h1 {{
                    margin: 0 0 20px;
                    font-weight: 300;
                    font-size: 20px;
                    color: #666;
                    font-family: "Roboto","Lucida Grande","DejaVu Sans","Bitstream Vera Sans",Verdana,Arial,sans-serif;
                }}
                </style>
                <body>
                <div class="w3-container">
                <div class="w3-dropdown-hover">
                    <button class="w3-button w3-border w3-text-blue w3-large w3-white" >More ...</button>
                    <div class="w3-dropdown-content w3-bar-block w3-border">
                    <a style=color:black href="/admin/{0}/{1}/{2}/change" class="w3-bar-item w3-button">See School Details</a>
                    <a style=color:black href="/admin/{0}/{1}/{2}/delete" class="w3-bar-item w3-button">Delete School</a>
                    </div>
                </div></div></body></html>
                """.format(app_name,
                           url_name,
                           school_id)
        return mark_safe(drop_down)

    action_link.allow_tags = True
    action_link.short_description = ""


class CsvImportForm(forms.Form):
    csv_file_for_standard = forms.FileField()


class StandardAdmin(admin.ModelAdmin):

    change_list_template = "lesson/standard_changelist.html"

    search_fields = ('name', 'state__name')

    list_display = ('name', 'state')

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('import-csv/', self.import_csv),
        ]
        return my_urls + urls

    def import_csv(self, request):
        if request.method == "POST":
            csv_file = request.FILES["csv_file_for_standard"]

            reader = csv.reader(csv_file)
            try:
                reader = csv.DictReader(codecs.iterdecode(csv_file, 'utf-8'))
                if reader.fieldnames == ['State', 'Standard Tag',
                                         'Standard Description', 'Element']:
                    for row in reader:
                        if list(row.items())[0][1] != '' and\
                                list(row.items())[1][1] != '':
                            state, created = State.objects.get_or_create(
                                name=list(row.items())[0][1]
                            )
                            standard = Standards.objects.filter(
                                name=list(row.items())[1][1])
                            if not standard:
                                std = Standards.objects.create(
                                    name=list(row.items())[1][1],
                                    description=list(row.items())[2][1],
                                    state=state,
                                    created_by=request.user
                                )
                                if list(row.items())[3][1] != '':
                                    TagElement.objects.create(
                                        element=list(row.items())[3][1],
                                        standards=std
                                    )
                            else:
                                messages.warning(
                                    request, 'Standard - {0} already exist!'
                                             ' Update manually'.format(list(
                                                 row.items())[1][1])
                                )

                    self.message_user(request,
                                      "Your csv file has been imported")
                    return redirect("..")

                else:
                    messages.error(request,
                                   "Your csv file is not in correct format")

            except Exception:
                messages.error(request,
                               "Your csv file is not in correct format")

        form = CsvImportForm()
        payload = {"form": form}
        return render(
            request, "admin/csv_form.html", payload
        )


class CustomStrategyBankAdmin(admin.ModelAdmin):
    autocomplete_fields = ['activity']
    search_fields = ['name']


class ObservationAdmin(admin.ModelAdmin):
    search_fields = ['created_by__username', 'created_by__email']

    list_display = ['lesson_plan', 'save_for_later', 'archive', 'created_by',
                    'date']

    list_filter = ('archive', 'save_for_later', ('date', DateRangeFilter))

    actions = ['archive']

    def archive(self, request, queryset):
        for activity in queryset:
            activity.archive = True
            activity.save()
        messages.add_message(
            request, messages.INFO, 'Selected observations are archived'
        )
    archive.short_description = 'Archive Selected'


class EvaluationTypeAdmin(admin.ModelAdmin):
    search_fields = ['name']


admin.site.register(LessonPlan, LessonPlanAdmin)
admin.site.register(School, SchoolAdmin)
admin.site.register(Container)
admin.site.register(LearningIntelligence)
admin.site.register(Activity, ActivityAdmin)
admin.site.register(Template, TemplateAdmin)
admin.site.register(Subject)
# admin.site.register(Class, ClassAdmin)
# admin.site.register(TagElement)
admin.site.register(Standards, StandardAdmin)
admin.site.register(ActivityTemplate, ActivityTemplateAdmin)
admin.site.register(SchoolType)
admin.site.register(State)
# admin.site.register(SchoolDocument)
admin.site.register(StrategyGroup, CustomStrategyBankAdmin)
admin.site.register(ObservationType)
admin.site.register(Observation, ObservationAdmin)
admin.site.register(SmartGuide)
admin.site.register(EvaluationType, EvaluationTypeAdmin)
admin.site.register(EvaluationChoices)
admin.site.register(EvaluationCriteriaGroup, )
