from datetime import datetime
from io import StringIO
from unittest.mock import MagicMock, patch

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.files import File
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from lesson.models import (Activity, ActivityComment, ActivityContainer,
                           ActivityTemplate, Attachment, Class, Container,
                           EvaluateLessonObservation, EvaluationChoices,
                           EvaluationCriteriaGroup, EvaluationType,
                           LearningIntelligence, LessonActivity, LessonPlan,
                           Observation, ObservationType, RateLesson, School,
                           SchoolDocument, SchoolType, SmartGuide, Standards,
                           State, StrategyGroup, Subject, TagElement, Template)
from lesson.serializers import (ActivityTemplateSerializer,
                                AttachmentSerializer, ContainerSerializer,
                                EvaluateLessonObservationSerializers,
                                EvaluationChoicesSerializers,
                                EvaluationCriteriaGroupSerializers,
                                EvaluationTypeSerializers,
                                LearningIntelligenceSerializer,
                                ObservationSerializers,
                                ObservationTypeSerializers,
                                PostActivityCommentSerializer,
                                PostActivityContainerSerializer,
                                PostActivitySerializer, PostClassSerializer,
                                PostLessonActivitySerializer,
                                PostLessonPlanSerializer,
                                PostTemplateSerializer, RateLessonSerializer,
                                SchoolDocumentSerializer, SchoolSerializer,
                                SmartGuideSerializers, StandardSerializer,
                                StrategyGroupSerializers, SubjectSerializer,
                                TagElementSerializer)

User = get_user_model()

client = APIClient()


class CreateSchoolTest(APITestCase):
    """
    Create School API test cases
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))
        self.school_type = SchoolType.objects.create(name='test type')
        self.state = State.objects.create(name='test state')

        self.data = {
            'name': 'New Bostan',
            'school_type': self.school_type.id,
            'state': self.state.id,
            'district': 'test district'
        }

    def test_admin_can_create_school(self):
        """
        Assert object creation success
        """
        user = User.objects.get(username=self.user_1)
        self.client.force_authenticate(user=user)
        self.client.login(username=self.user_1.username,
                          password=self.user_1.password)
        response = self.client.post(reverse('school-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.client.logout()
        self.client.force_authenticate(user=None)

    def test_user_can_not_create_school(self):
        """
        Teacher can not create user
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2)
        self.client.force_authenticate(user=user)

        response = self.client.post(reverse('school-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ReadSchoolTest(APITestCase):
    """
    Read School API test cases
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))
        self.school_type = SchoolType.objects.create(name='test type')
        self.state = State.objects.create(name='test state')

        self.school = School.objects.create(name='New Bosten',
                                            school_type=self.school_type,
                                            state=self.state)

    def test_admin_can_read_school_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('school-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_school_detail(self):
        """
        Assert object detail read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('school-detail',
                                           args=[self.school.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_can_read_school_list(self):
        """
        Teacher can not create user
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('school-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_school_detail(self):
        """
        Assert object detail read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('school-detail',
                                           args=[self.school.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UpdateSchoolTest(APITestCase):
    """
    Update School API test cases
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.school_type = SchoolType.objects.create(name='test type')
        self.state = State.objects.create(name='test state')

        self.school = School.objects.create(name='New Bosten',
                                            school_type=self.school_type,
                                            state=self.state)

        self.data = SchoolSerializer(self.school).data
        self.data.update({
            'name': 'Changed',
        })

    def test_admin_can_update_school(self):
        """
        Admin can update school
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)

        response = self.client.put(reverse('school-detail',
                                           args=[self.school.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_update_school(self):
        """
        teacher can not update school
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)

        response = self.client.put(reverse('school-detail',
                                           args=[self.school.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class DeleteSchoolTest(APITestCase):
    """
    Delete School API test cases
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))
        self.school_type = SchoolType.objects.create(name='test type')
        self.state = State.objects.create(name='test state')

        self.school = School.objects.create(name='New Bosten',
                                            school_type=self.school_type,
                                            state=self.state)

    def test_admin_can_delete_school(self):
        """
        Assert delete success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)

        response = self.client.delete(
            reverse('school-detail', args=[self.school.id]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_can_not_delete_school(self):
        """
        Teacher can not delete school
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)

        response = self.client.delete(
            reverse('school-detail', args=[self.school.id]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class CreateSubjectTest(APITestCase):
    """
    Create Subject API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))
        self.data = {
            'name': 'english',
        }

    def test_admin_can_create_subject(self):
        """
        Assert object creation success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('subject-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_cannot_create_subject(self):
        """
        Teacher can not create subject
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('subject-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ReadSubjectTest(APITestCase):
    """
    Read Subject API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))
        self.subject = Subject.objects.create(name='english')

    def test_admin_can_read_subject_list(self):
        """
        Admin can read subject
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('subject-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_read_subject_detail(self):
        """
        Assert object detail read success
        """
        response = self.client.get(reverse('subject-detail',
                                           args=[self.subject.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_cannot_read_subject_list(self):
        """
        Teacher can not read subject list
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('subject-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_user_cannot_read_subject_detail(self):
        """
        Teacher can not read subject details
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('subject-detail',
                                           args=[self.subject.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class UpdateSubjectTest(APITestCase):
    """
    Update Subject API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))
        self.subject = Subject.objects.create(name='english')
        self.data = SubjectSerializer(self.subject).data
        self.data.update({
            'name': 'math',
        })

    def test_admin_can_update_subject(self):
        """
        Assert update success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('subject-detail',
                                           args=[self.subject.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_cannot_update_subject(self):
        """
        Teacher can not update subject
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('subject-detail',
                                           args=[self.subject.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class DeleteSubjectTest(APITestCase):
    """
    Delete Subject API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))
        self.subject = Subject.objects.create(name='english')

    def test_admin_can_delete_subject(self):
        """
        Assert delete success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(
            reverse('subject-detail', args=[self.subject.id]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_user_cannot_delete_subject(self):
        """
        Teacher user can not delete subject
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(
            reverse('subject-detail', args=[self.subject.id]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class CreateLearningIntelligenceTest(APITestCase):
    """
    Create Learning Intelligence API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.data = {
            'name': 'intelligence',
        }

    def test_admin_can_create_intelligence(self):
        """
        Assert object creation success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('learningintelligence-list'),
                                    self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_cannot_create_intelligence(self):
        """
        Teacher can not create intelligence
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('learningintelligence-list'),
                                    self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ReadLearningIntelligenceTest(APITestCase):
    """
    Read Learning Intelligence API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))
        self.intelligence = LearningIntelligence.objects.create(
            name='verbal-linguistic'
        )

    def test_admin_can_read_learningintelligence_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('learningintelligence-list'),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_learningintelligence_detail(self):
        """
        Assert object detail read success
        """
        response = self.client.get(reverse('learningintelligence-detail',
                                           args=[self.intelligence.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_cannot_read_learningintelligence_list(self):
        """
        Teacher can not read learning intelligence
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('learningintelligence-list'),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_admin_can_read_learningintelligence_detail(self):
        """
        Teacher can not read learning intelligence details
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('learningintelligence-detail',
                                           args=[self.intelligence.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class UpdateLearningIntelligenceTest(APITestCase):
    """
    Update Learning Intelligence API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))
        self.intelligence = LearningIntelligence.objects.create(
            name='verbal-linguistic'
        )
        self.data = LearningIntelligenceSerializer(self.intelligence).data
        self.data.update({
            'name': 'changed'
        })

    def test_admin_can_update_learningintelligence(self):
        """
        Assert update success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('learningintelligence-detail',
                                           args=[self.intelligence.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=user)

    def test_user_cannot_update_learningintelligence(self):
        """
        Teacher can not update learningintelligence
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('learningintelligence-detail',
                                           args=[self.intelligence.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class DeleteLearningIntelligenceTest(APITestCase):
    """
    Delete Learning Intelligence API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))
        self.intelligence = LearningIntelligence.objects.create(
            name='verbal-linguistic'
        )

    def test_admin_can_delete_learningintelligence(self):
        """
        Assert delete success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(
            reverse('learningintelligence-detail', args=[self.intelligence.id]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_user_cannot_delete_learningintelligence(self):
        """
        Teacher user can not delete learning intelligence
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(
            reverse('learningintelligence-detail', args=[self.intelligence.id]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class CreateActivityTest(APITestCase):
    """
    Create Activity API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))
        self.sub1 = Subject.objects.create(name='math')
        self.sub2 = Subject.objects.create(name='history')

        self.data1 = {
            'tittle': 'i_do',
            'description': 'description about activity',
            'subject': self.sub1.id,
        }

        self.data2 = {
            'tittle': 'we_do',
            'description': 'description about activity',
            'subject': self.sub2.id,
        }

    def test_admin_can_create_activity(self):
        """
        Assert object creation success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('activity-list'), self.data1,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_can_create_activity(self):
        """
        Assert object creation success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('activity-list'), self.data2,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.client.force_authenticate(user=None)


class ReadActivityTest(APITestCase):
    """
    Read Activity API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.activity = Activity.objects.create(
            tittle='i do',
            description='activity description',
            default_activity=True
        )

    def test_admin_can_read_activity_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('activity-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_activity_detail(self):
        """
        Assert object detail read success
        """
        response = self.client.get(reverse('activity-detail',
                                           args=[self.activity.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_can_read_activity_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('activity-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_activity_detail(self):
        """
        Assert object detail read success
        """
        response = self.client.get(reverse('activity-detail',
                                           args=[self.activity.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class UpdateActivityTest(APITestCase):
    """
    Update Activity API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.activity = Activity.objects.create(
            tittle='i do',
            description='activity description',
            default_activity=True
        )
        self.data = PostActivitySerializer(self.activity).data
        self.data.update({
            'tittle': 'we do'
        })

    def test_admin_can_update_activity(self):
        """
        Assert update success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('activity-detail',
                                           args=[self.activity.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_can_not_update_activity(self):
        """
        Teacher can not update activity
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('activity-detail',
                                           args=[self.activity.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class DeleteActivityTest(APITestCase):
    """
    Delete Activity API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))
        self.activity = Activity.objects.create(
            tittle='i do',
            description='activity description',
            created_by=self.user_2,
        )
        self.default_activity = Activity.objects.create(
            tittle='we do',
            description='activity description',
            default_activity=True
        )

    def test_admin_can_delete_activity(self):
        """
        Assert update success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('activity-detail',
                                              args=[self.default_activity.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_can_delete_activity(self):
        """
        Assert delete success
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('activity-detail',
                                              args=[self.activity.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_cannot_delete_default_activity(self):
        """
        Teacher can not delete default activity
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('activity-detail',
                                              args=[self.default_activity.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class CreateTemplateTest(APITestCase):
    """
    Create Template API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.data = {
            'name': 'template_name'
        }

    def test_admin_can_create_template(self):
        """
        Assert object creation success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('template-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_cannot_create_template(self):
        """
        Techer can not create template
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('template-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ReadTemplateTest(APITestCase):
    """
    Read Template API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))
        self.template = Template.objects.create(name='temlplate')

    def test_can_read_template_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('template-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_read_template_detail(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('template-detail',
                                           args=[self.template.id]), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_template_list(self):
        """
        Teacher can read template
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('template-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_template_detail(self):
        """
        Teacher can read template detail
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('template-detail',
                                           args=[self.template.id]), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UpdateTemplateTest(APITestCase):
    """
    Update Template API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.template = Template.objects.create(
            name='template'
        )
        self.data = PostTemplateSerializer(self.template).data
        self.data.update({
            'name': 'template2'
        })

    def test_admin_can_update_template(self):
        """
        Admin can update template
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('template-detail',
                                           args=[self.template.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_not_update_template(self):
        """
        Teacher can not update template
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('template-detail',
                                           args=[self.template.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class DeleteTemplateTest(APITestCase):
    """
    Delete Template API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.template = Template.objects.create(
            name='template'
        )

    def test_admin_can_delete_template(self):
        """
        Assert template success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('template-detail',
                                              args=[self.template.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_user_cannot_delete_template(self):
        """
        Teacher can not delete template success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('template-detail',
                                              args=[self.template.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class CreateClassTest(APITestCase):
    """
    Create Class API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.user_3 = User.objects.create(
            username='test111@gmail.com',
            password='testpassword',
            email='test111@gmail.com',
        )
        self.user_3.groups.add(Group.objects.get(name=grp))

        self.data = {
            'name': '9th class',
            'created_by': self.user_1.id,
            'co_teacher': [self.user_2.id, ]
        }
        self.data1 = {
            'name': '9th class',
            'created_by': self.user_2.id,
            'co_teacher': [self.user_3.id, ]
        }

    def test_admin_can_create_class(self):
        """
        Assert object creation success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('class-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_can_create_class(self):
        """
        Teacher can create class
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('class-list'), self.data1,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class ReadClassTest(APITestCase):
    """
    Read Class API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))
        self.classes = Class.objects.create(
            name='9th class',
            created_by=self.user_1,
        )
        self.classes.co_teacher.add(self.user_2)

    def test_admin_can_read_class_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('class-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_class_detail(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('class-detail',
                                           args=[self.classes.id]), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_class_list(self):
        """
        Teacher can read class
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('class-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_class_detail(self):
        """
        Teacher can read class detail
        """
        response = self.client.get(reverse('class-detail',
                                           args=[self.classes.id]), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UpdateClassTest(APITestCase):
    """
    Update Class API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.classes = Class.objects.create(
            name='9th class', created_by=self.user_2)
        # self.classes.co_teacher.add(self.user_2)
        self.data = PostClassSerializer(self.classes).data
        self.data.update({
            'name': '10 th class'
        })

    def test_admin_can_update_class(self):
        """
        Admin can update class
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('class-detail',
                                           args=[self.classes.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_update_class(self):
        """
        Teacher can update class
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('class-detail',
                                           args=[self.classes.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class DeleteClassTest(APITestCase):
    """
    Delete Class API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.classes = Class.objects.create(
            name='9th class', created_by=self.user_2)
        self.classes.co_teacher.add(self.user_2)

    def test_admin_can_delete_class(self):
        """
        Assert class success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('class-detail',
                                              args=[self.classes.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_user_can_delete_class(self):
        """
        Teacher can delete class success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('class-detail',
                                              args=[self.classes.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class CreateStandardTest(APITestCase):
    """
    Create Standard API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.data1 = {
            'name': 'ELACC9-10RL2',
            'created_by': self.user_1.id
        }
        self.data2 = {
            'name': 'ELACC9-10RL2',
            'created_by': self.user_2.id
        }

    def test_admin_can_create_standards(self):
        """
        Assert object creation success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('standards-list'), self.data1,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_can_create_standards(self):
        """
        Techer can create standard
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('standards-list'), self.data2,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class ReadStandardTest(APITestCase):
    """
    Read Standard API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.standard = Standards.objects.create(name='ELACC9-10RL2',
                                                 created_by=self.user_2)

    def test_can_read_standard_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('standards-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_read_standard_detail(self):
        """
        Assert object read success
        """
        response = self.client.get(reverse('standards-detail',
                                           args=[self.standard.id]), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_standard_list(self):
        """
        Teacher can read standard
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('standards-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_standard_detail(self):
        """
        Teacher can read standard detail
        """
        response = self.client.get(reverse('standards-detail',
                                           args=[self.standard.id]), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UpdateStandardTest(APITestCase):
    """
    Update Standard API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.standard = Standards.objects.create(
            name='ELACC9-10RL2', created_by=self.user_2
        )
        self.data = StandardSerializer(self.standard).data
        self.data.update({
            'name': 'ELACC9-10RL2'
        })

    def test_admin_can_update_standards(self):
        """
        Admin can update standard
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('standards-detail',
                                           args=[self.standard.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_update_standard(self):
        """
        Teacher can not update standard
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('standards-detail',
                                           args=[self.standard.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class DeleteStandardTest(APITestCase):
    """
    Delete Standard API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.standard = Standards.objects.create(
            name='ELACC9-10RL2', created_by=self.user_2
        )

    def test_admin_can_delete_standard(self):
        """
        Assert class success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('standards-detail',
                                              args=[self.standard.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_user_cannot_delete_standard(self):
        """
        Teacher can delete standard success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('standards-detail',
                                              args=[self.standard.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class CreateLessonPlanTest(APITestCase):
    """
    Create Lesson Plan API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.standard = Standards.objects.create(name='ELACC9-10RL2')

        self.data = {
            'tittle': 'english',
            'objective': 'lesson objective',
            'standard': [self.standard.id],
            'created_by': self.user_2.id,
            'last_updated': datetime.now(),
            'differentiation': 'differentiation text',
            'accommodations': 'accommodations text',
            'essential_questions': 'essential_questions text',
            'assessment': 'assessment text',
            'use_of_technology': 'key_vocabulary text',
            'vocabulary': 'extension text',
        }

    def test_admin_can_create_lessonplan(self):
        """
        Assert object creation success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('lessonplan-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_can_create_lessonplan(self):
        """
        Techer can create lesson plan
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('lessonplan-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class ReadLessonPlanTest(APITestCase):
    """
    Read Lesson Plan API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.klass = Class.objects.create(name='test', created_by=self.user_2)

        self.standard = Standards.objects.create(name='ELACC9-10RL2')
        self.lesson_plan = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2,
            last_updated=datetime.now(),
            lesson_class=self.klass,
            differentiation='differentiation text',
            accommodations='accommodations text',
            essential_questions='essential_questions text',
            assessment='assessment text',
            use_of_technology='key_vocabulary text',
            vocabulary='extension text',
        )
        self.lesson_plan.standard.set([self.standard])

    def test_can_read_lessonplan_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('lessonplan-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_read_lessonplan_detail(self):
        """
        Assert object read success
        """
        response = self.client.get(reverse('lessonplan-detail',
                                           args=[self.lesson_plan.id]), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_lessonplan_list(self):
        """
        Teacher can read lesson plan
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('lessonplan-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_lessonplan_detail(self):
        """
        Teacher can read lessonplan detail
        """
        response = self.client.get(reverse('lessonplan-detail',
                                           args=[self.lesson_plan.id]), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UpdateLessonPlanTest(APITestCase):
    """
    Update Lesson Plan API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))
        self.klass = Class.objects.create(name='test', created_by=self.user_2)

        self.standard = Standards.objects.create(name='ELACC9-10RL2')
        self.lesson_plan = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2,
            lesson_class=self.klass
        )
        # self.lesson_plan.standard.set([self.standard])
        self.data = PostLessonPlanSerializer(self.lesson_plan).data
        self.data.update({
            'tittle': 'math',
            'last_updated': datetime.now()
        })

    def test_admin_can_update_lessonplan(self):
        """
        Admin can update lessonplan
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('lessonplan-detail',
                                           args=[self.lesson_plan.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_update_lesson_plan(self):
        """
        Teacher can update lesson plan
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('lessonplan-detail',
                                           args=[self.lesson_plan.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class DeleteLessonPlanTest(APITestCase):
    """
    Delete lessonplan API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.standard = Standards.objects.create(name='ELACC9-10RL2')
        self.lesson_plan1 = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2,
            mark_as_complete=True,
            last_updated=datetime.now()
        )
        self.lesson_plan1.standard.set([self.standard])

        self.lesson_plan2 = LessonPlan.objects.create(
            tittle='math',
            objective='lesson objective',
            created_by=self.user_2,
            mark_as_complete=False,
            last_updated=datetime.now()
        )
        self.lesson_plan2.standard.set([self.standard])

    def test_admin_can_delete_lessonplan(self):
        """
        Assert delete object success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('lessonplan-detail',
                                              args=[self.lesson_plan1.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_user_can_delete_lessonplan(self):
        """
        Teacher can delete lesson plan which are not mark as complete
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('lessonplan-detail',
                                              args=[self.lesson_plan2.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class CreateRateLessonTest(APITestCase):
    """
    Create Rate Lesson API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.standard = Standards.objects.create(name='ELACC9-10RL2')

        self.lesson_plan = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2
        )
        self.lesson_plan.standard.set([self.standard])

        self.data = {
            'notes': 'some feedback',
            'lesson_plan': self.lesson_plan.id,
            'rated_by': self.user_2.id
        }

    def test_admin_can_rate_lessonplan(self):
        """
        Assert object creation success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('ratelesson-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_can_rate_lessonplan(self):
        """
        Teacher can rate lesson plan
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('ratelesson-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class ReadRateLessonTest(APITestCase):
    """
    Read Rate Lesson Plan API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.standard = Standards.objects.create(name='ELACC9-10RL2')
        self.lesson_plan = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2
        )
        self.lesson_plan.standard.set([self.standard])

        self.rate_lesson = RateLesson.objects.create(
            notes='some feedback',
            lesson_plan=self.lesson_plan,
            rated_by=self.user_2
        )

    def test_can_read_lessonplan_rating_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('ratelesson-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_read_lessonplan_rating_detail(self):
        """
        Assert object read success
        """
        response = self.client.get(reverse('ratelesson-detail',
                                           args=[self.rate_lesson.id]), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_lessonplan_rating_list(self):
        """
        Teacher can read lesson plan rating
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('ratelesson-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_lessonplan_rating_detail(self):
        """
        Teacher can read lessonplan rating detail
        """
        response = self.client.get(reverse('ratelesson-detail',
                                           args=[self.rate_lesson.id]), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UpdateRatePlanTest(APITestCase):
    """
    Update Lesson Plan Rating API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.standard = Standards.objects.create(name='ELACC9-10RL2')
        self.lesson_plan = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2
        )
        self.lesson_plan.standard.set([self.standard])

        self.rate_lesson = RateLesson.objects.create(
            engagement=4.0,
            notes='some feedback',
            lesson_plan=self.lesson_plan,
            rated_by=self.user_2
        )

        self.data = RateLessonSerializer(self.rate_lesson).data
        self.data.update({
            'engagement': 5.0
        })

    def test_admin_can_update_lessonplan_rating(self):
        """
        Admin can update lessonplan rating
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('ratelesson-detail',
                                           args=[self.rate_lesson.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_not_update_lesson_plan_rating(self):
        """
        Teacher can update lesson plan rating
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('ratelesson-detail',
                                           args=[self.rate_lesson.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class DeleteRateLessonTest(APITestCase):
    """
    Delete lessonplan rating API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.standard = Standards.objects.create(name='ELACC9-10RL2')

        self.lesson_plan = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2,
        )
        self.lesson_plan.standard.set([self.standard])

        self.rate_lesson = RateLesson.objects.create(
            notes='some feedback',
            lesson_plan=self.lesson_plan,
            rated_by=self.user_2
        )

    def test_admin_can_delete_lessonplan_rating(self):
        """
        Assert delete object success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('ratelesson-detail',
                                              args=[self.rate_lesson.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_user_cannot_delete_lessonplan_rating(self):
        """
        Teacher can not delete lesson plan rating
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('ratelesson-detail',
                                              args=[self.rate_lesson.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class CreateContainerTest(APITestCase):
    """
    Create Container API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.data = {
            'title': 'container title',
        }

    def test_admin_can_create_container(self):
        """
        Assert object creation success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('container-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_cannot_create_container(self):
        """
        Teacher cannot create container
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('container-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ReadContainerTest(APITestCase):
    """
    Read Container API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.container = Container.objects.create(
            title='some tittle',
        )

    def test_can_read_container_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('container-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_read_container_detail(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('container-detail',
                                           args=[self.container.id]), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_container_list(self):
        """
        Teacher can read container list
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('container-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_container_detail(self):
        """
        Teacher can read container detail
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('container-detail',
                                           args=[self.container.id]), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UpdateContainerTest(APITestCase):
    """
    Update Container API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.container = Container.objects.create(
            title='container tittle'
        )

        self.data = ContainerSerializer(self.container).data
        self.data.update({
            'title': 'some title'
        })

    def test_admin_can_update_container(self):
        """
        Admin can update container
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('container-detail',
                                           args=[self.container.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_cannot_update_container(self):
        """
        Teacher cannot update container
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('container-detail',
                                           args=[self.container.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class DeleteContainerest(APITestCase):
    """
    Delete container API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.container = Container.objects.create(
            title='container title',
        )

    def test_admin_can_delete_container(self):
        """
        Assert delete object success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('container-detail',
                                              args=[self.container.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_user_cannot_delete_container(self):
        """
        Teacher can not delete container
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('container-detail',
                                              args=[self.container.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class CreateActivityContainerTest(APITestCase):
    """
    Create Activity Container API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        # self.activity = Activity.objects.create(
        #     tittle='i do',
        #     description='activity description',
        # )

        self.standard = Standards.objects.create(name='ELACC9-10RL2')
        # self.lesson_plan = LessonPlan.objects.create(
        #     tittle='english',
        #     objective='lesson objective',
        #     created_by=self.user_2
        # )
        self.template = Template.objects.create(name='template')
        self.container = Container.objects.create(title='some tittle')

        self.data = {
            # 'activity': self.activity.id,
            # 'lesson_plan': self.lesson_plan.id,
            'template': self.template.id,
            'container': self.container.id
        }

    def test_admin_can_create_activity_container(self):
        """
        Assert object creation success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('activitycontainer-list'),
                                    self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_can_create_activity_container(self):
        """
        Teacher can create activity container
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('activitycontainer-list'),
                                    self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class ReadActivityContainerTest(APITestCase):
    """
    Read Activity Container API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.template = Template.objects.create(name='template')
        self.container = Container.objects.create(title='some tittle')
        self.activity_container = ActivityContainer.objects.create(
            template=self.template,
            container=self.container
        )

    def test_can_read_activity_container_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('activitycontainer-list'),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_read_activity_container_detail(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('activitycontainer-detail',
                                           args=[self.activity_container.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_activity_container_list(self):
        """
        Teacher can read container list
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('activitycontainer-list'),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_activity_container_detail(self):
        """
        Teacher can read activity container detail
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('activitycontainer-detail',
                                           args=[self.activity_container.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UpdateActivityContainerTest(APITestCase):
    """
    Update Activity Container API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.template = Template.objects.create(name='template')
        self.template_1 = Template.objects.create(name='template1')
        self.container = Container.objects.create(title='some tittle')
        self.activity_container = ActivityContainer.objects.create(
            template=self.template,
            container=self.container
        )

        self.data = PostActivityContainerSerializer(
            self.activity_container
        ).data
        self.data.update({
            'template': self.template_1.id
        })

    def test_admin_can_update_activity_container(self):
        """
        Admin can update activity container
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('activitycontainer-detail',
                                           args=[self.activity_container.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_update_activity_container(self):
        """
        Teacher can update activity container
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('activitycontainer-detail',
                                           args=[self.activity_container.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class DeleteActivityContaineres(APITestCase):
    """
    Delete activity container API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.template = Template.objects.create(name='template')
        self.container = Container.objects.create(title='some tittle')
        self.activity_container = ActivityContainer.objects.create(
            template=self.template,
            container=self.container
        )

    def test_admin_can_delete_activity_container(self):
        """
        Assert delete object success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('activitycontainer-detail',
                                              args=[self.activity_container.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_user_can_delete_activity_container(self):
        """
        Teacher can delete activity container
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('activitycontainer-detail',
                                              args=[self.activity_container.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class CreateTagElementTest(APITestCase):
    """
    Create Tag Element API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.standard = Standards.objects.create(name='ELACC9-10RL2')

        self.data = {
            'element': 'some element',
            'standards': self.standard.id,
        }

    def test_admin_can_create_tag_element(self):
        """
        Assert object creation success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('tagelement-list'),
                                    self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_can_create_tag_element(self):
        """
        Teacher can create tag element
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('tagelement-list'),
                                    self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class ReadTagElementTest(APITestCase):
    """
    Read Tag Element API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.standard = Standards.objects.create(name='ELACC9-10RL2')

        self.tag_element = TagElement.objects.create(
            element='some element',
            standards=self.standard,
        )

    def test_can_read_tag_element_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('tagelement-list'),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_read_tag_element_detail(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('tagelement-detail',
                                           args=[self.tag_element.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_tag_element_list(self):
        """
        Teacher can read tag element list
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('tagelement-list'),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_tag_element_detail(self):
        """
        Teacher can read tag element detail
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('tagelement-detail',
                                           args=[self.tag_element.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UpdateTagElementTest(APITestCase):
    """
    Update Tag Element API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.standard = Standards.objects.create(name='ELACC9-10RL2')
        self.tag_element = TagElement.objects.create(
            element='some element',
            standards=self.standard
        )

        self.data = TagElementSerializer(self.tag_element).data
        self.data.update({
            'element': 'element2'
        })

    def test_admin_can_update_tag_element(self):
        """
        Admin can update tag element
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('tagelement-detail',
                                           args=[self.tag_element.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_update_tag_element(self):
        """
        Teacher can update tag element
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('tagelement-detail',
                                           args=[self.tag_element.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class DeleteTagElement(APITestCase):
    """
    Delete tag element API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.standard = Standards.objects.create(name='ELACC9-10RL2')
        self.tag_element = TagElement.objects.create(
            element='some element',
            standards=self.standard
        )

    def test_admin_can_delete_tag_element(self):
        """
        Assert delete object success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('tagelement-detail',
                                              args=[self.tag_element.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_user_cannot_delete_tag_element(self):
        """
        Teacher can not delete tag element
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('tagelement-detail',
                                              args=[self.tag_element.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


# class CreateAttachmentTest(APITestCase):
#     """
#     Create Attachment API test case
#     """
#     def setUp(self):
#         """
#         Test case init data
#         """
#         self.superuser = User.objects.create_superuser('john', 'john@snow.com',
#                                                        'johnpassword')
#         user = User.objects.get(username='john')
#         self.client.force_authenticate(user=user)

#         grp = Group(name='administrator')
#         grp.save()
#         self.user_1 = User.objects.create(
#             username='mike@tyson.com',
#             password='mikepassword123',
#             email='mike@tyson.com',
#         )
#         self.user_1.groups.add(Group.objects.get(name=grp))

#         grp = Group(name='teacher')
#         grp.save()
#         self.user_2 = User.objects.create(
#             username='test@gmail.com',
#             password='testpassword',
#             email='test@gmail.com',
#         )
#         self.user_2.groups.add(Group.objects.get(name=grp))

#         self.data = {
#             'attachment': StringIO('test'),
#         }

#     @patch('django.core.files.storage.FileSystemStorage.save')
#     def test_admin_can_create_attachment(self, mock_save):
#         """
#         Assert object creation success
#         """
#         mock_save.return_value = 'Test1.pdf'
#         self.client.force_authenticate(user=None)
#         user = User.objects.get(username=self.user_1.username)
#         self.client.force_authenticate(user=user)
#         response = self.client.post(reverse('attachment-list'),
#                                     self.data, format='multipart')
#         self.assertEqual(response.status_code, status.HTTP_201_CREATED)

#     @patch('django.core.files.storage.FileSystemStorage.save')
#     def test_user_can_create_attachment(self, mock_save):
#         """
#         Teacher can create attachments
#         """
#         mock_save.return_value = 'Test1.pdf'
#         self.client.force_authenticate(user=None)
#         user = User.objects.get(username=self.user_2.username)
#         self.client.force_authenticate(user=user)
#         response = self.client.post(reverse('attachment-list'),
#                                     self.data, format='multipart')
#         self.assertEqual(response.status_code, status.HTTP_201_CREATED)


# class ReadAttachmentTest(APITestCase):
#     """
#     Read Attachment API test case
#     """
#     @patch('django.core.files.storage.FileSystemStorage.save')
#     def setUp(self, mock_save):
#         """
#         Test case init data
#         """
#         self.superuser = User.objects.create_superuser('john', 'john@snow.com',
#                                                        'johnpassword')
#         user = User.objects.get(username='john')
#         self.client.force_authenticate(user=user)

#         grp = Group(name='administrator')
#         grp.save()
#         self.user_1 = User.objects.create(
#             username='mike@tyson.com',
#             password='mikepassword',
#             email='mike@tyson.com',
#         )
#         self.user_1.groups.add(Group.objects.get(name=grp))

#         grp = Group(name='teacher')
#         grp.save()
#         self.user_2 = User.objects.create(
#             username='test@gmail.com',
#             password='testpassword',
#             email='test@gmail.com',
#         )
#         self.user_2.groups.add(Group.objects.get(name=grp))

#         file_mock = MagicMock(spec=File, name='FileMock')
#         file_mock.name = 'test.pdf'
#         mock_save.return_value = 'Test.pdf'

#         self.attachment = Attachment.objects.create(attachment=file_mock)

#     def test_can_read_attachment_list(self):
#         """
#         Assert object read success
#         """
#         self.client.force_authenticate(user=None)
#         user = User.objects.get(username=self.user_1.username)
#         self.client.force_authenticate(user=user)
#         response = self.client.get(reverse('attachment-list'),
#                                    format='multipart')
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

#     def test_can_read_attachment_detail(self):
#         """
#         Assert object read success
#         """
#         self.client.force_authenticate(user=None)
#         user = User.objects.get(username=self.user_1.username)
#         self.client.force_authenticate(user=user)
#         response = self.client.get(reverse('attachment-detail',
#                                            args=[self.attachment.id]),
#                                    format='multipart')
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

#     def test_user_can_read_attachment_list(self):
#         """
#         Teacher can read attachment list
#         """
#         self.client.force_authenticate(user=None)
#         user = User.objects.get(username=self.user_2.username)
#         self.client.force_authenticate(user=user)
#         response = self.client.get(reverse('attachment-list'),
#                                    format='multipart')
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

#     def test_user_can_read_attachment_detail(self):
#         """
#         Teacher can read attachment detail
#         """
#         self.client.force_authenticate(user=None)
#         user = User.objects.get(username=self.user_2.username)
#         self.client.force_authenticate(user=user)
#         response = self.client.get(reverse('attachment-detail',
#                                            args=[self.attachment.id]),
#                                    format='multipart')
#         self.assertEqual(response.status_code, status.HTTP_200_OK)


# class UpdateAttachmentTest(APITestCase):
#     """
#     Update Attachment API test case
#     """

#     @patch('django.core.files.storage.FileSystemStorage.save')
#     def setUp(self, mock_save):
#         """
#         Test case init data
#         """
#         self.superuser = User.objects.create_superuser('john', 'john@snow.com',
#                                                        'johnpassword')
#         user = User.objects.get(username='john')
#         self.client.force_authenticate(user=user)

#         grp = Group(name='administrator')
#         grp.save()
#         self.user_1 = User.objects.create(
#             username='mike@tyson.com',
#             password='mikepassword123',
#             email='mike@tyson.com',
#         )
#         self.user_1.groups.add(Group.objects.get(name=grp))

#         grp = Group(name='teacher')
#         grp.save()
#         self.user_2 = User.objects.create(
#             username='test@gmail.com',
#             password='testpassword',
#             email='test@gmail.com',
#         )
#         self.user_2.groups.add(Group.objects.get(name=grp))

#         file_mock = MagicMock(spec=File, name='FileMock')
#         file_mock.name = 'test1.pdf'
#         mock_save.return_value = 'Test.pdf'

#         self.attachment = Attachment.objects.create(
#             attachment=file_mock,
#         )

#         self.data = AttachmentSerializer(self.attachment).data
#         self.data.update({
#             'attachment': StringIO('test')
#         })

#     @patch('django.core.files.storage.FileSystemStorage.save')
#     def test_admin_can_update_attachment(self, mock_save):
#         """
#         Admin can update attachment
#         """
#         mock_save.return_value = 'Test.pdf'
#         self.client.force_authenticate(user=None)
#         user = User.objects.get(username=self.user_1.username)
#         self.client.force_authenticate(user=user)
#         response = self.client.put(reverse('attachment-detail',
#                                            args=[self.attachment.id]),
#                                    self.data, format='multipart')
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

#     @patch('django.core.files.storage.FileSystemStorage.save')
#     def test_user_can_update_attachment(self, mock_save):
#         """
#         Teacher can update attachment
#         """
#         mock_save.return_value = 'Test.pdf'
#         user = User.objects.get(username=self.user_2.username)
#         self.client.force_authenticate(user=user)
#         response = self.client.put(reverse('attachment-detail',
#                                            args=[self.attachment.id]),
#                                    self.data, format='multipart')
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#         self.client.force_authenticate(user=None)


# class DeleteAttachmentTest(APITestCase):
#     """
#     Delete attachment API test case
#     """

#     @patch('django.core.files.storage.FileSystemStorage.save')
#     def setUp(self, mock_save):
#         """
#         Test case init data
#         """
#         self.superuser = User.objects.create_superuser('john', 'john@snow.com',
#                                                        'johnpassword')
#         user = User.objects.get(username='john')
#         self.client.force_authenticate(user=user)

#         grp = Group(name='administrator')
#         grp.save()
#         self.user_1 = User.objects.create(
#             username='mike@tyson.com',
#             password='mikepassword123',
#             email='mike@tyson.com',
#         )
#         self.user_1.groups.add(Group.objects.get(name=grp))

#         grp = Group(name='teacher')
#         grp.save()
#         self.user_2 = User.objects.create(
#             username='test@gmail.com',
#             password='testpassword',
#             email='test@gmail.com',
#         )
#         self.user_2.groups.add(Group.objects.get(name=grp))


#         file_mock = MagicMock(spec=File, name='FileMock')
#         file_mock.name = 'test1.pdf'
#         mock_save.return_value = 'Test.pdf'

#         self.attachment = Attachment.objects.create(
#             attachment=file_mock,
#         )

#     def test_admin_can_delete_attachment(self):
#         """
#         Assert delete object success
#         """
#         self.client.force_authenticate(user=None)
#         user = User.objects.get(username=self.user_1.username)
#         self.client.force_authenticate(user=user)
#         response = self.client.delete(reverse('attachment-detail',
#                                               args=[self.attachment.id]),
#                                       format='json')
#         self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

#     def test_user_cannot_delete_attachment(self):
#         """
#         Teacher can  delete attachment
#         """
#         self.client.force_authenticate(user=None)
#         user = User.objects.get(username=self.user_2.username)
#         self.client.force_authenticate(user=user)
#         response = self.client.delete(reverse('attachment-detail',
#                                               args=[self.attachment.id]),
#                                       format='json')
#         self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class CreateLessonActivityTest(APITestCase):
    """
    Create LessonActivity API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.activity = Activity.objects.create(
            tittle='i do',
            description='activity description',
        )

        self.lesson_plan = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2
        )

        self.data = {
            'set_duration': 10,
            'set_grouping': 'whole_class',
            'lesson_plan': self.lesson_plan.id,
            'activity':  self.activity.id,
            'activity_guide': False
        }

    def test_admin_can_create_lesson_activity(self):
        """
        Assert object creation success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('lessonactivity-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_can_create_lesson_activity(self):
        """
        Assert object creation success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('lessonactivity-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.client.force_authenticate(user=None)


class ReadLessonActivityTest(APITestCase):
    """
    Read LessonActivity API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.activity = Activity.objects.create(
            tittle='i do',
            description='activity description',
        )

        self.lesson_plan = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2
        )

        self.lesson_activity = LessonActivity.objects.create(
            set_duration=10,
            set_grouping='whole_class',
            activity=self.activity,
            lesson_plan=self.lesson_plan,
            activity_guide=False
        )

    def test_admin_can_read_lesson_activity_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('lessonactivity-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_activity_detail(self):
        """
        Assert object detail read success
        """
        response = self.client.get(reverse('lessonactivity-detail',
                                           args=[self.lesson_activity.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_can_read_lesson_activity_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('lessonactivity-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_lesson_activity_detail(self):
        """
        Assert object detail read success
        """
        response = self.client.get(reverse('lessonactivity-detail',
                                           args=[self.lesson_activity.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class UpdateLessonActivityTest(APITestCase):
    """
    Update LessonActivity API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.activity = Activity.objects.create(
            tittle='i do',
            description='activity description',
        )

        self.lesson_plan = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2
        )

        self.lesson_activity = LessonActivity.objects.create(
            set_duration=10,
            set_grouping='whole_class',
            activity=self.activity,
            lesson_plan=self.lesson_plan,
            activity_guide=False
        )
        self.data = PostLessonActivitySerializer(self.lesson_activity).data
        self.data.update({
            'set_duration': 20
        })

    def test_admin_can_update_lesson_activity(self):
        """
        Assert update success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('lessonactivity-detail',
                                           args=[self.lesson_activity.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_can_update_lesson_activity(self):
        """
        Teacher can update lesson activity
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('lessonactivity-detail',
                                           args=[self.lesson_activity.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class DeleteLessonActivityTest(APITestCase):
    """
    Delete LessonActivity API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.activity = Activity.objects.create(
            tittle='i do',
            description='activity description',
        )
        self.lesson_plan = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2
        )

        self.lesson_activity = LessonActivity.objects.create(
            set_duration=10,
            set_grouping='whole_class',
            activity=self.activity,
            lesson_plan=self.lesson_plan,
            activity_guide=False
        )

    def test_admin_can_delete_lesson_activity(self):
        """
        Assert update success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('lessonactivity-detail',
                                              args=[self.lesson_activity.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_cannot_delete_lesson_activity(self):
        """
        Teacher can delete lesson activity
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('lessonactivity-detail',
                                              args=[self.lesson_activity.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)


class CreateActivityCommentTest(APITestCase):
    """
    Create Activity Comment API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.activity = Activity.objects.create(
            tittle='i do',
            description='activity description',
        )

        self.lesson_plan = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2
        )

        self.lesson_activity = LessonActivity.objects.create(
            set_duration=10,
            set_grouping='whole_class',
            lesson_plan=self.lesson_plan,
            activity=self.activity
        )

        self.data = {
            'comment': 'Comment',
            'commented_by': self.user_2.id,
            'lesson_activity': self.lesson_activity.id,
            'reviewed': False
        }

    def test_admin_can_create_activity_comment(self):
        """
        Assert object creation success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('activitycomment-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_can_create_activity_comment(self):
        """
        Assert object creation success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('activitycomment-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.client.force_authenticate(user=None)


class ReadActivityCommentTest(APITestCase):
    """
    Read ActivityComment API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.activity = Activity.objects.create(
            tittle='i do',
            description='activity description',
        )

        self.lesson_plan = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2
        )

        self.lesson_activity = LessonActivity.objects.create(
            set_duration=10,
            set_grouping='whole_class',
            activity=self.activity,
            lesson_plan=self.lesson_plan
        )
        self.activity_comment = ActivityComment.objects.create(
            comment='Comment',
            commented_by=self.user_2,
            lesson_activity=self.lesson_activity,
            reviewed=False
        )

    def test_admin_can_read_activity_comment_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('activitycomment-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_activity_comment_detail(self):
        """
        Assert object detail read success
        """
        response = self.client.get(reverse('activitycomment-detail',
                                           args=[self.activity_comment.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class UpdateActivityCommentTest(APITestCase):
    """
    Update Activity Comment API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.activity = Activity.objects.create(
            tittle='i do',
            description='activity description',
        )

        self.lesson_plan = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2
        )

        self.lesson_activity = LessonActivity.objects.create(
            set_duration=10,
            set_grouping='whole_class',
            activity=self.activity,
            lesson_plan=self.lesson_plan
        )
        self.activity_comment = ActivityComment.objects.create(
            comment='Comment',
            commented_by=self.user_2,
            lesson_activity=self.lesson_activity,
            reviewed=False
        )
        self.data = PostActivityCommentSerializer(self.activity_comment).data
        self.data.update({
            'reviewed': True
        })

    def test_admin_can_update_activity_comment(self):
        """
        Assert update success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('activitycomment-detail',
                                           args=[self.activity_comment.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_can_update_activity_comment(self):
        """
        Teacher can update activity comment
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('activitycomment-detail',
                                           args=[self.activity_comment.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class DeleteActivityCommentTest(APITestCase):
    """
    Delete ActivityComment API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.activity = Activity.objects.create(
            tittle='i do',
            description='activity description',
        )
        self.lesson_plan = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2
        )

        self.lesson_activity = LessonActivity.objects.create(
            set_duration=10,
            set_grouping='whole_class',
            activity=self.activity,
            lesson_plan=self.lesson_plan
        )

        self.activity_comment = ActivityComment.objects.create(
            comment='Comment',
            commented_by=self.user_2,
            lesson_activity=self.lesson_activity,
            reviewed=False
        )

    def test_admin_can_delete_activity_comment(self):
        """
        Assert delete success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('activitycomment-detail',
                                              args=[self.activity_comment.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_can_delete_activity_comment(self):
        """
        Teacher can delete activity comment
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('activitycomment-detail',
                                              args=[self.activity_comment.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)


class CreateActivityTemplateTest(APITestCase):
    """
    Create Activity Comment API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.activity = Activity.objects.create(
            tittle='i do',
            description='activity description',
        )

        self.data = {
            'name': 'activity template name',
            'activity': self.activity.id,
            'description': "activity template description"
        }

    def test_admin_can_create_activity_template(self):
        """
        Test admin can create activity template.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('activitytemplate-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_cannot_create_activity_template(self):
        """
        Test teacher cannot create activity template.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('activitytemplate-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class ReadActivityTemplateTest(APITestCase):
    """
    Read Activity Template API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.activity = Activity.objects.create(
            tittle='i do',
            description='activity description',
        )

        self.activity_template = ActivityTemplate.objects.create(
            name='activity template name',
            activity=self.activity,
            description='activity template description'
        )

    def test_admin_can_read_activity_template_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('activitytemplate-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_activity_template_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('activitytemplate-detail',
                                           args=[self.activity_template.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_teacher_can_read_activity_template_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('activitytemplate-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_teacher_can_read_activity_template_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('activitytemplate-detail',
                                           args=[self.activity_template.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class UpdateActivityTemplateTest(APITestCase):
    """
    Update Activity Template API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.activity = Activity.objects.create(
            tittle='i do',
            description='activity description',
        )

        self.activity_template = ActivityTemplate.objects.create(
            name='activity template name',
            activity=self.activity,
            description='activity template description'
        )

        self.data = ActivityTemplateSerializer(self.activity_template).data
        self.data.update({
            'name': 'activity template name updated'
        })

    def test_admin_can_update_activity_template(self):
        """
        Assert update success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('activitytemplate-detail',
                                           args=[self.activity_template.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_cannot_update_activity_template(self):
        """
        Teacher can update activity template
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('activitytemplate-detail',
                                           args=[self.activity_template.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class DeleteActivityTemplateTest(APITestCase):
    """
    Delete ActivityTemplate API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.activity = Activity.objects.create(
            tittle='i do',
            description='activity description',
        )
        self.activity_template = ActivityTemplate.objects.create(
            name='activity template name',
            activity=self.activity,
            description='activity template description'
        )

    def test_admin_can_delete_activity_template(self):
        """
        Test admin can delete activity template.
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('activitytemplate-detail',
                                              args=[self.activity_template.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_cannot_delete_activity_template(self):
        """
        Teacher can delete activity template
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('activitytemplate-detail',
                                              args=[self.activity_template.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class CreateSchoolDocumentTest(APITestCase):
    """
    Create school document API test case.
    """

    def setUp(self):
        """
        Test case init data.
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.data = {
            'instruction': 'instruction here',
            'content': 'content link here',
            'learning_environment': 'class room management description',
            'differentiation': True,
            'accommodations': True,
            'essential_questions': False,
            'remediation': True,
            'use_of_technology': True,
            'vocabulary': True
        }

    def test_admin_can_school_document(self):
        """
        Test admin can create school document.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('schooldocument-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_cannot_create_school_document(self):
        """
        Test teacher cannot create school document.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('schooldocument-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class ReadSchoolDocumentTest(APITestCase):
    """
    Read School Document API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.school_document = SchoolDocument.objects.create(
            instruction='instruction here',
            content='content link here',
            learning_environment='class room management description',
            differentiation=True,
            accommodations=True,
            essential_questions=False,
            remediation=True,
            use_of_technology=True,
            vocabulary=True
        )

    def test_admin_can_read_school_documents_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('schooldocument-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_school_document_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('schooldocument-detail',
                                           args=[self.school_document.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_teacher_cannot_read_school_document_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('schooldocument-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_teacher_cannot_read_school_document_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('schooldocument-detail',
                                           args=[self.school_document.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class UpdateSchoolDocumentTest(APITestCase):
    """
    Update School Document API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.school_document = SchoolDocument.objects.create(
            instruction='instruction here',
            content='content link here',
            learning_environment='class room management description',
            differentiation=True,
            accommodations=True,
            essential_questions=False,
            remediation=True,
            use_of_technology=True,
            vocabulary=True
        )

        self.data = SchoolDocumentSerializer(self.school_document).data
        self.data.update({
            'essential_questions': True
        })

    def test_admin_can_update_school_document(self):
        """
        Assert update success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('schooldocument-detail',
                                           args=[self.school_document.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_cannot_update_school_document(self):
        """
        Teacher cannot update school document
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('schooldocument-detail',
                                           args=[self.school_document.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class DeleteSchoolDocumentTest(APITestCase):
    """
    Delete SchoolDocument API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.school_document = SchoolDocument.objects.create(
            instruction='instruction here',
            content='content link here',
            learning_environment='class room management description',
            differentiation=True,
            accommodations=True,
            essential_questions=False,
            remediation=True,
            use_of_technology=True,
            vocabulary=True
        )

    def test_admin_can_delete_school_document(self):
        """
        Test admin can delete school document.
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('schooldocument-detail',
                                              args=[self.school_document.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_can_not_delete_school_document(self):
        """
        Teacher cannot delete school document
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('schooldocument-detail',
                                              args=[self.school_document.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class CreateStrategyGroupTest(APITestCase):
    """
    Create strategy group API test case.
    """

    def setUp(self):
        """
        Test case init data.
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.activity = Activity.objects.create(tittle='test')

        self.data = {
            'name': 'custom strategy',
            'activity': [self.activity.id]
        }

    def test_admin_can_create_strategy_group(self):
        """
        Test admin can create strategy group.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('strategygroup-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_can_not_create_strategy_group(self):
        """
        Test teacher can not create strategy group.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('strategygroup-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class ReadStrategyGroupTest(APITestCase):
    """
    Read strategy group API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.activity = Activity.objects.create(tittle='test')

        self.strategy_group = StrategyGroup.objects.create(
            name='custom strategy',
        )
        self.strategy_group.activity.set([self.activity])

    def test_admin_can_read_strategy_group_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('strategygroup-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_strategy_group_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('strategygroup-detail',
                                           args=[self.strategy_group.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_teacher_can_read_strategy_group_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('strategygroup-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_teacher_can_read_strategy_group_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('strategygroup-detail',
                                           args=[self.strategy_group.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class UpdateStrategyGroupTest(APITestCase):
    """
    Update strategy group API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.activity = Activity.objects.create(tittle='test')

        self.strategy_group = StrategyGroup.objects.create(
            name='custom strategy',
        )
        self.strategy_group.activity.set([self.activity])

        self.data = StrategyGroupSerializers(self.strategy_group).data
        self.data.update({
            'name': 'test strategy'
        })

    def test_admin_can_update_strategy_group(self):
        """
        Assert update success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('strategygroup-detail',
                                           args=[self.strategy_group.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_can_not_update_strategy_group(self):
        """
        Teacher can not update strategy group
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('strategygroup-detail',
                                           args=[self.strategy_group.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class DeleteStrategyGroupTest(APITestCase):
    """
    Delete strategy group API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.activity = Activity.objects.create(tittle='test')

        self.strategy_group = StrategyGroup.objects.create(
            name='custom strategy',
        )
        self.strategy_group.activity.set([self.activity])

    def test_admin_can_delete_strategy_group(self):
        """
        Test admin can delete strategy group.
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('strategygroup-detail',
                                              args=[self.strategy_group.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_can_not_delete_strategy_group(self):
        """
        Teacher can not delete strategy group
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('strategygroup-detail',
                                              args=[self.strategy_group.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class CreateObservationTypeTest(APITestCase):
    """
    Create observation type API test case.
    """

    def setUp(self):
        """
        Test case init data.
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.data = {
            'name': 'Formal'
        }

    def test_admin_can_create_observation_type(self):
        """
        Test admin can create observation_type.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('observationtype-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_can_not_create_observation_type(self):
        """
        Test teacher can not create observation_type.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('observationtype-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class ReadObservationTypeTest(APITestCase):
    """
    Read observation type API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.obs_type = ObservationType.objects.create(
            name='Formal',
        )

    def test_admin_can_read_observation_type_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('observationtype-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_observattion_type_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('observationtype-detail',
                                           args=[self.obs_type.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_teacher_can_read_observation_type_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('observationtype-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_teacher_can_read_observation_type_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('observationtype-detail',
                                           args=[self.obs_type.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class UpdateObservationTypeTest(APITestCase):
    """
    Update observation type API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.type = ObservationType.objects.create(
            name='Formal',
        )

        self.data = ObservationTypeSerializers(self.type).data
        self.data.update({
            'name': 'Informal'
        })

    def test_admin_can_update_type(self):
        """
        Assert update success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('observationtype-detail',
                                           args=[self.type.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_can_not_update_type(self):
        """
        Teacher can not update observation type
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('observationtype-detail',
                                           args=[self.type.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class DeleteObservationTypeTest(APITestCase):
    """
    Delete observation type API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.type = ObservationType.objects.create(
            name='Formal',
        )

    def test_admin_can_delete_type(self):
        """
        Test admin can delete observation type.
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('observationtype-detail',
                                              args=[self.type.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_can_delete_type(self):
        """
        Teacher can not delete observation type
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('observationtype-detail',
                                              args=[self.type.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class CreateObservationTest(APITestCase):
    """
    Create observation API test case.
    """

    def setUp(self):
        """
        Test case init data.
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.type = ObservationType.objects.create(name='Formal')

        self.lesson_plan = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2
        )

        self.data = {
            'type': self.type.id,
            'description': 'obsetion description',
            'save_for_later': False,
            'lesson_plan': self.lesson_plan.id,
            'created_by': self.user_1.id
        }

    def test_admin_can_create_observation(self):
        """
        Test admin can create observation.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('observation-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_can_create_observation(self):
        """
        Test teacher can create observation on his lesson.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('observation-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.client.force_authenticate(user=None)


class ReadObservationTest(APITestCase):
    """
    Read observation API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.type = ObservationType.objects.create(name='Formal')

        self.lesson_plan = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2
        )

        self.observation = Observation.objects.create(
            type=self.type, description='observation description',
            save_for_later=False, created_by=self.user_1,
            lesson_plan=self.lesson_plan
        )

    def test_admin_can_read_observation_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('observation-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_observation_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('observation-detail',
                                           args=[self.observation.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_teacher_can_read_observation_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('strategygroup-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_teacher_can_read_observation_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('observation-detail',
                                           args=[self.observation.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class UpdateObservationTest(APITestCase):
    """
    Update observation API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.type = ObservationType.objects.create(name='Formal')

        self.lesson_plan = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2
        )

        self.observation = Observation.objects.create(
            type=self.type, save_for_later=True, created_by=self.user_1,
            lesson_plan=self.lesson_plan
        )

        self.data = ObservationSerializers(self.observation).data
        self.data.update({
            'save_for_later': True
        })

    def test_admin_can_update_observation(self):
        """
        Assert update success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('observation-detail',
                                           args=[self.observation.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_can_not_update_observation(self):
        """
        Teacher can not update observation other observation
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('observation-detail',
                                           args=[self.observation.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class DeleteObservationTest(APITestCase):
    """
    Delete observation API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.type = ObservationType.objects.create(name='Formal')

        self.lesson_plan = LessonPlan.objects.create(
            tittle='english',
            objective='lesson objective',
            created_by=self.user_2
        )

        self.observation = Observation.objects.create(
            type=self.type, created_by=self.user_1, lesson_plan=self.lesson_plan
        )

    def test_admin_can_delete_observation(self):
        """
        Test admin can delete observation.
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('observation-detail',
                                              args=[self.observation.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_can_not_delete_observation(self):
        """
        Teacher can not delete observation
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('observation-detail',
                                              args=[self.observation.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class CreateSmartGuideTest(APITestCase):
    """
    Create smart_guide API test case.
    """

    def setUp(self):
        """
        Test case init data.
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.subject = Subject.objects.create(name='math')

        self.data = {
            'name': 'test',
            'subject': self.subject.id,
            'archive': False,
            'description': 'smart description'
        }

    def test_admin_can_create_smart_guide(self):
        """
        Test admin can create smart guide.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('smartguide-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_can_not_create_smart_guide(self):
        """
        Test teacher can not create smart guide.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('smartguide-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class ReadSmartGuideTest(APITestCase):
    """
    Read smart guide API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.school = School.objects.create(name='test')
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
            school=self.school
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
            school=self.school
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.subject = Subject.objects.create(name='math')

        self.smart_guide = SmartGuide.objects.create(
            name='test smart guide', description='smart guide description',
            archive=False, subject=self.subject, school=self.school
        )

    def test_admin_can_read_smart_guide_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('smartguide-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_smart_guide_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('smartguide-detail',
                                           args=[self.smart_guide.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_teacher_can_read_smart_guide_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('smartguide-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_teacher_can_read_smart_guide_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('smartguide-detail',
                                           args=[self.smart_guide.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class UpdateSmartGuideTest(APITestCase):
    """
    Update smart guide API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)
        self.school = School.objects.create(name='test')
        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
            school=self.school
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
            school=self.school
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.subject = Subject.objects.create(name='math')

        self.smart_guide = SmartGuide.objects.create(
            name='test', subject=self.subject, archive=False,
            description='smart guide description', school=self.school
        )

        self.data = SmartGuideSerializers(self.smart_guide).data
        self.data.update({
            'description': 'test test test'
        })

    def test_admin_can_update_smart_guide(self):
        """
        Assert update success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('smartguide-detail',
                                           args=[self.smart_guide.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_can_not_update_smart_guide(self):
        """
        Teacher can not update smart guide
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('smartguide-detail',
                                           args=[self.smart_guide.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class DeleteSmartGuideTest(APITestCase):
    """
    Delete smart guide API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        self.school = School.objects.create(name='test')
        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
            school=self.school
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
            school=self.school
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.subject = Subject.objects.create(name='math')

        self.smart_guide = SmartGuide.objects.create(
            name='test', subject=self.subject, archive=False,
            description='smart guide description', school=self.school
        )

    def test_admin_can_delete_smart_guide(self):
        """
        Test admin can delete smart guide.
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('smartguide-detail',
                                              args=[self.smart_guide.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_can_not_delete_smart_guide(self):
        """
        Teacher can not delete smart guide
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('smartguide-detail',
                                              args=[self.smart_guide.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class CreateEvaluationTypeTest(APITestCase):
    """
    Create evaluation_type API test case.
    """

    def setUp(self):
        """
        Test case init data.
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.data = {
            'name': 'test',
            'default_type': True,
        }

    def test_admin_can_create_evalution_type(self):
        """
        Test admin can create evalution_type.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('evaluationtype-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_not_create_evalution_type(self):
        """
        Test teacher can not create evalution_type.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('evaluationtype-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class ReadEvaluationTypeTest(APITestCase):
    """
    Read evaluattion type API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.evaluation_type = EvaluationType.objects.create(
            name='test', default_type=True
        )

    def test_admin_can_read_evaluation_type_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('evaluationtype-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_evaluation_type_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('evaluationtype-detail',
                                           args=[self.evaluation_type.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_teacher_can_read_evaluation_type_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('evaluationtype-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_teacher_can_read_evaluation_type_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('evaluationtype-detail',
                                           args=[self.evaluation_type.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class UpdateEvaluationTypeTest(APITestCase):
    """
    Update evaluation type API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.evaluation_type = EvaluationType.objects.create(
            name='test', default_type=True
        )
        self.data = EvaluationTypeSerializers(self.evaluation_type).data
        self.data.update({
            'name': 'test 1'
        })

    def test_admin_can_update_evaluation_type(self):
        """
        Assert update success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('evaluationtype-detail',
                                           args=[self.evaluation_type.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_can_not_update_evaluation_type(self):
        """
        Assert can not update
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('evaluationtype-detail',
                                           args=[self.evaluation_type.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class DeleteEvaluationTypeTest(APITestCase):
    """
    Delete evaluation type API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.evaluation_type = EvaluationType.objects.create(
            name='test', default_type=True,
        )

    def test_admin_can_delete_evaluationtype(self):
        """
        Test admin can delete evaluation type.
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('evaluationtype-detail',
                                              args=[self.evaluation_type.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_can_not_delete_evaluation_type(self):
        """
        Teacher can not delete evaluation type
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('evaluationtype-detail',
                                              args=[self.evaluation_type.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class CreateEvaluationChoicesTest(APITestCase):
    """
    Create evaluation_choice API test case.
    """

    def setUp(self):
        """
        Test case init data.
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.data = {
            'name': 'test',
        }

    def test_admin_can_create_evaluation_choices(self):
        """
        Test admin can create evalution_choice.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('evaluationchoices-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_can_not_create_evalution_choices(self):
        """
        Test teacher can not create evalution_choice.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('evaluationchoices-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class ReadEvaluationChoicesTest(APITestCase):
    """
    Read evaluation choices API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.evaluation_choices = EvaluationChoices.objects.create(name='test')

    def test_admin_can_read_evaluation_choices_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('evaluationchoices-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_evaluation_choices_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('evaluationchoices-detail',
                                           args=[self.evaluation_choices.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_teacher_can_read_evaluation_choices_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('evaluationchoices-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_teacher_can_read_evaluation_choices_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('evaluationchoices-detail',
                                           args=[self.evaluation_choices.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class UpdateEvaluationChoicesTest(APITestCase):
    """
    Update evaluation choices API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.evaluation_choices = EvaluationChoices.objects.create(name='test')
        self.data = EvaluationChoicesSerializers(self.evaluation_choices).data
        self.data.update({
            'name': 'choice test'
        })

    def test_admin_can_update_evaluation_choices(self):
        """
        Assert update success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('evaluationchoices-detail',
                                           args=[self.evaluation_choices.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_can_not_update_evaluation_choices(self):
        """
        Assert can not update
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('evaluationchoices-detail',
                                           args=[self.evaluation_choices.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class DeleteEvaluationChoicesTest(APITestCase):
    """
    Delete evaluation choices API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.evaluation_choices = EvaluationChoices.objects.create(name='test')

    def test_admin_can_delete_evaluation_choices(self):
        """
        Test admin can delete evaluation choices.
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('evaluationchoices-detail',
                                              args=[self.evaluation_choices.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_can_not_delete_evaluation_choices(self):
        """
        Teacher can not delete evaluation choices
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('evaluationchoices-detail',
                                              args=[self.evaluation_choices.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class CreateEvaluateLessonObservationTest(APITestCase):
    """
    Create evaluation_lesson_observation API test case.
    """

    def setUp(self):
        """
        Test case init data.
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.lesson_plan = LessonPlan.objects.create(
            tittle='test', created_by=self.user_2)
        obs = Observation.objects.create(lesson_plan=self.lesson_plan,
                                         created_by=self.user_1)

        eval_type = EvaluationType.objects.create(name='test')
        eval_choice = EvaluationChoices.objects.create(name='test')
        self.data = {
            'observation': obs.id,
            'evaluation_type': eval_type.id,
            'evaluation_choice': eval_choice.id
        }

    def test_admin_can_create_evaluation_lesson_observation(self):
        """
        Test admin can create evalution_lesson_observation.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse(
            'evaluatelessonobservation-list'), self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_can_create_evalution_lesson_observation(self):
        """
        Test teacher can create evalution_lesson_observation.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse(
            'evaluatelessonobservation-list'), self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.client.force_authenticate(user=None)


class ReadEvaluateLessonObservationTest(APITestCase):
    """
    Read evaluation lesson observation API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.lesson_plan = LessonPlan.objects.create(
            tittle='test', created_by=self.user_2)
        obs = Observation.objects.create(lesson_plan=self.lesson_plan,
                                         created_by=self.user_1)

        eval_type = EvaluationType.objects.create(name='test')
        eval_choice = EvaluationChoices.objects.create(name='test')

        self.eval_lesson_obs = EvaluateLessonObservation.objects.create(
            observation=obs, evaluation_type=eval_type,
            evaluation_choice=eval_choice)

    def test_admin_can_read_evaluation_lesson_observation_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse(
            'evaluatelessonobservation-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_evaluation_lesson_observation_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('evaluatelessonobservation-detail',
                                           args=[self.eval_lesson_obs.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_teacher_can_read_lesson_observation_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse(
            'evaluatelessonobservation-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_teacher_can_read_lesson_observation_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('evaluatelessonobservation-detail',
                                           args=[self.eval_lesson_obs.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class UpdateEvaluateLessonObservationTest(APITestCase):
    """
    Update evaluation lesson observation API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.lesson_plan = LessonPlan.objects.create(
            tittle='test', created_by=self.user_2)
        obs = Observation.objects.create(lesson_plan=self.lesson_plan,
                                         created_by=self.user_1)

        eval_type = EvaluationType.objects.create(name='test')
        eval_type1 = EvaluationType.objects.create(name='test1')
        eval_choice = EvaluationChoices.objects.create(name='test')

        self.eval_lesson_obs = EvaluateLessonObservation.objects.create(
            observation=obs, evaluation_type=eval_type,
            evaluation_choice=eval_choice)
        self.data = EvaluateLessonObservationSerializers(
            self.eval_lesson_obs).data
        self.data.update({
            'evaluation_type': eval_type1.id
        })

    def test_admin_can_update_evaluation_lesson_observation(self):
        """
        Assert update success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('evaluatelessonobservation-detail',
                                           args=[self.eval_lesson_obs.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_can_update_evaluation_lesson_observation(self):
        """
        Assert update success
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('evaluatelessonobservation-detail',
                                           args=[self.eval_lesson_obs.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class DeleteEvaluateLessonObservationTest(APITestCase):
    """
    Delete evaluation lesson observation API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.lesson_plan = LessonPlan.objects.create(
            tittle='test', created_by=self.user_2)
        obs = Observation.objects.create(lesson_plan=self.lesson_plan,
                                         created_by=self.user_1)

        eval_type = EvaluationType.objects.create(name='test')
        eval_choice = EvaluationChoices.objects.create(name='test')

        self.eval_lesson_obs = EvaluateLessonObservation.objects.create(
            observation=obs, evaluation_type=eval_type,
            evaluation_choice=eval_choice)

    def test_admin_can_delete_evaluation_lesson_observation(self):
        """
        Test admin can delete evaluation lesson observation.
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('evaluatelessonobservation-detail',
                                              args=[self.eval_lesson_obs.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_can_not_delete_evaluation_lesson_observation(self):
        """
        Teacher can not delete evaluation lesson observation.
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('evaluatelessonobservation-detail',
                                              args=[self.eval_lesson_obs.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class CreateEvaluationCriteriaGroupTest(APITestCase):
    """
    Create evaluation_type_criteria API test case.
    """

    def setUp(self):
        """
        Test case init data.
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        eval_type = EvaluationType.objects.create(name='test')
        self.data = {
            'name': 'test',
            'archive': True,
            'evaluation_type': [eval_type.id, ]
        }

    def test_admin_can_create_evaluation_criteria_group(self):
        """
        Test admin can create evaluation_criteria_grou[p].
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('evaluationcriteriagroup-list'),
                                    self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_can_create_evalution_criteria_group(self):
        """
        Test teacher can create evaluation_type_criteria.
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('evaluationcriteriagroup-list'),
                                    self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.client.force_authenticate(user=None)


class ReadEvaluationCriteriaGroupTest(APITestCase):
    """
    Read evaluation_criteria_group API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        eval_type = EvaluationType.objects.create(name='test')

        self.eval_criteria_grp = EvaluationCriteriaGroup.objects.create(
            name='test', archive=False)
        self.eval_criteria_grp.evaluation_type.add(eval_type)

        self.school = School.objects.create(name='test')
        self.school.evaluation_criteria_group.add(self.eval_criteria_grp)
        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
            school=self.school,
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
            school=self.school,
        )
        self.user_2.groups.add(Group.objects.get(name=grp))


    def test_admin_can_read_evaluation_criteria_group_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse(
            'evaluationcriteriagroup-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_evaluation_criteria_group_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('evaluationcriteriagroup-detail',
                                           args=[self.eval_criteria_grp.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_teacher_can_read_evaluation_criteria_group_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse(
            'evaluationcriteriagroup-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_teacher_can_read_evaluation_criteria_group_detail(self):
        """
        Assert object detail read success
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('evaluationcriteriagroup-detail',
                                           args=[self.eval_criteria_grp.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class UpdateEvaluationCriteriaGroupTest(APITestCase):
    """
    Update evaluation criteria group API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()

        eval_type = EvaluationType.objects.create(name='test')

        self.eval_criteria_grp = EvaluationCriteriaGroup.objects.create(
            name='test', archive=False)
        self.eval_criteria_grp.evaluation_type.add(eval_type.id)

        self.school = School.objects.create(name='test')
        self.school.evaluation_criteria_group.add(self.eval_criteria_grp)

        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
            school=self.school,
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
            school=self.school,
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.data = EvaluationCriteriaGroupSerializers(
            self.eval_criteria_grp).data

        self.data.update({
            'archive': True
        })

    def test_admin_can_update_evaluation_criteria_group(self):
        """
        Assert update success
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('evaluationcriteriagroup-detail',
                                           args=[self.eval_criteria_grp.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_can_update_evaluation_criteria_group(self):
        """
        Assert update success
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.put(reverse('evaluationcriteriagroup-detail',
                                           args=[self.eval_criteria_grp.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class DeleteEvaluateLessonObservationTest(APITestCase):
    """
    Delete evaluation lesson observation API test case
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        eval_type = EvaluationType.objects.create(name='test')

        self.eval_criteria_grp = EvaluationCriteriaGroup.objects.create(
            name='test', archive=False)
        self.eval_criteria_grp.evaluation_type.add(eval_type)

        self.school = School.objects.create(name='test')
        self.school.evaluation_criteria_group.add(self.eval_criteria_grp)
        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword123',
            email='mike@tyson.com',
            school=self.school,
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
            school=self.school,
        )
        self.user_2.groups.add(Group.objects.get(name=grp))


    def test_admin_can_delete_evaluation_criteria_group(self):
        """
        Test admin can delete evaluation criteria group.
        """
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('evaluationcriteriagroup-detail',
                                              args=[self.eval_criteria_grp.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_can_not_delete_evaluation_criteria_group(self):
        """
        Teacher can delete evaluation criteria group.
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(reverse('evaluationcriteriagroup-detail',
                                              args=[self.eval_criteria_grp.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)
