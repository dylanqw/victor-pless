from datetime import date, datetime
from os import environ
import sendgrid
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import m2m_changed
from django.dispatch import receiver
from django.utils.text import slugify
from djrichtextfield.models import RichTextField
from rest_framework import serializers
from sortedm2m.fields import SortedManyToManyField
from tara.settings.config_tara import main_conf as config


class School(models.Model):
    """
    This model will keep all schools details
    """
    name = models.CharField(max_length=150, unique=True)
    school_type = models.ForeignKey('SchoolType', null=True, blank=True,
                                    on_delete=models.SET_NULL)
    state = models.ForeignKey('State', null=True, blank=True,
                              on_delete=models.SET_NULL)
    district = models.CharField(max_length=300, blank=True, null=True)
    created_at = models.DateField(auto_now=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True,
                                   blank=True, on_delete=models.SET_NULL,
                                   related_name='school_user')
    template = SortedManyToManyField('Template', blank=True,
                                     related_name='template_school')
    strategy_group = SortedManyToManyField('StrategyGroup',
                                           blank=True)
    evaluation_criteria_group = SortedManyToManyField('EvaluationCriteriaGroup',
                                                      blank=True)

    def __str__(self):
        return self.name.title()

    def clean(self):
        self.name = self.name.lower()

    def save(self, *args, **kwargs):
        if self.pk:
            self.name = self.name.lower()
            super(School, self).save(*args, **kwargs)
        else:
            self.name = self.name.lower()
            if School.objects.filter(name=self.name):
                raise serializers.ValidationError(
                    {'error': 'School with same name already exists in our system',
                     'status': 500}
                )
            else:
                super(School, self).save(*args, **kwargs)


class Class(models.Model):
    """
    This model will keep all classes details
    """
    name = models.CharField(max_length=150)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, blank=True,
        on_delete=models.CASCADE
    )
    co_teacher = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                        related_name='co_teacher',
                                        null=True, blank=True
                                        )
    background = models.CharField(max_length=50, null=True)
    share_with_school = models.BooleanField(default=False)
    school = models.ManyToManyField(School, blank=True)
    shared_to_school = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        # from tara_user.models import update_users_last_updated_on
        color_list = ['#DE8F6E', '#C3D2D5', '#BDA0BC', '#7FF9F9', '#70A9C1',
                      '#8CADA7', '#DBCFB0', '#D888B2']
        if not self.pk:
            classes = Class.objects.filter(created_by=self.created_by)
            color_index = classes.count()
            if color_index <= 7:
                self.background = color_list[color_index]
            else:
                self.background = color_list[color_index % 8]
            super(Class, self).save(*args, **kwargs)
            if Class.objects.filter(created_by=self.created_by).count() == 1:
                first_class_creation_email(self.created_by, getDomain())
        else:
            super(Class, self).save(*args, **kwargs)


@receiver(m2m_changed, sender=Class.school.through)
def class_assign_to_school(sender, instance, **kwargs):
    from tara_user.models import User, create_class_and_lesson
    action = kwargs.pop('action', None)
    if action == "post_add":
        school_id = kwargs.pop('pk_set', None)
        for id in school_id:
            for user in User.objects.filter(school=School.objects.get(id=id)):
                create_class_and_lesson(user, instance)
        instance.shared_to_school = True
        instance.save()


def getDomain():
    if "TARA_APIS_ENVIRON" in environ.keys():
        if environ["TARA_APIS_ENVIRON"] == "DEV":
            domain = config.get('DEV_DOMAIN')
            return domain
        if environ["TARA_APIS_ENVIRON"] == "PROD":
            domain = config.get('LIVE_DOMAIN')
            return domain
        if environ["TARA_APIS_ENVIRON"] == "local":
            domain = config.get('LOCAL_DOMAIN')
            return domain
        if environ["TARA_APIS_ENVIRON"] == "QA":
            domain = config.get('QA_DOMAIN')
            return domain
        if environ["TARA_APIS_ENVIRON"] == "Staging":
            domain = config.get('STAGING_DOMAIN')
            return domain


def sendEmailToCoTeacher(classes, domain, co_teacher):
    """
    Method for sending email to the class co_teacher,
    after coteacher is being added to class.
    """
    tara_link = "https://{0}/login".format(domain)
    from_email = 'noreply@taraedtech.com'
    if not environ["TARA_APIS_ENVIRON"] == "PROD":
        sg = sendgrid.SendGridAPIClient(config.get('TARA_SENDGRID_API_KEY'))
        template_id = 'd-6a011eea48414254a163a6ad43562199'
    else:
        sg = sendgrid.SendGridAPIClient(config.get('LIVE_TARA_SENDGRID_API_KEY'))
        template_id = 'd-c86b20103bf14088b557f7f8ea66c0ba'
    data = {
        "personalizations": [
            {
                "to": [
                    {
                        "email": co_teacher.email,
                    }
                ],
                "dynamic_template_data": {
                    "lclass_name": classes.name,
                    "teacher_name": classes.created_by.first_name + ' ' +
                    classes.created_by.last_name,
                    "tara_link": tara_link,
                },
            }
        ],
        "from": {
            "email": from_email
        },
        "template_id": template_id
    }
    sg.client.mail.send.post(request_body=data)


def first_class_creation_email(user, domain):
    """
    Method for sending an email to the users who create class first time.
    """
    subject = 'Welcome to Tara Account'
    sg = sendgrid.SendGridAPIClient(config.get('LIVE_TARA_SENDGRID_API_KEY'))
    from_email = 'noreply@taraedtech.com'
    template_id = "d-df4343a6ec3c4f47a0cd3e1e1d9b9520"
    to_email = user.email
    tara_link = "https://{0}/login".format(domain)
    data = {
        "personalizations": [
            {
                "to": [
                    {
                        "email": to_email,
                    }
                ],
                "dynamic_template_data": {
                    "activation_link": tara_link,
                },
            }
        ],
        "from": {
            "email": from_email
        },
        "template_id": template_id
    }
    sg.client.mail.send.post(request_body=data)


class Standards(models.Model):
    """
    This model will keep all standards details
    """
    name = models.CharField(max_length=150)
    description = models.TextField(null=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)
    state = models.ForeignKey('State', on_delete=models.SET_NULL, null=True,
                              blank=True)

    def __str__(self):
        return self.name


class Container(models.Model):
    """
    This model will keep all container details
    """
    title = models.CharField(max_length=150, unique=True)

    def __str__(self):
        return self.title


class Subject(models.Model):
    """
    This model will keep all subjects details
    """
    name = models.CharField(max_length=150)

    def __str__(self):
        return self.name


class LearningIntelligence(models.Model):
    """
    This model will keep all learning intelligence details
    """
    name = models.CharField(max_length=150, unique=True)

    def __str__(self):
        return self.name


class Activity(models.Model):
    """
    This model will keep all activity related details
    """
    tittle = models.CharField(max_length=150)
    description = RichTextField(null=True, blank=True)

    subject = models.ForeignKey(
        Subject, null=True, on_delete=models.SET_NULL, blank=True
    )
    learning_intelligence = models.ManyToManyField(
        LearningIntelligence, blank=True
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL
    )
    default_activity = models.BooleanField(default=False)
    archive = models.BooleanField(default=False)

    def __str__(self):
        if self.subject:
            return "{0} - {1}".format(self.tittle, self.subject)
        else:
            return "{0}".format(self.tittle)

    class Meta:
        verbose_name_plural = "Activities"
        unique_together = (('tittle', 'subject'),)


class Template(models.Model):
    """
    This model will store all templates
    """
    name = models.CharField(max_length=150, unique=True)
    container = SortedManyToManyField(Container, blank=True)

    def __str__(self):
        return self.name


def no_past(value):
    if value < date.today():
        raise ValidationError('Date cannot be in the past.')


class Calender(models.Model):
    """
    This model will store calender dates
    """
    date = models.DateField(validators=[no_past])
    lesson = models.ForeignKey('LessonPlan', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.date)

    class Meta:
        unique_together = ('date', 'lesson')


class LessonPlan(models.Model):
    """
    This model will keep all lesson plans
    """
    tittle = models.CharField(max_length=150)
    slug = models.SlugField(unique=True, null=True, blank=True, max_length=200)
    date = models.DateTimeField(null=True, blank=True)
    standard = models.ManyToManyField(
        Standards, related_name='lesson_standard', blank=True
    )
    elements = models.ManyToManyField(
        'TagElement', related_name='standard_elements', blank=True
    )
    objective = RichTextField(blank=True, null=True)
    mark_as_complete = models.BooleanField(default=False)
    template = models.ForeignKey(
        Template, null=True, blank=True, on_delete=models.SET_NULL
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    lesson_class = models.ForeignKey(
        Class, null=True, on_delete=models.CASCADE
    )
    last_updated = models.DateTimeField(auto_now=True)
    last_updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, blank=True,
        related_name='last_updated_by', on_delete=models.SET_NULL
    )
    differentiation = RichTextField(blank=True, null=True)
    accommodations = RichTextField(blank=True, null=True)
    essential_questions = RichTextField(blank=True, null=True)
    remediation = RichTextField(blank=True, null=True)  # before assessment now remediation
    use_of_technology = RichTextField(blank=True, null=True)  # before key_vocabulary now use_of_technology
    vocabulary = RichTextField(blank=True, null=True)  # before extension now vocabulary
    supplementary_materials = RichTextField(blank=True, null=True)
    procedures = RichTextField(blank=True, null=True)
    connections_to_prior_knowledge_and_learning = RichTextField(
        blank=True, null=True)
    conceptual_understanding_goal = RichTextField(blank=True, null=True)
    key_points = RichTextField(blank=True, null=True)
    assessment = RichTextField(blank=True, null=True)  # new assesment field
    student_exemplar_response = RichTextField(blank=True, null=True)
    back_pocket_questions = RichTextField(blank=True, null=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(self.tittle)
            if LessonPlan.objects.filter(slug=self.slug):
                self.slug = slugify(self.tittle + '_by_' + '' +
                                    self.created_by.first_name + '_' +
                                    self.created_by.last_name
                                    if self.created_by.first_name
                                    else self.tittle + '_by_' + '' +
                                    self.created_by.username)
                if LessonPlan.objects.filter(slug=self.slug):
                    self.slug = slugify(self.tittle + '_by_' + '' +
                                        self.created_by.username)

                if LessonPlan.objects.filter(slug=self.slug):
                    raise serializers.ValidationError(
                        {
                            'error': 'Lesson with same name already exists in'
                                     ' our system, try creating your lesson'
                                     ' with different name!',
                            'status': 500
                        }
                    )
            user = self.created_by
            user.last_updated_on = datetime.now()
            user.save()
            super(LessonPlan, self).save(*args, **kwargs)
            if LessonPlan.objects.filter(
                    created_by=self.created_by).count() == 1:
                lesson_create_first_time_email(self.created_by)
        else:
            if self.tittle != LessonPlan.objects.filter(slug=self.slug)[0].tittle:
                self.slug = slugify(self.tittle)
                if LessonPlan.objects.filter(slug=self.slug):
                    self.slug = slugify(self.tittle + '_by_' + '' +
                                        self.created_by.first_name + '_' +
                                        self.created_by.last_name
                                        if self.created_by.first_name
                                        else self.created_by.username)
                    if LessonPlan.objects.filter(slug=self.slug):
                        self.slug = slugify(self.tittle + '_by_' + '' +
                                            self.created_by.username)

                    if LessonPlan.objects.filter(slug=self.slug):
                        raise serializers.ValidationError(
                            {
                                'error': 'Lesson with same name already exists'
                                         ' in our system, try creating your'
                                         ' lesson with different name!',
                                'status': 500
                            }
                        )
                user = self.created_by
                user.last_updated_on = datetime.now()
                user.save()
            super(LessonPlan, self).save(*args, **kwargs)

    def __str__(self):
        return '{0}'.format(self.tittle)


def lesson_create_first_time_email(user):
    """
    Method for sending email when user create their first lesson.
    """
    sg = sendgrid.SendGridAPIClient(config.get('LIVE_TARA_SENDGRID_API_KEY'))
    from_email = 'noreply@taraedtech.com'
    template_id = "d-c095e305e1a4499a9bf064bd2c0049c2"
    to_email = user.email
    data = {
        "personalizations": [
            {
                "to": [
                    {
                        "email": to_email,
                    }
                ],
                "dynamic_template_data": {},
            }
        ],
        "from": {
            "email": from_email
        },
        "template_id": template_id
    }
    sg.client.mail.send.post(request_body=data)


class RateLesson(models.Model):
    """
    This model will keep lesson executed ratings
    """
    ENGAGEMENT_CHOICES = (
        (0.5, '0.5'), (1.0, '1.0'),
        (1.5, '1.5'), (2.0, '2.0'),
        (2.5, '2.5'), (3.0, '3.0'),
        (3.5, '3.5'), (4.0, '4.0'),
        (4.5, '4.5'), (5.0, '5.0'),
    )
    engagement = models.FloatField(choices=ENGAGEMENT_CHOICES, null=True)

    EFFECTIVENESS_CHOICES = (
        (0.5, '0.5'), (1.0, '1.0'),
        (1.5, '1.5'), (2.0, '2.0'),
        (2.5, '2.5'), (3.0, '3.0'),
        (3.5, '3.5'), (4.0, '4.0'),
        (4.5, '4.5'), (5.0, '5.0'),
    )
    effectiveness = models.FloatField(choices=EFFECTIVENESS_CHOICES, null=True)

    notes = models.TextField(null=True)
    lesson_plan = models.ForeignKey(LessonPlan, on_delete=models.CASCADE)
    rated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    created_on = models.DateTimeField(auto_now=True)


class TagElement(models.Model):
    """
    This model will keep tag element
    """
    element = models.TextField(null=True)
    standards = models.ForeignKey(Standards, on_delete=models.CASCADE,
                                  null=True, blank=True)

    def __str__(self):
        return '{0}'.format(self.element)


class Attachment(models.Model):
    """
    This model will keep all attachments
    """
    attachment = models.FileField(upload_to='uploads/', null=True, blank=True)


class LessonActivity(models.Model):
    """
    This model will keep lessonAcivity
    """
    lesson_plan = models.ForeignKey(LessonPlan, on_delete=models.CASCADE)
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE)

    DURATION_CHOICES = (
        (5, '5 Minute'), (10, '10 Minute'), (15, '15 Minute'),
        (20, '20 Minute'), (25, '25 Minute'), (30, '30 Minute'),
        (35, '35 Minute'), (40, '40 Minute'), (45, '45 Minute'),
        (50, '50 Minute'), (55, '55 Minute'), (60, '60 Minute')
    )
    set_duration = models.PositiveIntegerField(choices=DURATION_CHOICES,
                                               blank=True, null=True)

    GROUPING_CHOICES = (
        ('independent', 'Independent'),
        ('small_groups', 'Small Groups'),
        ('whole_class', 'Whole Class'),
        ('partners', 'Partners')
    )
    set_grouping = models.CharField(max_length=15, choices=GROUPING_CHOICES,
                                    blank=True, null=True)

    sequence = models.IntegerField(null=True)
    attachment = models.ManyToManyField(Attachment, null=True, blank=True)
    archive = models.BooleanField(default=False)
    description = RichTextField(blank=True, null=True)
    container = models.ForeignKey(
        Container, null=True, blank=True, on_delete=models.SET_NULL
    )
    custom_set_duration = models.BooleanField(default=False)
    activity_guide = models.BooleanField(default=False)

    def __str__(self):
        return '<{0} for {1} >'.format(self.activity, self.lesson_plan,)

    def save(self, *args, **kwargs):
        lesson_plan = LessonPlan.objects.get(id=self.lesson_plan.id)
        super(LessonActivity, self).save(*args, **kwargs)
        lesson_plan.save()


class ActivityContainer(models.Model):
    """
    This model will keep activity container
    """
    template = models.ForeignKey(
        Template, null=True, blank=True, on_delete=models.SET_NULL
    )
    container = models.ForeignKey(
        Container, null=True, blank=True, on_delete=models.SET_NULL
    )
    lesson_activity = models.ForeignKey(
        LessonActivity, null=True, blank=True, on_delete=models.SET_NULL
    )


class ActivityComment(models.Model):
    """
    This model will keep activity comment
    """
    comment = models.CharField(max_length=5000)
    commented_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    created_on = models.DateTimeField(auto_now=True)
    lesson_activity = models.ForeignKey(
        LessonActivity, on_delete=models.CASCADE
    )
    reviewed = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        """
        Overriding the save method to send notification email to lesson creator
        """
        if not self.pk:
            if "TARA_APIS_ENVIRON" in environ.keys():
                if environ["TARA_APIS_ENVIRON"] == "DEV":
                    domain = config.get('DEV_DOMAIN')
                if environ["TARA_APIS_ENVIRON"] == "PROD":
                    domain = config.get('LIVE_DOMAIN')
                if environ["TARA_APIS_ENVIRON"] == "local":
                    domain = config.get('LOCAL_DOMAIN')
                if environ["TARA_APIS_ENVIRON"] == "QA":
                    domain = config.get('QA_DOMAIN')
                if environ["TARA_APIS_ENVIRON"] == "Staging":
                    domain = config.get('STAGING_DOMAIN')
            super(ActivityComment, self).save(*args, **kwargs)
            if environ["TARA_APIS_ENVIRON"] not in ["JENKINS", ]:
                if not self.commented_by == self.lesson_activity.lesson_plan.\
                        created_by:
                    send_notification_email(self, domain)
        else:
            super(ActivityComment, self).save(*args, **kwargs)


def send_notification_email(comment, domain):
    """
    Method for sending notification email to the lesson creator,
    after comment is created.
    """
    tara_link = "https://{0}/login".format(domain)
    from_email = 'noreply@taraedtech.com'
    sg = sendgrid.SendGridAPIClient(config.get('LIVE_TARA_SENDGRID_API_KEY'))
    template_id = 'd-eff8399a055d40acb9f6f77bb5daebb0'
    if comment.commented_by.first_name:
        teacher_name = comment.commented_by.first_name + ' ' +\
            comment.commented_by.last_name
    else:
        teacher_name = comment.commented_by.email

    data = {
        "personalizations": [
            {
                "to": [
                    {
                        "email": comment.lesson_activity.lesson_plan.
                        created_by.email,
                    }
                ],
                "dynamic_template_data": {
                    "lesson_name": comment.lesson_activity.lesson_plan.
                    tittle,
                    "teacher_name": teacher_name,
                    "tara_link": tara_link,
                },
            }
        ],
        "from": {
            "email": from_email
        },
        "template_id": template_id
    }
    sg.client.mail.send.post(request_body=data)


class ActivityTemplate(models.Model):
    """
    This model will keep activity template
    """
    name = models.CharField(max_length=250)
    activity = models.ForeignKey(
        Activity, on_delete=models.CASCADE
    )
    description = RichTextField(blank=True, null=True)

    def __str__(self):
        return self.name


class SchoolType(models.Model):
    """
    This model will keep school type
    """
    name = models.CharField(max_length=300)

    def __str__(self):
        return self.name


class State(models.Model):
    """
    This model will keep state
    """
    name = models.CharField(max_length=300)

    def __str__(self):
        return self.name


class SchoolDocument(models.Model):
    """
    This model will keep all school documents.
    """
    instruction = RichTextField(blank=True, null=True)
    content = RichTextField(blank=True, null=True)
    learning_environment = RichTextField(blank=True, null=True)  # before Classroom Management
    differentiation = models.BooleanField(default=False)
    accommodations = models.BooleanField(default=False)
    essential_questions = models.BooleanField(default=False)
    remediation = models.BooleanField(default=False)
    use_of_technology = models.BooleanField(default=False)
    vocabulary = models.BooleanField(default=False)
    school = models.OneToOneField(School, on_delete=models.CASCADE, null=True)
    supplementary_materials = models.BooleanField(default=False)
    procedures = models.BooleanField(default=False)
    connections_to_prior_knowledge_and_learning = models.BooleanField(
        default=False)
    conceptual_understanding_goal = models.BooleanField(default=False)
    key_points = models.BooleanField(default=False)
    assessment = models.BooleanField(default=False)
    student_exemplar_response = models.BooleanField(default=False)
    back_pocket_questions = models.BooleanField(default=False)

    def __str__(self):
        return "{0}".format(self.school.name if self.school else '')


class StrategyGroup(models.Model):
    """
    This model will keep all strategy groups
    """
    name = models.CharField(max_length=250, unique=True)
    activity = models.ManyToManyField(Activity, blank=True)

    def __str__(self):
        return self.name


class ObservationType(models.Model):
    """
    This model will keep all types.
    """
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class Observation(models.Model):
    """
    This model will keep all observation.
    """
    type = models.ForeignKey(ObservationType, null=True, blank=True, on_delete=models.SET_NULL)
    DURATION_CHOICES = (
        (10, '10 minutes'), (15, '15 minutes'), (20, '20 minutes'),
        (25, '25 minutes'), (30, '30 minutes'),
    )
    duration = models.PositiveIntegerField(choices=DURATION_CHOICES,
                                           blank=True, null=True)
    lesson_plan = models.ForeignKey(LessonPlan, null=True,
                                    on_delete=models.CASCADE)
    description = RichTextField(blank=True, null=True)
    save_for_later = models.BooleanField(default=False)
    smart_guide = models.BooleanField(default=False)
    archive = models.BooleanField(default=False)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL,
                                   on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=True)
    read = models.BooleanField(default=False)

    def __str__(self):
        return 'observation on {} by {}'.format(self.lesson_plan.tittle,
                                                self.created_by.username)

    def save(self, *args, **kwargs):
        if self.created_by == self.lesson_plan.created_by:
            self.read = True
        super().save(*args, **kwargs)


def lesson_observation_email(lesson, domain):
    """
    Method for sending email when coach submit obseration on teachers lesson.
    """
    sg = sendgrid.SendGridAPIClient(config.get('LIVE_TARA_SENDGRID_API_KEY'))
    from_email = 'noreply@taraedtech.com'
    # template for lesson creator
    template_id = "d-f1e79b342994488a818bbc8bd095829a"
    to_email = lesson.created_by.email

    tara_link = "https://{0}/login".format(domain)
    data = {
        "personalizations": [
            {
                "to": [
                    {
                        "email": to_email,
                    }
                ],
                "dynamic_template_data": {
                    "lesson_name": lesson.tittle,
                    # "username": lesson.created_by.email,
                    "tara_link": tara_link,
                },
            }
        ],
        "from": {
            "email": from_email
        },
        "template_id": template_id
    }
    sg.client.mail.send.post(request_body=data)


class SmartGuide(models.Model):
    """
    This model will keep smart guide
    """
    name = models.CharField(max_length=250, null=True)
    subject = models.OneToOneField(
        Subject, on_delete=models.CASCADE, null=True, blank=True
    )
    description = RichTextField(blank=True, null=True)
    archive = models.BooleanField(default=False)
    school = models.OneToOneField(School, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name


class EvaluationType(models.Model):
    """
    This model will keep evaluation type.
    """
    name = models.CharField(max_length=150)
    default_type = models.BooleanField(default=False)
    school = models.ForeignKey(School, on_delete=models.CASCADE, null=True,
                               blank=True)

    def __str__(self):
        return self.name


class EvaluationChoices(models.Model):
    """
    This model will keep evaluation choices.
    """
    name = models.CharField(max_length=150)

    def __str__(self):
        return self.name


class EvaluateLessonObservation(models.Model):
    """
    This model will keep evaluation lesson observation.
    """
    observation = models.ForeignKey(Observation, on_delete=models.CASCADE)
    evaluation_type = models.ForeignKey(EvaluationType,
                                        on_delete=models.CASCADE)
    evaluation_choice = models.ForeignKey(EvaluationChoices,
                                          on_delete=models.CASCADE)
    last_evaluated = models.DateField(auto_now=True)


class EvaluationCriteriaGroup(models.Model):
    """
    This model will keep evaluation criteria group.
    """
    name = models.CharField(max_length=250)
    evaluation_type = SortedManyToManyField(EvaluationType)
    archive = models.BooleanField(default=False)

    def __str__(self):
        return self.name
