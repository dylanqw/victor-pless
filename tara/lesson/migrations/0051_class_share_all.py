# Generated by Django 2.2.1 on 2020-03-06 12:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lesson', '0050_auto_20200304_0658'),
    ]

    operations = [
        migrations.AddField(
            model_name='class',
            name='share_all',
            field=models.BooleanField(default=False),
        ),
    ]
