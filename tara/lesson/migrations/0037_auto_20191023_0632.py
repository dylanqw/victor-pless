# Generated by Django 2.2.1 on 2019-10-23 06:32

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import sortedm2m.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('lesson', '0036_state'),
    ]

    operations = [
        migrations.AddField(
            model_name='school',
            name='created_at',
            field=models.DateField(auto_now=True),
        ),
        migrations.AddField(
            model_name='school',
            name='created_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='school_user', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='school',
            name='district',
            field=models.CharField(blank=True, max_length=300, null=True),
        ),
        migrations.AddField(
            model_name='school',
            name='school_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='lesson.SchoolType'),
        ),
        migrations.AddField(
            model_name='school',
            name='state',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='lesson.State'),
        ),
        migrations.AddField(
            model_name='school',
            name='template',
            field=sortedm2m.fields.SortedManyToManyField(blank=True, help_text=None, related_name='template_school', to='lesson.Template'),
        ),
    ]
