# Generated by Django 2.2.1 on 2019-07-09 06:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lesson', '0011_auto_20190703_1233'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lessonplan',
            name='activity',
        ),
    ]
