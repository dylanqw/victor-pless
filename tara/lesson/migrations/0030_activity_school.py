# Generated by Django 2.2.1 on 2019-09-16 11:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('lesson', '0029_standards_created_by'),
    ]

    operations = [
        migrations.AddField(
            model_name='activity',
            name='school',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='lesson.School'),
        ),
    ]
