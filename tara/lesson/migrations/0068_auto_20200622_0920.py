# Generated by Django 2.2.1 on 2020-06-22 14:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('lesson', '0067_auto_20200622_0715'),
    ]

    operations = [
        migrations.AddField(
            model_name='smartguide',
            name='school',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='lesson.School'),
        ),
        migrations.AlterField(
            model_name='smartguide',
            name='subject',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='lesson.Subject'),
        ),
    ]
