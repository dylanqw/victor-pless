import sendgrid
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import gettext_lazy as _

from tara.settings.config_tara import main_conf as config

UserModel = get_user_model()


def send_password_reset_email(context):
    """
        Method for sending password reset email to the users
    """

    # Making sure that dynamically respective domain would be choosen
    app_site = context['domain']

    # Generating the registration link to attach in email
    password_link = "{0}://{1}/reset/password/{2}/{3}".format(
        context['protocol'], app_site, context['uid'], context['token'])

    to_email = context['email']

    from_email = 'noreply@taraedtech.com'

    sg = sendgrid.SendGridAPIClient(config.get('TARA_SENDGRID_API_KEY'))

    data = {
        "from": {
            "email": from_email,
        },
        "personalizations": [
            {
                "to": [
                    {
                        "email": to_email,
                    }
                ],
                "dynamic_template_data": {
                    "password_link": password_link,
                },
            }
        ],
        "template_id": "d-b533bebf6b3d4e5a825756c94a1a7236"
    }
    resp = sg.client.mail.send.post(request_body=data)
    print(resp)


class CustomPasswordResetForm(forms.Form):
    email = forms.EmailField(label=_("Email"), max_length=254)
    domain = forms.CharField(label=_("Domain"), max_length=254)

    def get_users(self, email):
        """
            Given an email, return matching user(s) who should receive a reset.
            This allows subclasses to more easily customize the default
            policies that prevent inactive users and users with unusable
            passwords from resetting their password.
        """
        active_users = UserModel._default_manager.filter(**{
            '%s__iexact' % UserModel.get_email_field_name(): email,
            'is_active': True,
        })
        return (u for u in active_users if u.has_usable_password())

    def save(self, domain_override=None,
             subject_template_name='registration/password_reset_subject.txt',
             email_template_name='registration/password_reset_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None, html_email_template_name=None,
             extra_email_context=None):
        """
            Generate a one-use only link for resetting password and send it
            to the user.
        """
        email = self.cleaned_data["email"]
        domain = self.cleaned_data["domain"]
        for user in self.get_users(email):
            context = {
                'email': email,
                'domain': domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'user': user,
                'token': token_generator.make_token(user),
                'protocol': 'https' if use_https else 'http',
            }
            send_password_reset_email(context)
