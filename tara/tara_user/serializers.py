from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db.models import Q, Value
from django.db.models.functions import Concat
from rest_auth.models import TokenModel
from rest_auth.serializers import PasswordResetSerializer, TokenSerializer
from rest_framework import serializers

from billing.models import BillingSubscription, SchoolSubscription
from lesson.models import (Class, LessonPlan, Observation, School, SchoolType,
                           State)
from tara_user.forms import CustomPasswordResetForm
from tara_user.models import MyTeacher

User = get_user_model()


class GetUserSerializer(serializers.ModelSerializer):
    """
    Get User model serializers
    """
    class_count = serializers.SerializerMethodField(
        'getClass'
    )

    completed_lesson_count = serializers.SerializerMethodField(
        'getLessonCount'
    )
    school = serializers.SerializerMethodField(
        'getSchool'
    )
    school_subscription = serializers.SerializerMethodField()
    stripe_subscription_status = serializers.SerializerMethodField()
    is_my_teacher = serializers.SerializerMethodField()
    latest_observation = serializers.SerializerMethodField()

    def getLessonCount(self, user):
        return LessonPlan.objects.filter(created_by=user,
                                         mark_as_complete=True).count()

    def getClass(self, user):
        return Class.objects.filter(created_by=user).count()

    def getSchool(self, user):
        from lesson.serializers import SchoolSerializer
        if user.school:
            schools = School.objects.filter(name=user.school.name)
            return [SchoolSerializer(school).data for school in schools][0]
        else:
            pass

    def get_school_subscription(self, user):
        return SchoolSubscription.objects.filter(
            school=user.school
        ).values('end_date', 'active')

    def get_stripe_subscription_status(self, user):
        return BillingSubscription.objects.filter(
            billing_account__created_by=user
        ).values('subscription_id', 'status')

    def get_is_my_teacher(self, obj):
        user = self.context['request'].user
        my_teachers = MyTeacher.objects.filter(user=user)
        if my_teachers:
            for my_teacher in my_teachers[0].my_teacher.all():
                if my_teacher == obj:
                    return True
        return False

    def get_latest_observation(self, user):
        """Get latest observation
        """
        obs = Observation.objects.filter(lesson_plan__created_by=user,
                                         save_for_later=False).last()
        if obs:
            lesson_obs = {
                'observation_id': obs.id,
                'save_for_later': obs.save_for_later,
                'observation_on': obs.date,
                'observation_by': obs.created_by.last_name
                if obs.created_by.last_name else obs.created_by.username,
                'user_type': obs.created_by.groups.all().order_by('name')[0].name
                if obs.created_by.groups.all() else 'principal',
                'read': obs.read,
            }
            return lesson_obs

    class Meta:
        """
        Serializing all fields
        """
        model = User
        # excluding fields which are not required
        exclude = ('user_permissions', 'password')
        depth = 2


class PostUserSerializer(serializers.ModelSerializer):
    """
    Post User model serializers
    """
    class Meta:
        """
        Serializing all fields
        """
        model = User
        # excluding fields which are not required
        exclude = ('user_permissions', 'date_joined', 'last_updated_on',)


class Userserializer(serializers.ModelSerializer):
    """
    Optimise User model serializers
    """
    class Meta:
        """
        Serializing all fields
        """
        model = User
        # excluding fields which are not required
        exclude = ('user_permissions', 'date_joined', 'password', 'last_login',
                   'password_set', 'is_staff', 'is_active', 'trial_expiry_date',
                   'state', 'groups', 'is_superuser')


class GroupSerializer(serializers.ModelSerializer):
    """
    Group model serializer
    """
    class Meta:
        """
        Serializing all fields
        """
        model = Group
        # excluding fields which are not required
        exclude = ('permissions',)


class CustomPasswordResetSerializer(PasswordResetSerializer,
                                    serializers.Serializer):
    """
    Serializer for requesting a password reset e-mail.
    """
    email = serializers.EmailField()
    domain = serializers.CharField()

    password_reset_form_class = CustomPasswordResetForm

    def get_email_options(self):
        """Override this method to change default e-mail options"""
        return {}

    def validate_email(self, value):
        # Create PasswordResetForm with the serializer
        self.reset_form = self.password_reset_form_class(
            data=self.initial_data
        )
        if not self.reset_form.is_valid():
            raise serializers.ValidationError(self.reset_form.errors)

        return value

    def save(self):
        request = self.context.get('request')
        # Set some values to trigger the send_email method.
        opts = {
            'use_https': request.is_secure(),
            'from_email': getattr(settings, 'DEFAULT_FROM_EMAIL'),
            'request': request,
        }

        opts.update(self.get_email_options())
        self.reset_form.save(**opts)


class CustomTokenSerializer(TokenSerializer):
    """
    Custom Token serializers
      - We have customized rest-auth TokenSerializer to
        get token key, user, group and password_set value.
    """
    user_group = serializers.SerializerMethodField()
    password_set = serializers.SerializerMethodField()
    trial_expiry_date = serializers.SerializerMethodField()
    school_subscription = serializers.SerializerMethodField()
    stripe_subscription_status = serializers.SerializerMethodField()

    def get_user_group(self, obj):
        if self.instance.user.groups.all():
            return self.instance.user.groups.values_list('name', flat=True).\
                order_by('name')[0]

    def get_password_set(self, obj):
        return self.instance.user.password_set

    def get_trial_expiry_date(self, obj):
        return self.instance.user.trial_expiry_date

    def get_school_subscription(self, obj):
        return SchoolSubscription.objects.filter(
            school=self.instance.user.school
        ).values('end_date', 'active')

    def get_stripe_subscription_status(self, obj):
        return BillingSubscription.objects.filter(
            billing_account__created_by=self.instance.user
        ).values('subscription_id', 'status')

    class Meta:
        model = TokenModel
        fields = ('key', 'user', 'user_group', 'password_set',
                  'trial_expiry_date', 'school_subscription',
                  'stripe_subscription_status')


class GetMyTeacherSerializer(serializers.ModelSerializer):
    """
    Get my_teacher serializer
    """
    my_teacher = serializers.SerializerMethodField()

    def get_my_teacher(self, users):
        last_name = self.context['request'].query_params.get('ordering')
        search_param = self.context['request'].query_params.get('search')
        my_teacher_list = []
        my_teacher = users.my_teacher.all().order_by('first_name')
        if search_param:
            my_teacher = my_teacher.annotate(name=Concat(
                'first_name', Value(' '),
                'last_name')
            ).filter(Q(username__icontains=search_param) | Q(
                name__icontains=search_param))
        if last_name:
            my_teacher = my_teacher.order_by(last_name)
        for user in my_teacher:
            obs = Observation.objects.filter(lesson_plan__created_by=user,
                                             save_for_later=False).last()
            if obs:
                lesson_obs = {
                    'observation_id': obs.id,
                    'save_for_later': obs.save_for_later,
                    'observation_on': obs.date,
                    'observation_by': obs.created_by.last_name
                    if obs.created_by.last_name else obs.created_by.username,
                    'user_type': obs.created_by.groups.all().order_by('name')[0].name
                    if obs.created_by.groups.all() else 'principal',
                    'read': obs.read,
                }
            my_teacher_obj = {
                'id': user.id,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'email': user.email,
                'class_created_count': Class.objects.filter(
                    created_by=user).count(),
                'completed_lesson_count': LessonPlan.objects.filter(
                    created_by=user, mark_as_complete=True).count(),
                'last_updated': user.last_updated_on,
                'latest_observation': lesson_obs if obs else None,
            }
            my_teacher_list.append(my_teacher_obj)
        return my_teacher_list

    class Meta:
        model = MyTeacher
        fields = '__all__'


class PostMyTeacherSerializer(serializers.ModelSerializer):
    """
    Post my_teacher serializer
    """
    class Meta:
        model = MyTeacher
        fields = '__all__'
