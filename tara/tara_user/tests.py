from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from lesson.models import School
from tara_user.models import MyTeacher, LearningCommunity, Cluster
from tara_user.serializers import (GroupSerializer, PostMyTeacherSerializer,
                                   PostUserSerializer)

User = get_user_model()

client = APIClient()


class CreateUserTest(APITestCase):
    """
    Create User API test cases
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            email='mike@tyson.com',
            password='mikepassword'
        )
        grp = Group(name='administrator')
        grp.save()
        self.user_1.groups.add(Group.objects.get(name=grp))

        self.user_2 = User.objects.create(
            username='test@gmail.com',
            email='test@gmail.com',
            password='testpassword'
        )
        grp = Group(name='teacher')
        grp.save()
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.group = Group(name='teacher')
        self.school = School.objects.create(name='New Bosten')
        test_cluster = Cluster.objects.create(name='mycluster')
        self.plc =  LearningCommunity.objects.create(name='myplc',cluster=test_cluster,school=self.school)
        self.data = {
            'username': 'test123@gmail.com',
            'first_name': 'test',
            'last_name': 'test',
            'email': 'test123@gmail.com',
            'password': 'testpassword',
            'group': self.group.id,
            'school': self.school.id,
            'my_school': 'test school',
            'plc' : self.plc.id
        }

    def test_admin_can_create_user(self):
        """
        Administrator can create user
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)

        response = self.client.post(reverse('user-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.client.force_authenticate(user=None)

    def test_user_cannot_create_user(self):
        """
        Teacher can not create user
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)

        response = self.client.post(reverse('user-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ReadUserTest(APITestCase):
    """
    Read User API test cases
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

    def test_admin_can_read_user_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)

        response = self.client.get(reverse('user-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_user_detail(self):
        """
        Assert object detail read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('user-detail', args=[self.user_1.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_cannot_read_user_list(self):
        """teacher can not read user list
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)

        response = self.client.get('user-list')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_user_can_read_user_detail(self):
        """
        teacher can read his own details
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('user-detail', args=[self.user_2.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UpdateUserTest(APITestCase):
    """
    Update User API test cases
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(username='mike@tyson.com',
                                          password='mikepassword',
                                          email='mike@tyson.com')
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(username='test@gmail.com',
                                          password='testpassword',
                                          email='test@gmail.com')
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.data = PostUserSerializer(self.user_2).data
        self.data.update({
            'first_name': 'test',
        })

    def test_admin_can_update_user(self):
        """
        Admin can update user
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)

        response = self.client.put(reverse('user-detail', args=[self.user_2.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_can_update_himself(self):
        """Teacher can update himself
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)

        response = self.client.put(reverse('user-detail', args=[self.user_2.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_cannot_update_other_user(self):
        """Teacher can not update other user
        """
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        self.data = PostUserSerializer(self.user_1).data
        self.data.update({
            'first_name': 'jhon',
        })
        response = self.client.put(reverse('user-detail', args=[self.user_1.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_authenticate(user=None)


class DeleteUserTest(APITestCase):
    """
    Delete User API test cases.
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user = User.objects.create(username='mike',
                                        password='mikepassword',
                                        email='mike@tyson.com')
        self.user.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_1 = User.objects.create(username='test@gmail.com',
                                          password='testpassword',
                                          email='test@gmail.com')
        self.user_1.groups.add(Group.objects.get(name=grp))

    def test_admin_can_delete_user(self):
        """Administative user can delete user
        """
        self.client.force_authenticate(user=self.user)

        response = self.client.delete(reverse('user-detail', args=[self.user_1.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_cannot_delete_user(self):
        """Teacher can not delete user
        """
        self.client.force_authenticate(user=self.user_1)

        response = self.client.delete(reverse('user-detail', args=[self.user_1.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class CreateGroupTest(APITestCase):
    """
    Create Group API test cases
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.data = {
            'name': 'department_couch',
        }

    def test_admin_can_create_group(self):
        """
        Assert object creation success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=self.user_1)
        response = self.client.post(reverse('group-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.client.force_authenticate(user=None)

    def test_user_cannot_create_group(self):
        """
        Teacher can not create group
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.post(reverse('group-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ReadGroupTest(APITestCase):
    """
    Read Group API test cases
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.group = Group.objects.create(name='department_coach')

    def test_admin_can_read_group_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('group-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_group_detail(self):
        """
        Assert object detail read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('group-detail', args=[self.group.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_can_read_group_list(self):
        """
        Teacher can read group list
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('group-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_read_group_detail(self):
        """
        teacher can read group details
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('group-detail', args=[self.group.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class UpdateGroupTest(APITestCase):
    """
    Update Group API test cases
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.group = Group.objects.create(name='department_coach')
        self.data = GroupSerializer(self.group).data
        self.data.update({
            'name': 'student',
        })

    def test_admin_can_update_group(self):
        """
        Admin can update group
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)

        response = self.client.put(reverse('group-detail',
                                           args=[self.group.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_cannot_update_group(self):
        """
        Teacher can not update group
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)

        response = self.client.put(reverse('group-detail',
                                           args=[self.group.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class DeleteGroupTest(APITestCase):
    """
    Delete Group API test cases
    """

    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.group = Group.objects.create(name='department_coach')

    def test_admin_can_delete_group(self):
        """
        Assert delete success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(
            reverse('group-detail', args=[self.group.id]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_cannot_delete_group(self):
        """
        Teacher can not delete group
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.delete(
            reverse('group-detail', args=[self.group.id]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.client.force_authenticate(user=None)


class CreateMyTeacherTest(APITestCase):
    """
    Create MyTeacher API test cases
    """
    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            email='mike@tyson.com',
            password='mikepassword'
        )
        grp = Group(name='administrator')
        grp.save()
        self.user_1.groups.add(Group.objects.get(name=grp))

        self.user_2 = User.objects.create(
            username='test@gmail.com',
            email='test@gmail.com',
            password='testpassword'
        )
        grp = Group(name='teacher')
        grp.save()
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.data = {
            'user': self.user_1.id,
            'my_teacher': [self.user_2.id]
        }

    def test_admin_can_create_my_teacher(self):
        """
        Administrator can create my_teacher list
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)

        response = self.client.post(reverse('myteacher-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.client.force_authenticate(user=None)

    def test_user_can_create_my_teacher(self):
        """
        Teacher can create my_teacher list
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)

        response = self.client.post(reverse('myteacher-list'), self.data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class ReadMyTeacherTest(APITestCase):
    """
    Read MyTeacher API test cases
    """
    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(
            username='mike@tyson.com',
            password='mikepassword',
            email='mike@tyson.com',
        )
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(
            username='test@gmail.com',
            password='testpassword',
            email='test@gmail.com',
        )
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.my_teacher = MyTeacher.objects.create(
            user=self.user_1,
        )
        self.my_teacher.my_teacher.set([self.user_2])

    def test_admin_can_read_my_teacher_list(self):
        """
        Assert object read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)

        response = self.client.get(reverse('myteacher-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_can_read_my_teacher_detail(self):
        """
        Assert object detail read success
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('myteacher-detail',
                                           args=[self.my_teacher.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_not_read_my_teacher_list(self):
        """teacher can read my_teacherer list
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)

        response = self.client.get(reverse('myteacher-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can__read_user_detail(self):
        """
        teacher can read my_teacher details
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)
        response = self.client.get(reverse('myteacher-detail',
                                           args=[self.my_teacher.id]),
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UpdateMyTeacherTest(APITestCase):
    """
    Update MyTeacher API test cases
    """
    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user_1 = User.objects.create(username='mike@tyson.com',
                                          password='mikepassword',
                                          email='mike@tyson.com')
        self.user_1.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_2 = User.objects.create(username='test@gmail.com',
                                          password='testpassword',
                                          email='test@gmail.com')
        self.user_2.groups.add(Group.objects.get(name=grp))

        self.my_teacher = MyTeacher.objects.create(
            user=self.user_1,
        )

        self.data = PostMyTeacherSerializer(self.my_teacher).data
        self.data.update({
            'my_teacher': [self.user_2.id],
        })

    def test_admin_can_update_my_teacher(self):
        """
        Admin can update my_teacher
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_1.username)
        self.client.force_authenticate(user=user)

        response = self.client.put(reverse('myteacher-detail',
                                           args=[self.my_teacher.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)

    def test_user_can_update_my_teacher(self):
        """Teacher can update my_teacher
        """
        self.client.force_authenticate(user=None)
        user = User.objects.get(username=self.user_2.username)
        self.client.force_authenticate(user=user)

        response = self.client.put(reverse('myteacher-detail',
                                           args=[self.my_teacher.id]),
                                   self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(user=None)


class DeleteMyTestTest(APITestCase):
    """
    Delete MyTest API test cases.
    """
    def setUp(self):
        """
        Test case init data
        """
        self.superuser = User.objects.create_superuser('john', 'john@snow.com',
                                                       'johnpassword')
        user = User.objects.get(username='john')
        self.client.force_authenticate(user=user)

        grp = Group(name='administrator')
        grp.save()
        self.user = User.objects.create(username='mike',
                                        password='mikepassword',
                                        email='mike@tyson.com')
        self.user.groups.add(Group.objects.get(name=grp))

        grp = Group(name='teacher')
        grp.save()
        self.user_1 = User.objects.create(username='test@gmail.com',
                                          password='testpassword',
                                          email='test@gmail.com')
        self.user_1.groups.add(Group.objects.get(name=grp))

        self.my_teacher = MyTeacher.objects.create(
            user=self.user,
        )
        self.my_teacher.my_teacher.set([self.user_1])

    def test_admin_can_delete_my_teacher(self):
        """Administative user can delete my_teacher
        """
        self.client.force_authenticate(user=self.user)

        response = self.client.delete(reverse('myteacher-detail',
                                              args=[self.my_teacher.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)

    def test_user_can_delete_my_teacher(self):
        """Teacher can delete my_teacher
        """
        self.client.force_authenticate(user=self.user_1)

        response = self.client.delete(reverse('myteacher-detail',
                                              args=[self.my_teacher.id]),
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.force_authenticate(user=None)
