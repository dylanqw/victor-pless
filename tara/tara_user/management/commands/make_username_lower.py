from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model

User = get_user_model()


class Command(BaseCommand):
    """
    Management utility to make all username in lowercase.
    """
    help = 'Used to make all username in lowercase.'

    def handle(self, *args, **options):
        user_name = User.objects.all()
        for user in user_name:
            user.username = user.username.lower()
            user.save()

        self.stdout.write(
            self.style.SUCCESS(
                "all username are in lower.")
        )
