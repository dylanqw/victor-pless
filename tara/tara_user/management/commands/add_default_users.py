"""
Management utility to add default users for the TARA.
"""
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand

from tara.settings.config_tara import main_conf as config

User = get_user_model()


class Command(BaseCommand):
    """
    Management utility to add new user.
    """
    help = 'Used to add default users for the app.'

    def handle(self, *args, **options):
        super_user = User.objects.filter(
            username=config.get('SUPER_USER_NAME')
        )
        if not super_user:
            super_user = User.objects.create_superuser(
                username=config.get('SUPER_USER_NAME'),
                email=config.get('SUPER_USER_EMAIL'),
                password=config.get('SUPER_USER_PWD'),
            )
            self.stdout.write(
                self.style.SUCCESS(
                    "{} superuser added successfully.".format(
                        super_user.username
                    )
                )
            )
        else:
            self.stdout.write(
                self.style.ERROR(
                    "{} superuser already exists.".format(
                        super_user[0].username)
                )
            )

        user_list = config.get('DEFAULT_USER')
        for users in user_list:
            user = User.objects.filter(
                username=users['email']
            )
            if not user:
                user, created = User.objects.get_or_create(
                    first_name=users['first_name'],
                    last_name=users['last_name'],
                    username=users['email'],
                    email=users['email'],
                    password=users['password'],
                )
                if created:
                    user.groups.add(Group.objects.get(name=users['group']))
                    self.stdout.write(
                        self.style.SUCCESS(
                            "{} user added successfully.".format(user.username))
                    )
            else:
                self.stdout.write(
                    self.style.ERROR(
                        "{} user already exists.".format(user[0].username))
                )
