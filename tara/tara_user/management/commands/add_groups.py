"""
Management utility to add groups that will help system administrator to assign
permissions group wise .
"""
from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group


class Command(BaseCommand):
    """
    Management utility to add groups.
    """
    help = 'Used to add groups.'

    def handle(self, *args, **options):

        group_list = [
            'administrator', 'school_leader', 'department_coach', 'teacher',
            'cluster_administrator'
        ]
        for group_name in group_list:
            group, created = Group.objects.get_or_create(
                name=group_name,
            )
            if created:
                self.stdout.write(
                    self.style.SUCCESS(
                        "{} Group added successfully.".format(group_name))
                )
            else:
                self.stdout.write(
                    self.style.ERROR(
                        "{} Group already exists.".format(group.name))
                )
