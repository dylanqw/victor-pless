from datetime import datetime
from os import environ
import sendgrid
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.tokens import default_token_generator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from lesson.models import (Calender, Class, Container, LessonActivity,
                           LessonPlan, School, State, Template)
from tara.settings.config_tara import main_conf as config


class User(AbstractUser):
    """
    This model behaves identically to the auth user model, we are extending
    auth user model to add extra fields. In future we'll be able to customize
    it if the further need arises.
    """
    id_number = models.CharField(null=True, blank=True, max_length=200)
    email = models.EmailField(blank=False, null=False, unique=True)
    school = models.ForeignKey(School, on_delete=models.SET_NULL, null=True,
                               blank=True)
    password_set = models.BooleanField(default=False)
    trial_expiry_date = models.DateField(null=True, blank=False)
    my_school = models.CharField(null=True, blank=True, max_length=200)
    state = models.ForeignKey(State, on_delete=models.SET_NULL, null=True)
    last_updated_on = models.DateTimeField(null=True, blank=True)
    plc = models.ForeignKey('LearningCommunity', on_delete=models.SET_NULL,
                            null=True, blank=True)

    def save(self, *args, **kwargs):
        """
        Overriding the save method to send verification token email on user
        registration
        """
        if not self.pk:
            if "TARA_APIS_ENVIRON" in environ.keys():
                if environ["TARA_APIS_ENVIRON"] == "DEV":
                    domain = config.get('DEV_DOMAIN')
                if environ["TARA_APIS_ENVIRON"] == "PROD":
                    domain = config.get('LIVE_DOMAIN')
                if environ["TARA_APIS_ENVIRON"] == "local":
                    domain = config.get('LOCAL_DOMAIN')
                if environ["TARA_APIS_ENVIRON"] == "QA":
                    domain = config.get('QA_DOMAIN')
                if environ["TARA_APIS_ENVIRON"] == "Staging":
                    domain = config.get('STAGING_DOMAIN')

            if not self.password or self.password == 'invitation':
                self.password = User.objects.make_random_password()
                raw_password = self.password
                self.set_password(self.password)
            else:
                raw_password = self._password or self.password
                if not self._password:
                    self.set_password(self.password)
                # print(raw_password)
                # self.set_password(self.password)
            self.is_active = False
            self.username = self.username.lower()
            self.email = self.username
            super(User, self).save(*args, **kwargs)
            if environ["TARA_APIS_ENVIRON"] not in ["JENKINS", "local"]:
                if not self.is_superuser:
                    send_verification_email(self, domain, raw_password)
                    send_confirmation_email(self)
        else:
            self.username = self.username.lower()
            cls = self.__class__
            old = cls.objects.get(pk=self.pk)
            if self.school != old.school:
                queryset = Class.school.through.objects.filter(
                    school=self.school)
                for classes in queryset:
                    shared_class = Class.objects.get(id=classes.class_id)
                    user_class = Class.objects.filter(name=shared_class.name,
                                                      created_by=self)
                    if not user_class:
                        create_class_and_lesson(self, shared_class)
            super(User, self).save(*args, **kwargs)


def send_verification_email(user, domain, pw):
    """
    Method for sending invitation email token to the newly registered users
    where in order to invite users with verified emails.
    """
    subject = 'Welcome to Tara Account'

    # Generating uid and encoding to disallow forgery access
    uid = urlsafe_base64_encode(force_bytes(user.pk))

    # Generating the activation link to attach in email
    activation_link = \
        "https://{0}/user/account/activate/uid={1}/token={2}".format(
            domain, uid, default_token_generator.make_token(user), )
    to_email = user.email
    user_name = user.username
    last_name = user.last_name

    from_email = 'noreply@taraedtech.com'

    if not environ["TARA_APIS_ENVIRON"] == "PROD":
        sg = sendgrid.SendGridAPIClient(config.get('TARA_SENDGRID_API_KEY'))
        template_id = "d-f904110e27194c6cbe672669cb510805"
    else:
        sg = sendgrid.SendGridAPIClient(
            config.get('LIVE_TARA_SENDGRID_API_KEY')
        )
        template_id = "d-df4343a6ec3c4f47a0cd3e1e1d9b9520"

    if last_name:
        data = {
            "personalizations": [
                {
                    "to": [
                        {
                            "email": to_email,
                        }
                    ],
                    "dynamic_template_data": {
                        "email": to_email,
                        "last_name": last_name,
                        "password": pw,
                        "activation_link": activation_link,
                    },
                }
            ],
            "from": {
                "email": from_email
            },
            "template_id": template_id
        }
    else:
        data = {
            "personalizations": [
                {
                    "to": [
                        {
                            "email": user_name,
                        }
                    ],
                    "dynamic_template_data": {
                        "email": user_name,
                        "last_name": user_name,
                        "password": pw,
                        "activation_link": activation_link,
                    },
                }
            ],
            "from": {
                "email": from_email
            },
            "template_id": template_id
        }
    sg.client.mail.send.post(request_body=data)


def send_confirmation_email(user):
    """
    Method for sending email to support@taraedtech.com for confirming newly
    created user.
    """
    subject = 'New User Register on TARA'

    from_email = 'noreply@taraedtech.com'

    if not environ["TARA_APIS_ENVIRON"] == "PROD":
        to_email = 'sunny@trinesis.com'
        sg = sendgrid.SendGridAPIClient(config.get('TARA_SENDGRID_API_KEY'))
    else:
        to_email = 'support@taraedtech.com'
        sg = sendgrid.SendGridAPIClient(
            config.get('LIVE_TARA_SENDGRID_API_KEY')
        )

    data = {
        "personalizations": [
            {
                "to": [
                    {
                        "email": to_email
                    }
                ],
                "subject": subject
            }
        ],
        "from": {
            "email": from_email
        },
        "content": [
            {
                "type": "text/html",

                "value": "Hey Admin,<br>"
                         "<p>A new user is registered with email"
                         " - {0}</p>"
                         "<br>Thanks,<br>"
                         "TARA Team"
                         "</p>".format(user.email)
            }
        ]
    }
    sg.client.mail.send.post(request_body=data)


@receiver(post_save, sender=User)
def user_creation_signal(sender, instance, created, **kwargs):
    logged_in = kwargs.pop('update_fields', None)
    if logged_in:
        pass
    else:
        if created:
            queryset = Class.school.through.objects.filter(
                school=instance.school
            )
            for classes in queryset:
                shared_class = Class.objects.get(id=classes.class_id)
                create_class_and_lesson(instance, shared_class)


def create_class_and_lesson(user, shared_class):
    """
    This function will create class and lessons for users of a schools.
    """
    lesson_class = Class.objects.create(name=shared_class.name,
                                        created_by=user)

    lessons = LessonPlan.objects.filter(
        lesson_class=shared_class).order_by('id')
    for lesson in lessons:
        lesson_activities = LessonActivity.objects.filter(lesson_plan=lesson)
        calendars = Calender.objects.filter(lesson=lesson)
        lesson_plan = LessonPlan.objects.create(
            tittle=lesson.tittle,
            objective=lesson.objective,
            differentiation=lesson.differentiation,
            accommodations=lesson.accommodations,
            essential_questions=lesson.essential_questions,
            remediation=lesson.remediation,
            use_of_technology=lesson.use_of_technology,
            vocabulary=lesson.vocabulary,
            supplementary_materials=lesson.supplementary_materials,
            procedures=lesson.procedures,
            connections_to_prior_knowledge_and_learning=lesson.
            connections_to_prior_knowledge_and_learning,
            conceptual_understanding_goal=lesson.conceptual_understanding_goal,
            key_points=lesson.key_points,
            assessment=lesson.assessment,
            student_exemplar_response=lesson.student_exemplar_response,
            back_pocket_questions=lesson.back_pocket_questions,
            template=Template.objects.get(id=lesson.template.id)
            if lesson.template else None, created_by=user,
            lesson_class=lesson_class)

        for standard in lesson.standard.all():
            lesson_plan.standard.add(standard)

        for element in lesson.elements.all():
            lesson_plan.elements.add(element)

        for calendar in calendars:
            Calender.objects.create(date=calendar.date, lesson=lesson_plan)

        for lesson_activity in lesson_activities:
            lesson_act = LessonActivity.objects.create(
                lesson_plan=lesson_plan, activity=lesson_activity.activity,
                set_duration=lesson_activity.set_duration,
                set_grouping=lesson_activity.set_grouping,
                sequence=lesson_activity.sequence,
                archive=lesson_activity.archive,
                description=lesson_activity.description,
                container=Container.objects.get(id=lesson_activity.container.id),
                custom_set_duration=lesson_activity.custom_set_duration,
                activity_guide=lesson_activity.activity_guide)

            for attachment in lesson_activity.attachment.all():
                lesson_act.attachment.add(attachment)


# def update_users_last_updated_on():
#     """
#     This method will update login user last_updated_on field
#     """
#     import inspect
#     for frame_record in inspect.stack():
#         if frame_record[3] == 'get_response':
#             request = frame_record[0].f_locals['request']
#             request.user.last_updated_on = datetime.now()
#             request.user.save()
#             break


class MyTeacher(models.Model):
    """
    This model will keep list of favorite teachers. 
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    my_teacher = models.ManyToManyField(User, blank=True,
                                        related_name='my_teacher')


class ClusterAdminSchool(models.Model):
    """
    This models will keep cluster admin school.
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    school = models.ManyToManyField(School, blank=True)

    def __str__(self):
        return self.user.username


class Cluster(models.Model):
    """
    This model will keep cluster details.
    """
    name = models.CharField(max_length=150)

    def __str__(self):
        return self.name


class LearningCommunity(models.Model):
    """
    This model will keep learning community details.
    """
    name = models.CharField(max_length=150)
    school = models.ForeignKey(School, on_delete=models.CASCADE)
    cluster = models.ForeignKey(Cluster, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
