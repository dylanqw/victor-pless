from django.contrib.auth import get_user_model
from rest_framework.permissions import BasePermission

User = get_user_model()


class UserPermission(BasePermission):
    """
    Custom Permissions for User
        - Administrator has access (CRUD) to every records..
        - School Leader can view and update only their own user record.
        - Department Coach can view and update only their own user record.
        - Teachers can view and update only their own user record.
        - staff user has access (CRUD) to every records
    """

    def has_permission(self, request, view):

        user_group = User.objects.filter(
            username=request.user).values_list('groups__name', flat=True).\
            order_by('groups__name')

        if 'administrator' in user_group or request.user.is_staff:
            return True

        elif ('school_leader' in user_group or 'department_coach' in user_group
              or 'teacher' in user_group) and request.method in [
                  'POST', 'DELETE']:
            return False
        else:
            return True

    def has_object_permission(self, request, view, obj):

        if request.user.is_authenticated:
            user_group = User.objects.filter(
                username=request.user
            ).values_list('groups__name', flat=True)

            if 'administrator' in user_group or request.user.is_staff:
                return True
            elif 'department_coach' in user_group or 'cluster_administrator'\
                    in user_group or 'school_leader' in user_group or\
                    'teacher' in user_group:
                if request.method == 'GET':
                    return True
                elif request.method in ['PUT', 'PATCH']:
                    return request.user == obj
            else:
                return False
        else:
            return False


class MyTeacherPermission(BasePermission):
    """Custom permission for my teacher
        - Administrator has access (CRUD) to every records..
        - School Leader can has access (CRUD) to every records
        - Department Coach has access (CRUD) to every records.
        - Teachers has no permission.
        - staff user has access (CRUD) to every records
    """

    def has_permission(self, request, view):

        if request.user.is_authenticated:
            return True
        else:
            return False
