import codecs
import csv

from django import forms
from django.contrib import admin, messages
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import GroupAdmin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group
from django.contrib.sites.models import Site
from django.db.models import Case, Count, IntegerField, Max, Sum, When
from django.db.models.functions import Cast
from django.shortcuts import redirect, render
from django.urls import path
from django.utils.safestring import mark_safe
from django_admin_listfilter_dropdown.filters import RelatedDropdownFilter
from rest_framework.authtoken.models import Token

from lesson.models import Class, LessonPlan, School, State
from tara_user.models import (Cluster, ClusterAdminSchool, LearningCommunity,
                              MyTeacher)

User = get_user_model()


# Customizing Default User Admin
class UserAdmin(BaseUserAdmin):

    change_list_template = "lesson/standard_changelist.html"

    list_display = ['email', 'last_name', 'role', 'last_lesson_mark_completed',
                    'classes_Created', 'lesson_Created',
                    'lesson_mark_completed', 'last_login', 'action_link', ]

    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (('Personal Info'), {'fields': ('first_name', 'last_name', 'email',
                                        'school', 'password_set', 'my_school',
                                        'state','plc','id_number')}),
        (('Permissions & Group'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                              'groups')}),
        (('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    list_filter = (('groups', RelatedDropdownFilter),
                   ('school', RelatedDropdownFilter),)

    actions = ["user_activate"]

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        qs = qs.annotate(
            _class_count=Count('class', distinct=True),
            _lesson_count=Count('lessonplan', distinct=True),
            _lesson_complete_count=Case(When(_class_count=0, then=0),
                                        default=Sum(Cast('lessonplan__mark_as_complete',
                                                         IntegerField()))/Count('class', distinct=True),),
            _last_lesson_mark_completed=Max('lessonplan__last_updated')
        )
        return qs

    def role(self, user):
        if user.groups.all():
            return str(user.groups.all().order_by('name')[0]).title().\
                replace("_", " ")
        else:
            return ""

    def last_lesson_mark_completed(self, obj):
        return obj._last_lesson_mark_completed

    def classes_Created(self, obj):
        return obj._class_count

    def lesson_Created(self, obj):
        return obj._lesson_count

    def lesson_mark_completed(self, obj):
        return obj._lesson_complete_count

    def action_link(self, obj):
        app_name = obj._meta.app_label
        url_name = self.model.__name__.lower()
        user_id = obj.id
        drop_down = """<html>
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
               <style>
                .w3-text-blue, .w3-hover-text-blue:hover{{
                   color: #79AEC8!important;
                }}strong {{font-weight: bold;}}body {{line-height: 1.2;}}
                h1 {{
                    margin: 0 0 20px;
                    font-weight: 300;
                    font-size: 20px;
                    color: #666;
                    font-family: "Roboto","Lucida Grande","DejaVu Sans","Bitstream Vera Sans",Verdana,Arial,sans-serif;
                }}
                </style>
                <body>
                <div class="w3-container">
                <div class="w3-dropdown-hover">
                    <button class="w3-button w3-border w3-text-blue w3-large w3-white" >More ...</button>
                    <div class="w3-dropdown-content w3-bar-block w3-border">
                    <a style=color:black href="/admin/{0}/{1}/{2}/password" class="w3-bar-item w3-button">Reset Password</a>
                    <a style=color:black href="/admin/{0}/{1}/{2}/delete" class="w3-bar-item w3-button">Delete User</a>
                    </div>
                </div></div></body></html>
                """.format(app_name,
                           url_name,
                           user_id)
        return mark_safe(drop_down)

    action_link.allow_tags = True
    action_link.short_description = ""

    role.admin_order_field = 'groups__name'
    classes_Created.admin_order_field = '_class_count'
    lesson_Created.admin_order_field = '_lesson_count'
    lesson_mark_completed.admin_order_field = '_lesson_complete_count'
    last_lesson_mark_completed.admin_order_field = '_last_lesson_mark_completed'

    def user_activate(self, request, queryset):
        for user in queryset:
            user.is_active = True
            user.save()
        messages.add_message(
            request, messages.INFO, "Selected User's account activated!"
        )
    user_activate.short_description = 'Activate Selected'

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('import-csv/', self.import_csv),
        ]
        return my_urls + urls

    def import_csv(self, request):
        if request.method == "POST":
            csv_file = request.FILES["csv_file_for_user"]

            reader = csv.reader(csv_file)
            try:
                reader = csv.DictReader(codecs.iterdecode(csv_file, 'utf-8'))
                if reader.fieldnames == ['first_name', 'last_name', 'email',
                                         'role', 'school', 'state', 'coach',
                                         'plc', 'id_number', 'cluster',
                                         'password']:
                    for row in reader:
                        user = User.objects.filter(
                            username=list(row.items())[2][1].lower()
                        )
                        if user:
                            if list(row.items())[4][1] != '':
                                school, created = School.objects.get_or_create(
                                    name=list(row.items())[4][1].lower(),
                                )
                            else:
                                school = None
                            if list(row.items())[5][1] != '':
                                state, created = State.objects.get_or_create(
                                    name=list(row.items())[5][1]
                                )
                            else:
                                state = None
                            if list(row.items())[9][1] != '':
                                cluster, created =\
                                    Cluster.objects.get_or_create(
                                        name=list(row.items())[9][1]
                                    )
                                if list(row.items())[7][1] != '':
                                    plc, created =\
                                        LearningCommunity.objects.get_or_create(
                                            name=list(row.items())[7][1],
                                            school=school,
                                            cluster=cluster
                                        )
                                else:
                                    plc = None
                            else:
                                cluster = None
                                plc = None
                            user[0].school = school
                            user[0].state = state
                            user[0].plc = plc
                            user[0].id_number = list(row.items())[8][1]\
                                if list(row.items())[8][1] != '' else None
                            if list(row.items())[3][1] != '':
                                user[0].groups.clear()
                                user[0].groups.add(Group.objects.get(
                                    name=list(
                                        row.items())[3][1].lower())
                                )
                            user[0].save()
                            if list(row.items())[6][1] != '':
                                coach = User.objects.filter(username=list(
                                    row.items())[6][1].lower())
                                if not coach:
                                    coach1, created = \
                                        User.objects.get_or_create(
                                            username=list(row.items())[6][1].lower(),
                                            email=list(row.items())[6][1].lower(),
                                            school=school, state=state
                                        )
                                    if created:
                                        coach1.groups.add(Group.objects.get(
                                            name='department_coach'))
                                        teacher = MyTeacher.objects.create(
                                            user=coach1)
                                        teacher.my_teacher.add(user[0])
                                if coach:
                                    my_teacher = MyTeacher.objects.filter(
                                        user=coach[0])
                                    if not my_teacher:
                                        teacher = MyTeacher.objects.create(
                                            user=coach[0])
                                        teacher.my_teacher.add(user[0])
                                    else:
                                        my_teacher[0].my_teacher.add(user[0])
                            messages.success(
                                request,
                                'User - {0} updated successfully!'.format(
                                    list(row.items())[2][1]
                                )
                            )
                        else:
                            if list(row.items())[4][1] != '':
                                school, created = School.objects.get_or_create(
                                    name=list(row.items())[4][1].lower(),
                                )
                            else:
                                school = None
                            if list(row.items())[5][1] != '':
                                state, created = State.objects.get_or_create(
                                    name=list(row.items())[5][1]
                                )
                            else:
                                state = None
                            if list(row.items())[9][1] != '':
                                cluster, created =\
                                    Cluster.objects.get_or_create(
                                        name=list(row.items())[9][1]
                                    )
                                if list(row.items())[7][1] != '':
                                    plc, created =\
                                        LearningCommunity.objects.get_or_create(
                                            name=list(row.items())[7][1],
                                            school=school,
                                            cluster=cluster
                                        )
                                else:
                                    plc = None
                            else:
                                cluster = None
                                plc = None

                            if list(row.items())[2][1] != '':
                                user, created = User.objects.get_or_create(
                                    first_name=list(row.items())[0][1],
                                    last_name=list(row.items())[1][1],
                                    username=list(row.items())[2][1].lower(),
                                    password=list(row.items())[10][1]
                                    if list(row.items())[10][1] != '' else None,
                                    email=list(row.items())[2][1].lower(),
                                    is_active=False,
                                    school=school,
                                    state=state,
                                    plc=plc,
                                    id_number=list(row.items())[8][1]
                                    if list(row.items())[8][1] != '' else None,
                                )
                                if created:
                                    if list(row.items())[3][1] != '':
                                        user.groups.add(Group.objects.get(
                                            name=list(
                                                row.items())[3][1].lower())
                                        )
                                if list(row.items())[6][1] != '':
                                    coach = User.objects.filter(username=list(
                                        row.items())[6][1].lower())
                                    if not coach:
                                        coach1, created =\
                                            User.objects.get_or_create(
                                                username=list(row.items())[6][1].lower(),
                                                email=list(row.items())[6][1].lower(),
                                                school=school, state=state
                                            )
                                        if created:
                                            coach1.groups.add(Group.objects.get(
                                                name='department_coach'))
                                            teacher = MyTeacher.objects.create(
                                                user=coach1)
                                            teacher.my_teacher.add(user)
                                    if coach:
                                        my_teacher = MyTeacher.objects.filter(
                                            user=coach[0])
                                        if not my_teacher:
                                            teacher = MyTeacher.objects.create(
                                                user=coach[0])
                                            teacher.my_teacher.add(user)
                                        else:
                                            my_teacher[0].my_teacher.add(user)
                            messages.success(
                                request,
                                'User - {0} added successfully!'.format(
                                    list(row.items())[2][1]
                                )
                            )

                    self.message_user(
                        request,
                        "Your csv file has been imported"
                    )
                    return redirect("..")
                else:
                    messages.error(
                        request,
                        "Error occurred while importing"
                    )

            except Exception as e:
                messages.error(
                    request,
                    "Your csv file is not in correct format"
                )

        form = CsvImportForm()
        payload = {"form": form}
        return render(
            request, "admin/user_csv_form.html", payload
        )


class CsvImportForm(forms.Form):
    csv_file_for_user = forms.FileField()


# Customizing Default Group Admin
class CustomGroupAdmin(GroupAdmin):
    fieldsets = (
        (None, {'fields': ('name',)}),
    )


admin.site.register(User, UserAdmin)
admin.site.unregister(Group)
admin.site.register(Group, CustomGroupAdmin)
admin.site.register(ClusterAdminSchool)

admin.site.unregister(Site)
admin.site.unregister(Token)
admin.site.register(Cluster)
admin.site.register(LearningCommunity)
