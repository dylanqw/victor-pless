import datetime
from collections import OrderedDict

import sendgrid
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.contrib.auth.tokens import default_token_generator
from django.db.models import Q, Value
from django.db.models.functions import Concat
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.utils.translation import gettext as _
from django_filters.rest_framework import DjangoFilterBackend
from rest_auth.views import LoginView, PasswordChangeView, PasswordResetView
from rest_framework import filters, status, viewsets
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from lesson.models import getDomain
from lesson.permissions import OnlyReadPermission
from tara.settings.config_tara import main_conf as config
from tara_user.models import ClusterAdminSchool, MyTeacher
from tara_user.permissions import MyTeacherPermission, UserPermission
from tara_user.serializers import (CustomPasswordResetSerializer,
                                   CustomTokenSerializer,
                                   GetMyTeacherSerializer, GetUserSerializer,
                                   GroupSerializer, PostMyTeacherSerializer,
                                   PostUserSerializer)

User = get_user_model()


class UserPagination(LimitOffsetPagination):
    default_limit = 15

    def get_paginated_response(self, data):
        if not self.request.user.is_anonymous:
            my_teacher = MyTeacher.objects.filter(user=self.request.user)
            if my_teacher:
                my_teacher_id = my_teacher[0].id
                my_teacher_list = [
                    uid.id for uid in my_teacher[0].my_teacher.all()]
            else:
                my_teacher_id = None
                my_teacher_list = []
            return Response(OrderedDict([
                ('count', self.count),
                ('next', self.get_next_link()),
                ('previous', self.get_previous_link()),
                ('my_teacher_id', my_teacher_id),
                ('my_teacher_list', my_teacher_list),
                ('results', data)
            ]))
        else:
            return super().get_paginated_response(data)


class UserViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing users.
    create:
        Create a new user instance.
    retrieve:
        Return the given user.
    update:
        Update the given user.
    partial_update:
        Update the given user given field only.
    destroy:
        Delete the given user.
    """

    permission_classes = (UserPermission, )
    pagination_class = UserPagination
    queryset = User.objects.all()
    serializer_class = GetUserSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter,
                       filters.OrderingFilter,)
    search_fields = ('^username', '^first_name', '^last_name')
    filterset_fields = ('school__id', 'school__name')
    ordering_fields = ('last_name',)

    def get_serializer_class(self):
        serializer_class = self.serializer_class

        if self.request.method in ['POST', 'PATCH', 'PUT']:
            serializer_class = PostUserSerializer
        return serializer_class

    def get_queryset(self):
        """
        Custom queryset for User
            - Administrator can create, view, update, and destroy Administrator
              School Leader, Department Coach and Teachers.
            - Teacher can only update their own user record.
            - School Leader can only update their own user record.
            - Department Coach can only update their own user record.
            - staff user has access (CRUD) to every records
        """

        user_name = self.request.query_params.get('search')
        co_teacher = self.request.query_params.get('co_teacher_search')
        if not self.request.user.is_authenticated:
            queryset = User.objects.none()
        else:
            user_group = User.objects.filter(
                username=self.request.user
            ).values_list('groups__name', flat=True)

            if self.request.user.is_staff or 'administrator' in user_group:
                queryset = User.objects.all()
            elif 'cluster_administrator' in user_group:
                schools = ClusterAdminSchool.objects.filter(
                    user=self.request.user)
                if schools:
                    queryset = User.objects.filter(Q(
                        school__in=schools[0].school.all()) | Q(
                            username=self.request.user.username)).exclude(
                                groups__name__in=['administrator', ])
                else:
                    queryset = User.objects.filter(
                        username=self.request.user.username)
            elif 'school_leader' in user_group:
                queryset = User.objects.filter(
                    school=self.request.user.school,
                    groups__name__in=['teacher', 'school_leader',
                                      'department_coach']
                ).exclude(groups__name__in=['administrator',
                                            'cluster_administrator'])
            elif 'department_coach' in user_group:
                queryset = User.objects.filter(
                    school=self.request.user.school,
                    groups__name__in=['teacher', 'department_coach']).exclude(
                        groups__name__in=['administrator',
                                          'cluster_administrator',
                                          'school_leader'])
            elif 'teacher' in user_group:
                queryset = User.objects.filter(
                    school=self.request.user.school,
                    groups__name__in=['teacher', ]).exclude(
                        groups__name__in=['administrator',
                                          'cluster_administrator',
                                          'school_leader',
                                          'department_coach'])
            else:
                queryset = User.objects.filter(username=self.request.user)
        if user_name:
            queryset = queryset.annotate(name=Concat(
                'first_name', Value(' '),
                'last_name')
            ).filter(Q(username__icontains=user_name) | Q(
                name__icontains=user_name)).exclude(
                    username=self.request.user.username)

        if co_teacher:
            queryset = User.objects.filter(username__iexact=co_teacher)

        return queryset.distinct().order_by('first_name')


class AccountActivateViewSet(APIView):
    """
    Validate an activation key and activate the corresponding user
    if valid. Returns the success message, ``error message`` on
    failure.
    """

    def get(self, request):

        # getting query string params and storing in a variable to use
        get_data = request.query_params

        try:
            uid = force_text(urlsafe_base64_decode(get_data['uid']))
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if not user.is_active:
            if user is not None and default_token_generator.check_token(
                    user, get_data['token']):
                user.is_active = True
                user.save()
                return Response({"message": "your email is verified and TARA"
                                            " account is activated "
                                            "successfully."},
                                status=status.HTTP_200_OK)
            else:
                return Response({"message": "your activation link is expired,"
                                            " Kindly contact TARA team."},
                                status=status.HTTP_406_NOT_ACCEPTABLE)
        else:
            return Response({"message": "your email is already verified and"
                                        " account is activated."},
                            status=status.HTTP_405_METHOD_NOT_ALLOWED)


class GroupViewSet(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing groups.
    create:
        Create a new group instance.
    retrieve:
        Return the given group.
    update:
        Update the given group.
    partial_update:
        Update the given group given field only.
    destroy:
        Delete the given group.
    """
    permission_classes = (OnlyReadPermission, )
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class CustomPasswordResetView(PasswordResetView):
    """
        Calls Django Auth PasswordResetForm save method.
        Accepts the following POST parameters: email
        Returns the success/fail message.
    """
    serializer_class = CustomPasswordResetSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        # Create a serializer with request.data
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        serializer.save()
        # Return the success message with OK HTTP status
        return Response({
            "detail": ("Password reset e-mail has been sent.")},
            status=status.HTTP_200_OK
        )


class CustomLoginView(LoginView):
    """
    Check the credentials and return the REST Token
    if the credentials are valid and authenticated.

    Accept the following POST parameters: username, password
    Return the REST Framework Token Object's key and user id.
    """
    # serializer_class = CustomLoginSerializer
    def get_response(self):
        serializer_class = CustomTokenSerializer

        if getattr(settings, 'REST_USE_JWT', False):
            data = {
                'user': self.user,
                'token': self.token
            }
            serializer = serializer_class(instance=data,
                                          context={'request': self.request})
        else:
            serializer = serializer_class(instance=self.token,
                                          context={'request': self.request})
        if not self.request.user.trial_expiry_date:
            self.request.user.trial_expiry_date = self.request.user.last_login\
                + datetime.timedelta(days=29)
            self.request.user.save()

        return Response(serializer.data, status=status.HTTP_200_OK)


class CustomSetPasswordView(PasswordChangeView):
    """
    Override password change view to set value of password_set to True
    """

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if not request.user.password_set:
            change_password_email(request.user, getDomain())
        request.user.password_set = True
        request.user.save()
        serializer.save()
        return Response({"detail": _("New password has been saved.")})


class MyTeacherViewset(viewsets.ModelViewSet):
    """
    list:
        Return a list of all the existing my teacher list.
    create:
        Create a new my teacher list.
    retrieve:
        Return the given my teacher list.
    update:
        Update the given my teacher list.
    partial_update:
        Update the given my teacher given field only.
    destroy:
        Delete the given my teacher list.
    """
    permission_classes = (IsAuthenticated, MyTeacherPermission)
    queryset = MyTeacher.objects.all()
    serializer_class = GetMyTeacherSerializer
    filter_backends = (DjangoFilterBackend, )
    filterset_fields = ('user',)

    def get_serializer_class(self):
        serializer_class = self.serializer_class
        if self.request.method not in ['GET']:
            serializer_class = PostMyTeacherSerializer
        return serializer_class


def change_password_email(user, domain):
    """
    Method for sending password change notification email to the users.
    """
    subject = 'Welcome to Tara Account'
    sg = sendgrid.SendGridAPIClient(config.get('LIVE_TARA_SENDGRID_API_KEY'))
    from_email = 'noreply@taraedtech.com'
    template_id = "d-0efa9ce14c5f45b39b2dbfcf32e7b77e"
    to_email = user.email
    tara_link = "https://{0}/login".format(domain)
    data = {
        "personalizations": [
            {
                "to": [
                    {
                        "email": to_email,
                    }
                ],
                "dynamic_template_data": {
                    "tara_link": tara_link,
                },
            }
        ],
        "from": {
            "email": from_email
        },
        "template_id": template_id
    }
    sg.client.mail.send.post(request_body=data)

