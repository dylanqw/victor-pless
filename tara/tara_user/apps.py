from django.apps import AppConfig


class TaraUserConfig(AppConfig):
    name = 'tara_user'
