from storages.backends.s3boto3 import S3Boto3Storage
from os import environ


class TaraFileStorage(S3Boto3Storage):

    if "TARA_APIS_ENVIRON" in environ.keys():
        if environ["TARA_APIS_ENVIRON"] == "DEV":
            location = 'media/dev-media'
    
        if environ["TARA_APIS_ENVIRON"] == "PROD":
            location = 'media/live-media'

        if environ["TARA_APIS_ENVIRON"] == "local":
            location = 'media/local-media'
    
        if environ["TARA_APIS_ENVIRON"] == "QA":
            location = 'media/qa-media'
    
        if environ["TARA_APIS_ENVIRON"] == "Staging":
            location = 'media/staging-media'
    file_overwrite = False
