"""tara URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import RedirectView, TemplateView
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from lesson import views as lesson_view
from tara.routers import router
from tara_user import views
from billing import views as billing_view

admin.site.site_title = 'TARA'
admin.site.site_header = 'TARA Dashboard'
admin.site.index_title = 'Dashboard'

# drf-yasg Configuration
schema_view = get_schema_view(
    openapi.Info(
        title="TARA APIs Docs",
        default_version='v1',
        description="TARA APIs Documentation",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    path(r'^', include('rest_framework.urls',
                       namespace='tara_auth')),
    url('djrichtextfield/', include('djrichtextfield.urls')),
    url(r'^api/auth/', include('rest_auth.urls')),
    url(r'^api/auth/forgot/password/', views.CustomPasswordResetView.as_view(),),
    url(r'^api/auth/password/reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        TemplateView.as_view(), name='password_reset_confirm'),
    url(r'^api/login', views.CustomLoginView.as_view()),
    url(r'^api/activate/', views.AccountActivateViewSet.as_view(),
        name='activate_user'),
    url(r'^api/password/change', views.CustomSetPasswordView.as_view()),

    # drf-yasg URL
    url(r'^api/docs/$', schema_view.with_ui('swagger', cache_timeout=0),
        name='api-doc'),
    # Temporary redirecting base url to api documentation url
    url(r'^$', RedirectView.as_view(url='/api/docs/')),
    url(r'^api/schoollessontemplates/', lesson_view.GetSchoolLessonTemplateAPIView.as_view()),
    url(r'^exportlessondocument/', lesson_view.download_docx),
    url(r'^exportlessoncalendar/', lesson_view.exportLessonCalendar),
    url(r'^exportobservationdocument/', lesson_view.download_observation_docx),
    # Stripe Webook
    url(r'^api/stripe/', csrf_exempt(billing_view.StripeWebhook.as_view())),
    url(r'^api/lesson_plans/(?P<slug>[\w-]+)/', lesson_view.GetLessonPlanViewSet.as_view())
]

if settings.DEBUG:

    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
