"""
Routers provide a convenient and consistent way of automatically
determining the URL conf for your API.
They are used by simply instantiating a Router class, and then registering
all the required ViewSets with that router.
For example, you might have a `urls.py` that looks something like this:
    router = routers.DefaultRouter()
    router.register('users', UserViewSet, 'user')
    router.register('accounts', AccountViewSet, 'account')
    urlpatterns = router.urls
"""
from rest_framework import routers
from tara_user import views as user_views
from lesson import views as lesson_views
from billing import views as billing_views

router = routers.DefaultRouter()

# Users Router
router.register(r'users', user_views.UserViewSet, base_name='user')
router.register(r'groups', user_views.GroupViewSet, base_name='group')
router.register(r'myteachers', user_views.MyTeacherViewset)

# Lesson Router
router.register(r'schools', lesson_views.SchoolViewSet)
router.register(r'subjects', lesson_views.SubjectViewSet)
router.register(r'classes', lesson_views.ClassViewSet)
router.register(r'standards', lesson_views.StandardViewSet)
router.register(r'containers', lesson_views.ContainerViewSet)
router.register(
    r'learningintelligences', lesson_views.LearningIntelligenceViewSet
)
router.register(r'activities', lesson_views.ActivityViewSet)
router.register(r'templates', lesson_views.TemplateViewSet)
router.register(r'lessonplans', lesson_views.LessonPlanViewSet)
router.register(r'ratelessons', lesson_views.RateLessonViewSet)
router.register(r'activitycontainer', lesson_views.ActivityContainerViewSet)
router.register(r'tagelements', lesson_views.TagElementViewSet)
router.register(r'attachments', lesson_views.AttachmentViewSet)
router.register(r'lessonactivities', lesson_views.LessonActivityViewSet)
router.register(r'activitycomments', lesson_views.ActivityCommentViewSet)
router.register(r'activitytemplates', lesson_views.ActivityTemplateViewSet)
router.register(r'schooldocuments', lesson_views.SchoolDocumentViewSet)
router.register(r'states', lesson_views.StateViewSet)
router.register(r'schooltypes', lesson_views.SchoolTypeViewSet)
router.register(r'calenders', lesson_views.CalenderViewset)
router.register(r'strategygroup', lesson_views.StrategyGroupViewset)
router.register(r'observationtype', lesson_views.ObservationTypeViewset)
router.register(r'observation', lesson_views.ObservationViewset)
router.register(r'smartguide', lesson_views.SmartGuideViewset)
router.register(r'evaluationtype', lesson_views.EvaluationTypeViewset)
router.register(r'evaluationchoices', lesson_views.EvaluationChoicesViewset)
router.register(r'evaluatelessonobservation',
                lesson_views.EvaluateLessonObservationViewset)
router.register(r'evaluationcriteriagroup',
                lesson_views.EvaluationCriteriaGroupViewset)

# Billing Router
router.register(r'billing', billing_views.BillingAccountViewSet)
router.register(r'coupon', billing_views.ValidateCouponViewSet,
                base_name='coupon')
router.register(r'schoolsubscription', billing_views.SchoolSubscriptionViewset)
