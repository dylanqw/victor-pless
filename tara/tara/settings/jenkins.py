"""Jenkins setting file

This file is being used by Jenkins server(PR Builder job) to run following:
    - Test cases
    - Static code analysis
    - Code coverage

Note: Inheriting base DB settings, sqlite DB, for test cases
"""

from .base import *

print('Using jenkins.py as settings file')

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '5432', 
    }
}
