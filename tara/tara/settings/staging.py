"""
    Staging setting file
"""

from .base import *

print('Using staging.py as settings file')

DEBUG = True

ALLOWED_HOSTS = ['staging-api.taraedtech.com']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': config['PSQL_DATABASE'],
        'USER': config['PSQL_USER'],
        'PASSWORD': config['PSQL_PASSWORD'],
        'HOST': config['PSQL_DB_HOST'],
        'PORT': '5432',
    }
}