"""
    prod setting file
"""

from .base import *

print('Using prod.py as settings file')

# SECURITY WARNING: don't run with debug turned on in production!

DEBUG = False

ALLOWED_HOSTS = ['api.taraedtech.com']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': config['PSQL_DATABASE'],
        'USER': config['PSQL_USER'],
        'PASSWORD': config['PSQL_PASSWORD'],
        'HOST': config['PSQL_DB_HOST'],
        'PORT': '5432',        
    }
}
