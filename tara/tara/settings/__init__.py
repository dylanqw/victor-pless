from os import environ

if "TARA_APIS_ENVIRON" in environ.keys():
    if environ["TARA_APIS_ENVIRON"] == "DEV":
        from .dev import *

    if environ["TARA_APIS_ENVIRON"] == "PROD":
        from .prod import *

    if environ["TARA_APIS_ENVIRON"] == "local":
        from .local import *

    if environ["TARA_APIS_ENVIRON"] == "QA":
        from .qa import *

    if environ["TARA_APIS_ENVIRON"] == "Staging":
        from .staging import *

    if environ["TARA_APIS_ENVIRON"] == "JENKINS":
        from .jenkins import *

else:
    raise NotImplementedError(
        'No TARA_APIS_ENVIRON variable found, exiting the program'
    )
