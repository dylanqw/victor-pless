#!/bin/bash

echo ""
echo "***** TARA's Docker build script *****"
date

echo $DEPLOYMENT_GROUP_NAME

if [ "$DEPLOYMENT_GROUP_NAME" == "Tara-Dev-Deployment-Group" ]
then
    export build_environment=DEV
fi

if [ "$DEPLOYMENT_GROUP_NAME" == "Tara-Qa-Deployment-Group" ]
then
    export build_environment=QA
fi

if [ "$DEPLOYMENT_GROUP_NAME" == "Tara-Master-Deployment-Group" ]
then
    export build_environment=PROD
fi

if [ "$DEPLOYMENT_GROUP_NAME" == "Tara-Staging-Deployment-Group" ]
then
    export build_environment=Staging
fi

echo "build_environment --> ${build_environment}"

cd /tmp

# settings for SSL
# cp /home/ec2-user/privkey.pem /tmp/certs/
# cp /home/ec2-user/cert.pem /tmp/certs/

#cp /home/ec2-user/config_tara.py /tmp/

echo "Running --> docker build --build-arg build_environment={build_environment} -t tara_apis ."
docker build --build-arg build_environment=${build_environment} -t tara_apis .

echo
echo
echo "***** Starting Postgresql Database container *****"

docker run --net tara-network \
            --name "$(python -c 'from config_tara import main_conf;print main_conf["PSQL_DB_HOST"]')" \
            -v /mnt/db_data:/var/lib/postgresql/data \
            -e POSTGRES_DB="$(python -c 'from config_tara import main_conf;print main_conf["PSQL_DATABASE"]')" \
            -e POSTGRES_USER="$(python -c 'from config_tara import main_conf;print main_conf["PSQL_USER"]')" \
            -e POSTGRES_PASSWORD="$(python -c 'from config_tara import main_conf;print main_conf["PSQL_PASSWORD"]')" \
            -p 5432:5432 \
            -d postgres:11.1

sleep 10

echo "Running --> docker run --net=tara-network --name tara_server -p 8080:8080 -d tara_server"
docker run --net tara-network \
            --name tara_server \
            -v /mnt/tara_logs/supervisor:/var/log/supervisor \
            -v /mnt/tara_logs/nginx:/var/log/nginx \
            -p 443:443 \
            -d tara_apis

echo
echo
echo "***** Running  Migrations *****"
docker exec tara_server bash -c 'cd /home/tara/tara;yes "yes" | python3.7 manage.py migrate'

if [ "$DEPLOYMENT_GROUP_NAME" == "Tara-Dev-Deployment-Group" ]; then
   echo
   echo
   echo "***** Running add_sites management command  *****"
   docker exec tara_server bash -c 'cd /home/tara/tara;yes "yes" | python3.7 manage.py add_sites "dev-api.taraedtech.com" "dev-api.taraedtech.com"'
fi
if [ "$DEPLOYMENT_GROUP_NAME" == "Tara-Qa-Deployment-Group" ]; then
   echo
   echo
   echo "***** Running add_sites management command  *****"
   docker exec tara_server bash -c 'cd /home/tara/tara;yes "yes" | python3.7 manage.py add_sites "qa-api.taraedtech.com" "qa-api.taraedtech.com"'
fi
if [ "$DEPLOYMENT_GROUP_NAME" == "Tara-Staging-Deployment-Group" ]; then
   echo
   echo
   echo "***** Running add_sites management command  *****"
   docker exec tara_server bash -c 'cd /home/tara/tara;yes "yes" | python3.7 manage.py add_sites "staging-api.taraedtech.com" "staging-api.taraedtech.com"'
fi

if [ "$DEPLOYMENT_GROUP_NAME" == "Tara-Master-Deployment-Group" ]; then
   echo
   echo
   echo "***** Running add_sites management command  *****"
   docker exec tara_server bash -c 'cd /home/tara/tara;yes "yes" | python3.7 manage.py add_sites "api.taraedtech.com" "api.taraedtech.com"'
fi

echo
echo
echo "***** Running add_state management command  *****"
docker exec tara_server bash -c 'cd /home/tara/tara;yes "yes" | python3.7 manage.py add_state'

echo
echo
echo "***** Running add_school_type management command  *****"
docker exec tara_server bash -c 'cd /home/tara/tara;yes "yes" | python3.7 manage.py add_school_type'

echo
echo
echo "***** Collecting Static Files *****"
docker exec tara_server bash -c 'cd /home/tara/tara;yes "yes" | python3.7 manage.py collectstatic --no-input'
