#!/bin/bash

# Overview: Script behind TARA-PR-Builder Jenkins job. This job runs whenever a 
# Merge request is created.
# 
# Prerequisites: Updated config_tara.py on Jenkins server(location: /opt/)
# 
# How to run -- Autorun in Jenkins job.
# 
# What this script will do:
#   1. Copy configuration file to Jenkins job workspace
#   2. Creates python virtual environment
#   3. Install python libs
#   4. Run unit test cases, code coverage, pylint
#   5. Generates reports for Jenkins to populate
# 
# Notes: ----- 

echo
echo
echo "***** TARA PR Builder script *****"
date

echo
echo
echo "***** Copying configuration file  *****"
sudo cp /opt/config_tara.py .
echo "*****Copy config file in current directory***"
sudo cp /opt/config_tara.py tara/tara/settings/

echo "***** Copying configuration End  *****"

echo
echo
echo "***** Creating python virtual environment *****"
python3 -m venv tara-env

echo
echo
echo "***** Virtual environment created, install requirements *****"
tara-env/bin/pip install -r requirements.txt

echo
echo
echo "***** Running Code coverage and Unit test cases *****"
echo "***** This will also generate XML report for JUnit *****"
echo "***** Jenkins will populate this XML report *****"
cd tara
../tara-env/bin/coverage run \
                        --source='.' manage.py test \
                        --settings=tara.settings.jenkins \
                        --noinput

../tara-env/bin/coverage report \
                            --omit=tara/wsgi.py, tara/settings/__init__.py, manage.py

../tara-env/bin/coverage xml

echo
echo
echo "***** Running prospector for linting the code *****"
../tara-env/bin/prospector --zero-exit --output-format pylint > prospector.log
