FROM ubuntu:18.04

MAINTAINER Sachinj@trinesis.com

# Install required packages and remove the apt packages cache when done.

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    git \
	nano \
	python3-dev \
	python3-pip\
	python3-setuptools \
	python-smbus \
	build-essential \
    apt-utils \
	libreadline-gplv2-dev \
	libncursesw5-dev \
	libgdbm-dev \
	libc6-dev \
	libbz2-dev \
	zlib1g-dev \
	libssl-dev \
	openssl \
	libffi-dev \
	nginx \
	supervisor \
	libmysqlclient-dev \
	libpq-dev \
	wget \
	tk-dev \
  	&& rm -rf /var/lib/apt/lists/*
	  
RUN wget https://www.python.org/ftp/python/3.7.0/Python-3.7.0.tgz
RUN tar xzf Python-3.7.0.tgz
RUN rm -f Python-3.7.0.tgz


RUN cd Python-3.7.0 && ./configure --enable-optimizations && make altinstall


RUN pip3.7 install --upgrade pip
RUN pip install uwsgi


# Setup all the config files

RUN echo "daemon off;" >> /etc/nginx/nginx.conf
COPY nginx-app.conf /etc/nginx/sites-available/default
COPY supervisor-app.conf /etc/supervisor/conf.d/

# COPY requirements.txt and RUN pip install BEFORE adding the rest of our code, this will cause Docker's caching mechanism
# to prevent re-installinig (all our) dependencies when we made a change in a line or two in our app.

COPY requirements.txt /home/tara/

RUN pip install -r /home/tara/requirements.txt

ARG build_environment=DEV

ENV TARA_APIS_ENVIRON=$build_environment

COPY . /home/tara/

# Adding configuration file inside settings directory
ADD ./config_tara.py /home/tara/tara/tara/settings/

ADD ./certs/taraedtech.com.crt /home/tara/certs/
ADD ./certs/taraedtech.com.key /home/tara/certs/

VOLUME /var/log/supervisor/

# EXPOSE 80
EXPOSE 443

CMD ["supervisord", "-n"]
